
# HRA2016 - Reversi #
Made with love for ICP 2016


### What is it ###
This is an implementation of reversi board game in C++. It was created as 
a part of ICP course on VUTBR Faculty of Information Technology.

### How to compile ###
There are 2 ways to compile this project, the first one uses pre-made Makefile and the 
second uses CMake.

This software requires theese dependencies to correctly work: 
	Qt toolkit (version at least 5.5, earlier versions were not tested)
	GCC compiler (has to support at least c++14, tested on version 5.3.1)
	The GUI requires OpenGL version of at least 3.3

1) Makefile
This method works only for Linux operating system, under theese conditions: 
	a) The Qt toolkit has a link to the correct version in /usr/local/share/Qt ,
		or the system has the correct version of qmake in PATH.
	b) The g++ compiler is present in PATH as "g++" or "g++-5.3".
If above conditions are met, then you can proceed with theese steps:
	a) Change directory to the root of the project.
	b) Enter command : make or make -j# , where # is number of threads to run.
	c) After the process completes, run : make run , to run the applications.

2) CMake
This method should work for systems, where the Qt toolkit was correctly installed.
To compile the project: 
	a) mkdir project_build && cd project_build
	b) cmake ..
	# Note : If any of the package finds fail, you have to correctly install the 
		required dependencies.
	c) make or make -j# , where # is the number of threads to run.
If the make command succeeded, the binary files should be in the "bin" directory in 
the root of the project.

### Authors ###
* xpokor67 - Jan Pokorný - xpokor67@fit.stud.vutbr.cz
* xpolas34 - Tomáš Polášek - xpolas34@fit.stud.vutbr.cz

### Used libraries ###
Qt toolkit : https://www.qt.io/
Dirent for Windows : https://github.com/tronkko/dirent

### License ###
Dirent : 
The MIT License (MIT)

Copyright (c) 2015 Toni Rönkkö

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.