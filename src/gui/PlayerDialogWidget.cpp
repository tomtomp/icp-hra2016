/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "PlayerDialogWidget.h"

namespace gui
{
    PlayerDialogWidget::PlayerDialogWidget(QWidget *parent, const core::Game *gamePtr, QString name) :
        QGroupBox(name, parent)
    {
        nameLabel = new QLabel(tr("Name: "));

        nameLineEdit = new QLineEdit();

        playerTypeLabel = new QLabel(tr("Type: "));

        playerTypeCombo = new QComboBox();
        for (const std::string &playerType : gamePtr->getPlayerTypes())
            playerTypeCombo->addItem(QString::fromStdString(playerType));

        connect(playerTypeCombo, SIGNAL(currentIndexChanged(const QString&)),
                this, SLOT(playerTypeChange(const QString&)));

        aiTypeLabel = new QLabel(tr("Ai type: "));

        aiTypeCombo = new QComboBox();
        for (const std::string &aiType : gamePtr->getAiTypes())
            aiTypeCombo->addItem(QString::fromStdString(aiType));
        aiTypeCombo->setDisabled(true);

        mainLayout = new QGridLayout;

        mainLayout->addWidget(nameLabel, 1, 0);
        mainLayout->addWidget(nameLineEdit, 1, 1);

        mainLayout->addWidget(playerTypeLabel, 2, 0);
        mainLayout->addWidget(playerTypeCombo, 2, 1);

        mainLayout->addWidget(aiTypeLabel, 3, 0);
        mainLayout->addWidget(aiTypeCombo, 3, 1);

        setLayout(mainLayout);
    }

    PlayerDialogWidget::~PlayerDialogWidget()
    {
        delete mainLayout;
    }

    void PlayerDialogWidget::playerTypeChange(const QString &text)
    {
        if (text == "AI")
            aiTypeCombo->setDisabled(false);
        else
            aiTypeCombo->setDisabled(true);
    }
}
