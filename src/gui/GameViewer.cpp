/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include <QtWidgets/QLabel>
#include "GameViewer.h"

namespace gui
{
    const QMatrix4x4 GameViewer::SHADOW_BIAS{0.5, 0.0, 0.0, 0.5,
                                             0.0, -0.5, 0.0, 0.5,
                                             0.0, 0.0, 0.5, 0.5,
                                             0.0, 0.0, 0.0, 1.0};

    GameViewer::GameViewer(QWidget *parent) :
        QOpenGLWidget(parent),
        mCameraDistance{2.0f},
        mCamera{{0.0f, 0.0f, mCameraDistance}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}},
        mActiveGame{-1}
    {
        QSurfaceFormat format;

        format.setSwapBehavior(QSurfaceFormat::SwapBehavior::DoubleBuffer);

        // VSync
        format.setSwapInterval(1);

        format.setSamples(4);

        format.setRenderableType(QSurfaceFormat::OpenGL);
        format.setProfile(QSurfaceFormat::CoreProfile);
        format.setVersion(3, 3);

        setFormat(format);

        //connect(this, SIGNAL(frameSwapped()), this, SLOT(frameSwappedSlot()));

        connect(&mUpdateTimer, SIGNAL(timeout()), this, SLOT(updateTick()));
        mUpdateTimer.start(static_cast<int>(static_cast<float>(MS_PER_S) / TARGET_UPS));

        connect(&mRenderTimer, SIGNAL(timeout()), this, SLOT(frameSwappedSlot()));
        mRenderTimer.start(static_cast<int>(static_cast<float>(MS_PER_S) / TARGET_FPS));
    }

    void GameViewer::printContextInfo()
    {
        std::string glType{(context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL"};
        std::string glVersion{reinterpret_cast<const char*>(glGetString(GL_VERSION))};
        std::string glProfile;

        switch (format().profile())
        {
            case QSurfaceFormat::CoreProfile:
                glProfile = "Core Profile";
                break;
            case QSurfaceFormat::NoProfile:
                glProfile = "No Profile";
                break;
            case QSurfaceFormat::CompatibilityProfile:
                glProfile = "Compatibility Profile";
                break;
        }

        util::Logger::logInfo(glType + " : " + glVersion + " (" + glProfile + ")");
    }

    GameViewer::~GameViewer()
    {
        makeCurrent();
        deinitializeGL();
    }

    void GameViewer::initializeGL()
    {
        initializeOpenGLFunctions();

        printContextInfo();

        glClearColor(0.329f, 0.694f, 0.203f, 1.0f);

        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);

        // Enable the alpha blending.
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        try
        {
            mProgram = new QOpenGLShaderProgram();
            if (!mProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/simple.vert"))
                throw std::runtime_error("Failed to compile simple.vert");
            if (!mProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/simple.frag"))
                throw std::runtime_error("Failed to compile simple.frag");
            if (!mProgram->link())
                throw std::runtime_error("Failed to link simple.");

            mTextProgram = new QOpenGLShaderProgram();
            if (!mTextProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/text.vert"))
                throw std::runtime_error("Failed to compile text.vert");
            if (!mTextProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/text.frag"))
                throw std::runtime_error("Failed to compile text.frag");
            if (!mTextProgram->link())
                throw std::runtime_error("Failed to link text.");

            mShadingProgram = new QOpenGLShaderProgram();
            if (!mShadingProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/shading.vert"))
                throw std::runtime_error("Failed to compile shading.vert");
            if (!mShadingProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/shading.frag"))
                throw std::runtime_error("Failed to compile shading.frag");
            if (!mShadingProgram->link())
                throw std::runtime_error("Failed to link shading.");
        } catch(std::runtime_error &e)
        {
            QMessageBox message;
            message.critical(nullptr, "Failed to create a shader program.", tr("Shader program creation failed to link, "
                "please check, if you have OpenGL version of 3.3 or higher! ") + QString::fromStdString(e.what()));
            message.show();
            exit(-1);
        }

        /*
        static const float vertexData[]{
            // Triangle
            -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

            // X axis
            -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

            // Y axis
            0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

            // Z axis
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
        };
         */

        mVAO.create();

        mProjectionMatrix.perspective(45.0f, static_cast<float>(width()) / height(), 0.1f, 100.0f);
        mTextProjectionMatrix.ortho(0.0f, width(), 0.0f, height(), 0.1f, 100.0f);
        mShadingProjectionMatrix.ortho(-2.0f, 2.0f, -2.0f, 2.0f, 0.0f, 10.0f);

        mLightPosition = QVector3D{0.0f, 1.0f, 2.0f};
        mLightViewMatrix.lookAt(mLightPosition, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f});

        mShadowFB = new QOpenGLFramebufferObject(1024, 1024, QOpenGLFramebufferObject::Depth, GL_TEXTURE_2D);

        /*
        mMesh = new gui::Mesh(":/stone.ply");
        mTexture = new gui::Texture(":/stone.bmp");

        mObj = new gui::RenderableObject(mMesh, mTexture);

        mObj->getTrans().setScale(0.2);

        mObjCollection = new gui::RenderableCollection(mMesh, mTexture);
        for (int jjj = 0; jjj < 10; ++jjj)
        {
            for (int iii = 0; iii < 10; ++iii)
            {
                gui::GameObject *newObj = new gui::GameObject(QVector3D{iii * 0.4f - 2.0f, jjj * 0.4f - 2.0f, 0.0f});
                mObjVec.push_back(newObj);
                mObjCollection->addObject(newObj);
            }
        }

        for (auto &o : mObjCollection->getObjects())
            o->getTrans().setScale(0.2);

        mText = new gui::TextObject("Hello World!", QFont("Arial", 40), QColor(255, 0, 0), QColor(255, 255, 255, 0),
                                    QVector3D{-1.0f, -1.0f, 0.0f});
        mText->getTrans().scale(0.5);
        */
    }

    void GameViewer::resizeGL(int width, int height)
    {
        mProjectionMatrix.setToIdentity();
        mProjectionMatrix.perspective(45.0f, static_cast<float>(width) / height, 0.1f, 100.0f);
        mTextProjectionMatrix.setToIdentity();
        mTextProjectionMatrix.ortho(0.0f, width, 0.0f, height, 0.1f, 100.0f);
        QOpenGLWidget::resizeGL(width, height);
        update();
    }

    void GameViewer::paintGL()
    {
        /*
        static std::size_t frames = 0;
        static QTime timer;

        if (frames == 0)
            timer.start();
        else
            std::cout << "FPS is : " << frames / (timer.elapsed() / 1000.0) << std::endl;
        frames++;
         */

        mVAO.bind();

        gui::GameModel *activeGame = getActiveGame();

        // Rendering the shading pass.
#if DO_SHADOWS
        mShadowFB->bind();

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        glViewport(0, 0, 1024, 1024);

        glEnable(GL_DEPTH_TEST);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        QMatrix4x4 depthMvp = mShadingProjectionMatrix * mLightViewMatrix;
        QMatrix4x4 depthBiasMvp = SHADOW_BIAS * depthMvp;

        if (activeGame)
            activeGame->renderS(this,
                                mShadingProgram, depthMvp);


        QOpenGLTexture shadowTex(mShadowFB->toImage(true));

        mShadowFB->bindDefault();
#else
        QMatrix4x4 depthMvp;
        QMatrix4x4 depthBiasMvp;
#endif

        // Actual rendering.
        glViewport(0, 0, width(), height());
        //glClearColor(0.329f, 0.694f, 0.203f, 1.0f);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#if DO_SHADOWS
        shadowTex.bind(0);
#endif

        mProgram->bind();
        mProgram->setUniformValue(3, mCamera.getLocation());
        mProgram->setUniformValue("shadowFB", 0);
        mProgram->setUniformValue("lightPos", mLightPosition);
        float time = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() % 10000) / (2.0E4f);
        mProgram->setUniformValue("time", time);
        mProgram->release();

        if (activeGame)
            activeGame->render(this,
                               mProgram, mProjectionMatrix * mCamera.getMatrix(),
                               mTextProgram, mTextProjectionMatrix * mCamera.getMatrix(),
                               depthBiasMvp);

#if DO_SHADOWS
        shadowTex.release();
#endif

        mVAO.release();

        /*
        if (mFirst)
        {
            QImage t = fb.toImage();
            for (int jjj = 0; jjj < t.height(); ++jjj)
            {
                for (int iii = 0; iii < t.width(); ++iii)
                {
                    QColor color(t.pixel(iii, jjj));
                    color.setAlpha(255);
                    t.setPixel(iii, jjj, color.rgba());
                }
            }

            for (int iii = 0; iii < t.byteCount(); ++iii)
                std::cout << static_cast<int>(t.bits()[iii]) << " ";
            std::cout << std::endl;
            QWidget *w = new QWidget();
            QLabel *l = new QLabel();
            l->setPixmap(QPixmap::fromImage(t));
            l->setParent(w);
            w->show();
            mFirst = false;
        }
        */

        /*
        for (auto o : mObjCollection->getObjects())
            o->getTrans().rotate(QQuaternion::fromEulerAngles(0.0, 6.0, 0.0));

        mObjCollection->render(this, mProgram, mProjectionMatrix * mViewMatrix * mModelTransform.getMatrix());
         */

        /*
        QString text{"Hello World!"};
        QColor background{0, 0, 0, 0};
        QColor color{255, 0, 0};
        QFont font("Arial", 40);
        QFontMetrics fm(font);
        int width = fm.width(text);
        int height = fm.height();

        QImage textImage(width, height, QImage::Format_RGB888);

        mPainter.begin(&textImage);
        mPainter.fillRect(0, 0, width, height, background);
        mPainter.setBrush(color);
        mPainter.setPen(color);
        mPainter.setFont(font);
        mPainter.drawText(0, height, text);
        mPainter.end();

        mTexture->updateTexture(textImage);
         */

        /*
        mProgram->bind();
        mObj->getTrans().rotate(QQuaternion::fromEulerAngles(0.0, 6.0, 0.0));
        mObj->render(this, mProgram, mProjectionMatrix * mViewMatrix * mModelTransform.getMatrix());
        mProgram->release();

        mTextProgram->bind();
        mText->render(this, mTextProgram, mTextProjectionMatrix * mViewMatrix);
        mTextProgram->release();
         */


        /*
        if (first)
        {
            QWidget *w = new QWidget();
            QLabel *l = new QLabel();
            l->setPixmap(QPixmap::fromImage(textImage));
            l->setParent(w);
            w->show();
            first = false;
        }

        glEnable(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE);
        glPixelStorei(GL_UNPACK_LSB_FIRST, GL_FALSE);
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, 0);
        glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
        glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 100, 10, 0, GL_RGB, GL_UNSIGNED_BYTE, textImage.constBits());
        glPushMatrix();
        glTranslatef(0.5f, 0.5f, 0.0f);
        glBegin(GL_QUADS);
        glTexCoord2f(1, 0); glVertex3f(0, 0, 0);
        glTexCoord2f(0, 0); glVertex3f(100, 0, 0);
        glTexCoord2f(0, 1); glVertex3f(100, 10, 0);
        glTexCoord2f(1, 1); glVertex3f(0, 10, 0);
        glEnd();
        glPopMatrix();
        */

        /*
        mModelTransform.rotate(QQuaternion::fromEulerAngles(0.0, 6.0, 0.0));

        mObj->render(this, mProgram, mProjectionMatrix * mViewMatrix * mModelTransform.getMatrix());
         */

        /*
        mModelTransform.rotate(QQuaternion::fromEulerAngles(1.0, 6.0, 1.0));
        mMvp = mProjectionMatrix * mViewMatrix * mModelTransform.getMatrix();
        mProgram->setUniformValue(mMvpId, mMvp);

        for (int jjj = 0; jjj < 5; ++jjj)
        {
            for (int iii = 0; iii < 5; ++iii)
            {
                trans::TransformMatrix3D copy = mModelTransform;

                copy.translate(iii * 0.4f - 1.0f, jjj * 0.4f - 1.0f, 0.0f);
                mMvp = mProjectionMatrix * mViewMatrix * copy.getMatrix();
                mProgram->setUniformValue(mMvpId, mMvp);

                mObj->render(this);
            }
        }
         */

        /*
        mTextureBuffer.bind();
        mProgram->setUniformValue("textureSampler", 0);

        mVAO.bind();
        glDrawElements(GL_TRIANGLES, mMesh.getIndices().size(), GL_UNSIGNED_INT, &mMesh.getIndices()[0]);
        mVAO.release();
        mTextureBuffer.release();
         */

        /*

        mTextureBuffer.bind();
        mModelTransform.rotate(QQuaternion::fromEulerAngles(0.0, 6.0, 0.0));

        mProgram->setUniformValue("textureSampler", 0);

        for (int jjj = 0; jjj < 5; ++jjj)
        {
            for (int iii = 0; iii < 5; ++iii)
            {
                trans::TransformMatrix3D copy = mModelTransform;

                copy.translate(iii * 0.4f - 1.0f, jjj * 0.4f - 1.0f, 0.0f);
                mProgram->setUniformValue(mMvpId, mMvp);

                mMvp = mProjectionMatrix * mViewMatrix * copy.getMatrix();
                mVAO.bind();
                glDrawElements(GL_TRIANGLES, mMesh.getIndices().size(), GL_UNSIGNED_INT, &mMesh.getIndices()[0]);
                mVAO.release();
            }
        }
        mTextureBuffer.release();
         */
    }

    void GameViewer::deinitializeGL()
    {
        mVAO.destroy();
        delete mShadowFB;
        delete mShadingProgram;
        delete mTextProgram;
        delete mProgram;
    }

    void GameViewer::frameSwappedSlot()
    {
        update();
    }

    void GameViewer::updateTick()
    {
        gui::GameModel *activeGame = getActiveGame();

        if (activeGame)
        {
            try
            {
                activeGame->update();
            } catch(std::runtime_error &e)
            {
                util::Logger::logDebug(std::string("Unable to update the game : ") + e.what());

                QMessageBox msgBox;
                msgBox.critical(0, tr("Unable to update the game!"),
                                tr("Unable to update the game : ") + QString::fromStdString(e.what()));
            }
        }
    }

    bool GameViewer::createNewGame(QString filename)
    {
        try
        {
            gui::GameModel *newGame{nullptr};
            if (filename.isEmpty())
                newGame = new gui::GameModel();
            else
                newGame = new gui::GameModel(filename);
            mGames.push_back(newGame);
        } catch (std::runtime_error &e)
        {
            util::Logger::logDebug(std::string("Could not create the game model : ") + e.what());

            QMessageBox msgBox;
            msgBox.critical(0, tr("Unable to create new game!"),
                            tr("Could not create the game model : ") + QString::fromStdString(e.what()));

            return false;
        }

        return true;
    }

    void GameViewer::mousePressEvent(QMouseEvent *event)
    {
        mFirst = true;
        if (event->button() == Qt::MouseButton::LeftButton)
        {
            gui::GameModel *activeGame = getActiveGame();

            if (activeGame)
                activeGame->mousePressEvent(event, mProjectionMatrix * mCamera.getMatrix(), width(), height());
        }
    }

    void GameViewer::wheelEvent(QWheelEvent *event)
    {
        QVector3D directionVector = mCamera.getLocation() - mCamera.getTarget();
        directionVector.normalize();
        mCameraDistance += event->delta() / -300.0f;
        if (mCameraDistance < 0.2f)
            mCameraDistance = 0.2f;
        else if (mCameraDistance > 10.0f)
            mCameraDistance = 10.0f;
        mCamera.setLocation(mCameraDistance * directionVector);
        /*
        if (mCameraDistance < 0.2f)
            mCameraDistance = 0.2f;
        else if (mCameraDistance > 10.0f)
            mCameraDistance = 10.0f;
        mViewMatrix.setToIdentity();
        mViewMatrix.lookAt({0.0f, 0.0f, mCameraDistance}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f});
         */
    }

    bool GameViewer::closeGame(int index)
    {
        gui::GameModel *activeGame = getGame(index);
        if (!activeGame)
            return false;

        try
        {
            mGames.erase(mGames.begin() + index);
            delete activeGame;
            if (mGames.size() == 0)
                mActiveGame = -1;
        } catch(std::runtime_error &e)
        {
            util::Logger::logDebug(std::string("Unable to close and delete a game : ") + e.what());

            QMessageBox msgBox;
            msgBox.critical(0, tr("Unable to close a game!"),
                            tr("Could not close the game model, probable memory leak : ") + QString::fromStdString(e.what()));
            return false;
        }

        return true;
    }

    bool GameViewer::setActiveGame(int gameIndex)
    {
        if (gameIndex >= static_cast<int>(mGames.size()))
            return false;

        mActiveGame = gameIndex;
        return true;
    }

    bool GameViewer::saveGame()
    {
        gui::GameModel *activeGame = getActiveGame();
        if (!activeGame)
            return false;

        QFileDialog fileDialog(this, tr("Please select save location"), "", tr("Saved game (*.gs)"));
        fileDialog.setFileMode(QFileDialog::AnyFile);
        fileDialog.setOption(QFileDialog::DontUseNativeDialog, true);
        fileDialog.setLabelText(QFileDialog::Accept, "Save");
        fileDialog.setDefaultSuffix(".gs");

        if (!fileDialog.exec())
            return false;

        QString filename = fileDialog.selectedFiles()[0];

        /*
        QString filename = QFileDialog::getSaveFileName(this, tr("Please select save location"), "",
                                                        tr("Saved game (*.gs)"));
        */

        if (filename.isEmpty())
            return false;

        try
        {
            if (!activeGame->saveGame(filename))
                throw std::runtime_error("Cannot write to file.");
        } catch(std::runtime_error &e)
        {
            util::Logger::logDebug(std::string("Unable to save the game : ") + e.what());

            QMessageBox msgBox;
            msgBox.critical(0, tr("Unable to save the game!"),
                            tr("Could not save the game : ") + QString::fromStdString(e.what()));
            return false;
        }

        return true;
    }

    bool GameViewer::undo()
    {
        gui::GameModel *activeGame = getActiveGame();
        if (!activeGame)
            return false;

        try
        {
            activeGame->undo();
        } catch(std::runtime_error &e)
        {
            util::Logger::logDebug(std::string("Unable to undo the game : ") + e.what());

            QMessageBox msgBox;
            msgBox.critical(0, tr("Unable to undo the game!"),
                            tr("Could not undo the game : ") + QString::fromStdString(e.what()));
            return false;
        }

        return true;
    }

    bool GameViewer::redo()
    {
        gui::GameModel *activeGame = getActiveGame();
        if (!activeGame)
            return false;

        try
        {
            activeGame->redo();
        } catch(std::runtime_error &e)
        {
            util::Logger::logDebug(std::string("Unable to redo the game : ") + e.what());

            QMessageBox msgBox;
            msgBox.critical(0, tr("Unable to redo the game!"),
                            tr("Could not redo the game : ") + QString::fromStdString(e.what()));
            return false;
        }

        return true;
    }

    /*
    void GameViewer::mouseMoveEvent(QMouseEvent *event)
    {
        std::cout << "Mouse move event" << std::endl;
    }
    */
}
