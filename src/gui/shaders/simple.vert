#version 330

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexUV;
layout(location = 3) in vec3 cameraPos;

uniform int lightness;
uniform mat4 m;
uniform mat3 mInvTrans;
uniform mat4 mvp;
uniform mat4 depthBiasMvp;
uniform vec3 lightPos;

out vec2 fragUV;
out vec3 fragCameraPos;
out vec3 fragLightPos;
out vec3 worldPosition;
out vec3 worldNormal;
out vec4 shadowCoord;

void main()
{
    vec4 position = vec4(vertexPos, 1.0);

    fragUV = vertexUV;
    fragCameraPos = cameraPos;
    fragLightPos = lightPos;
    worldPosition = vec3(m * position);
    worldNormal = normalize(mInvTrans * vertexNormal);

    gl_Position = mvp * position;
    shadowCoord = depthBiasMvp * position;
}
