#version 330

uniform int lightness;
uniform sampler2D textureSampler;
uniform sampler2D shadowFB;
uniform int effectType;
uniform float time;

in vec2 fragUV;
in vec3 fragCameraPos;
in vec3 fragLightPos;
in vec3 worldPosition;
in vec3 worldNormal;
in vec4 shadowCoord;

out vec4 fragColor;

// Structure representing a light
struct light
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

light light0 = light(
                vec3(0.2, 0.2, 0.2),
                vec3(1.0, 1.0, 1.0),
                vec3(1.0, 1.0, 1.0)
               );

struct material
{
    vec3 ambient;
    vec3 specular;
    float reflect;
};

material material0 = material(
                        vec3(0.2, 0.2, 0.2),
                        vec3(1.0, 1.0, 1.0),
                        5.0
                     );

// Used in smoothing of shadows.
const vec2 poissonDisk[4] = vec2[](
    vec2(-0.94201624, -0.39906216),
    vec2(0.94558609, -0.76890725),
    vec2(-0.094184101, -0.92938870),
    vec2(0.34495938, 0.29387760)
);

void main()
{
    float visibility = 1.0;
    vec4 coords = shadowCoord / shadowCoord.w;

    vec3 color = texture(textureSampler, fragUV).rgb;

    vec3 viewDirection = normalize(fragCameraPos - worldPosition);
    vec3 lightDirection = normalize(fragLightPos);

    /*
    float bias = 0.005;
    for (int iii = 0; iii < 4; iii++)
    {
        if (texture(shadowFB, coords.xy + poissonDisk[iii]/700.0).z < coords.z - bias)
            visibility -= 0.2;
    }
    */

    vec3 ambient = light0.ambient * material0.ambient;
    vec3 diffuse = visibility * light0.diffuse * color * max(0.0, dot(worldNormal, lightDirection));
    vec3 specular;
    if (dot(worldNormal, lightDirection) < 0.0)
        specular = vec3(0.0, 0.0, 0.0);
    else
    {
        specular = visibility * light0.specular * material0.specular *
                   pow(
                    max(
                        0.0,
                        dot(
                            reflect(
                                -lightDirection,
                                worldNormal
                            ),
                            viewDirection
                        )
                   ),
                    material0.reflect
                  );
    }

    fragColor = vec4(ambient + diffuse + specular + (lightness / 10.0 * vec3 (1.0, 0.0, 1.0)), 1.0);
    //fragColor = vec4(pow(vec3(0.2, 0.07, 0.01) / flow(fragUV), vec3(1.4)), 1.0);
    //gl_FragColor = vec4((shadowCoord / shadowCoord.w).rgb, 1.0);
}
