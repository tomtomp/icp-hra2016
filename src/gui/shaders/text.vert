#version 330

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexUV;

uniform int lightness;
uniform mat4 mvp;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

out vec2 fragUV;

void main()
{
    fragUV = vertexUV;
    vec4 pos = mvp * vec4(vertexPos, 1.0);
    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
}
