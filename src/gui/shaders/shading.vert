#version 330

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexUV;
layout(location = 3) in vec3 cameraPos;

uniform int lightness;
uniform mat4 m;
uniform mat3 mInvTrans;
uniform mat4 mvp;

out vec2 fragUV;

void main()
{
    vec4 position = vec4(vertexPos, 1.0);
    fragUV = vertexUV;
    gl_Position = mvp * position;
}
