/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAMEVIEWER_H
#define HRA2016_GAMEVIEWER_H

#include "render/transform/TransformMatrix3D.h"

#include "render/MeshLoader.h"
#include "render/Mesh.h"
#include "render/RenderableObject.h"
#include "render/RenderableCollection.h"
#include "render/TextObject.h"
#include "render/GameModel.h"
#include "render/Camera.h"
#include "render/TextureLoader.h"
#include "../util/Logger.h"

#include <QSurfaceFormat>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFramebufferObjectFormat>
#include <QTime>
#include <QTimer>
#include <QPainter>
#include <QColor>
#include <QTextStream>
#include <QtGlobal>
#include <QtDebug>
#include <QFileDialog>

namespace gui
{
    /**
     * Widget used for displaying game.
     */
    class GameViewer : public QOpenGLWidget, protected QOpenGLFunctions
    {
        Q_OBJECT

    public:
        /**
         * Create new game viewer and initialize OpenGL context.
         */
        GameViewer(QWidget *parent);

        /**
         * Print information about the OpenGL context of this viewer.
         */
        void printContextInfo();

        ~GameViewer();

        /**
         * Create a new game.
         * @return If the game creation failed, returns false, else returns true.
         */
        bool createNewGame(QString filename = "");

        /**
         * Set the active game to the given index.
         * @param gameIndex Index to become the active game, -1 = no active game.
         * @return Returns true, if the switch succeeded, else returns false.
         */
        bool setActiveGame(int gameIndex);

        /**
         * Close the game on given index.
         * @param index Index of the game.
         * @return Returns true, if the game closing succeeded, else returns false.
         */
        bool closeGame(int index);

        /**
         * Save the active game.
         * @return Returns true, if the saving was successful.
         */
        bool saveGame();

        /**
         * Undo the current game.
         * @return Returns true, if the undo was successful.
         */
        bool undo();

        /**
         * Redo the current game.
         * @return Returns true, if the redo was successful.
         */
        bool redo();

        /// Target number of frames per second.
        static constexpr std::size_t TARGET_FPS{30};
        /// Target number of game updates per second.
        static constexpr std::size_t TARGET_UPS{60};
        /// Number of milliseconds per one second.
        static constexpr std::size_t MS_PER_S{1000};
    private slots:
        void frameSwappedSlot();
        void updateTick();
    private:
        void mousePressEvent(QMouseEvent *event);
        void wheelEvent(QWheelEvent *event);

        //void mouseMoveEvent(QMouseEvent *event);

        /**
         * Get the currently active game.
         * @return The currently active game, or nullptr, if none was selected, or the index is incorrect.
         */
        GameModel *getActiveGame()
        {
            if (mActiveGame >= 0 && mActiveGame < static_cast<int>(mGames.size()))
                return mGames[mActiveGame];

            return nullptr;
        }

        /**
         * Get game on the given index.
         * @param index Index of the game.
         * @return Pointer to a GameModel, or nullptr.
         */
        GameModel *getGame(int index)
        {
            if (mActiveGame >= 0 && mActiveGame < static_cast<int>(mGames.size()))
                return mGames[index];

            return nullptr;
        }

        /**
         * Initialize the OpenGL context and required structures.
         */
        void initializeGL();

        /**
         * Resize the OpenGL viewport.
         * @param width The new width.
         * @param height The new height.
         */
        void resizeGL(int width, int height);

        /**
         * Draw something...
         */
        void paintGL();

        /**
         * Deinitialize the OpenGL context and other structures.
         */
        void deinitializeGL();

        /// Timer used to plan updates.
        QTimer mUpdateTimer;
        /// Timer used to plan rendering.
        QTimer mRenderTimer;

        /// Basic rendering shader program.
        QOpenGLShaderProgram *mProgram;
        /// Shader program for text rendering.
        QOpenGLShaderProgram *mTextProgram;
        /// Basic rendering shader program.
        QOpenGLShaderProgram *mShadingProgram;

        /// Distance of the camera from the target point.
        float mCameraDistance{2.0f};
        /// The main camera.
        gui::Camera mCamera;
        /// Position of the light.
        QVector3D mLightPosition;
        /// The view matrix from the lights position.
        QMatrix4x4 mLightViewMatrix;

        /// The projection matrix for regular objects.
        QMatrix4x4 mProjectionMatrix;
        /// The projection matrix for text objects.
        QMatrix4x4 mTextProjectionMatrix;
        /// The projection matrix for shading.
        QMatrix4x4 mShadingProjectionMatrix;

        /// Empty VAO, fixes no rendering.
        QOpenGLVertexArrayObject mVAO;

        /// Framebuffer used for rendering the shadow map.
        QOpenGLFramebufferObject *mShadowFB{nullptr};

        /// Vector containing separate game instances.
        std::vector<gui::GameModel*> mGames;
        /// Currently active game. -1 = no game.
        int mActiveGame{-1};

        /// Changes the coordinate system from <-1, 1> to <0, 1> and also inverts the x axis.
        static const QMatrix4x4 SHADOW_BIAS;

        bool mFirst{false};
    protected:
    };
}

#endif //HRA2016_GAMEVIEWER_H
