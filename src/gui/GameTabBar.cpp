/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "GameTabBar.h"

namespace gui
{
    GameTabBar::GameTabBar(QWidget *parent) :
        QTabBar(parent)
    {
        setTabsClosable(true);
        setShape(Shape::RoundedNorth);
        setStyleSheet("QTabWidget::pane {\n"
                      "    border-top: 2px solid #C2070B;\n"
                      "    position: absolute;\n"
                      "    top: -0.5em;\n"
                      "}\n"
                      "QTabWidget::tab-bar {\n"
                      "    alignment: center;\n"
                      "}\n"
                      "QTabBar::tab {\n"
                      "    border: 2px solid ;\n"
                      "    border-top-left-radius: 4px;\n"
                      "    border-top-right-radius: 4px;\n"
                      "    min-width: 8ex;\n"
                      "    padding: 2px;\n"
                      "}\n"
                      "QTabBar::tab:selected {\n"
                      "    border-color: #0B3E0B;\n"
                      "    border-bottom-color: #0B3E0B;\n"
                      "}");

        connect(this, SIGNAL(currentChanged(int)), parent, SLOT(changedTab(int)));
        connect(this, SIGNAL(tabCloseRequested(int)), parent, SLOT(closeTab(int)));
    }

}
