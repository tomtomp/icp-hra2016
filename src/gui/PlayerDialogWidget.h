/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_PLAYERDIALOGWIDGET_H
#define HRA2016_PLAYERDIALOGWIDGET_H

#include "../core/Game.h"

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QGroupBox>

namespace gui
{
    class PlayerDialogWidget : public QGroupBox
    {
        Q_OBJECT

    public:
        /**
         * Create a part of the new game dialog with questions about the player.
         * @param parent Parent ptr.
         * @param gamePtr Pointer to the game, used for getting the available options.
         */
        explicit PlayerDialogWidget(QWidget *parent, const core::Game *gamePtr, QString name);

        /**
         * Get name from the nameLineEdit.
         * @return The name.
         */
        QString getName() const
        {
            return nameLineEdit->text();
        }

        /**
         * Get the index of the chosen player type.
         * @return The index.
         */
        std::size_t getType() const
        {
            return static_cast<std::size_t>(playerTypeCombo->currentIndex());
        }

        /**
         * Get the index of the chosen ai type.
         * @return The index.
         */
        std::size_t getAiType() const
        {
            return static_cast<std::size_t>(aiTypeCombo->currentIndex());
        }

        ~PlayerDialogWidget();
    private slots:
        void playerTypeChange(const QString &text);
    private:
        /// Components
        QLabel *nameLabel{nullptr};
        QLineEdit *nameLineEdit{nullptr};
        QLabel *playerTypeLabel{nullptr};
        QComboBox *playerTypeCombo{nullptr};
        QLabel *aiTypeLabel{nullptr};
        QComboBox *aiTypeCombo{nullptr};
        QGridLayout *mainLayout{nullptr};
        /// End of components
    protected:
    };
}

#endif //HRA2016_PLAYERDIALOGWIDGET_H
