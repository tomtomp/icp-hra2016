/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAMEWIDGET_H
#define HRA2016_GAMEWIDGET_H

#include "GameTabBar.h"
#include "GameViewer.h"
#include "render/RenderableObject.h"

#include <QWidget>
#include <QVBoxLayout>
#include <QTimer>
#include <QFileDialog>

namespace gui
{
    /**
     * Main game widget.
     */
    class GameWidget : public QWidget
    {
        Q_OBJECT

    public:
        /**
         * Construct new game widget.
         * @parent Parent ptr.
         */
        GameWidget(QWidget *parent = nullptr);

        ~GameWidget();
    public slots:
        void changedTab(int index);
        void closeTab(int index);
        void newGame();
        void loadGame();
        void saveGame();
        void undo();
        void redo();
    private:

        /// Total number of games in this instance.
        std::size_t mNumberOfGames{0};

        /// Components
        gui::GameTabBar *gameTabBar{nullptr};
        gui::GameViewer *gameViewer{nullptr};
        QVBoxLayout *mainLayout{nullptr};
        /// End of components
    protected:
    };
}

#endif //HRA2016_GAMEWIDGET_H
