/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "NewGameDialog.h"

namespace gui
{
    NewGameDialog::NewGameDialog(QWidget *parent, const core::Game *gamePtr) :
        QDialog(parent),
        mGamePtr(gamePtr)
    {
        sizeLabel = new QLabel(tr("Size: "));
        sizeComboBox = new QComboBox();
        for (std::size_t sizeOption : gamePtr->getBoardSizes())
        {
            QString numStr = QString::number(sizeOption);
            sizeComboBox->addItem(numStr + "x" + numStr);
        }

        okButton = new QPushButton(tr("OK"));
        connect(okButton, &QPushButton::clicked, this, &NewGameDialog::okPressed);

        cancelButton = new QPushButton(tr("Cancel"));
        connect(cancelButton, &QPushButton::clicked, this, &NewGameDialog::cancelPressed);

        playerDialog1 = new gui::PlayerDialogWidget(this, gamePtr, tr("Player 1"));

        playerDialog2 = new gui::PlayerDialogWidget(this, gamePtr, tr("Player 2"));

        mainLayout = new QVBoxLayout;
        sizeLayout = new QHBoxLayout;
        playersLayout = new QHBoxLayout;
        buttonLayout = new QHBoxLayout;

        sizeLayout->addWidget(sizeLabel);
        sizeLayout->addWidget(sizeComboBox);

        playersLayout->addWidget(playerDialog1);
        playersLayout->addWidget(playerDialog2);

        buttonLayout->addStretch(1);
        buttonLayout->addWidget(okButton);
        buttonLayout->addWidget(cancelButton);

        mainLayout->addLayout(sizeLayout);
        mainLayout->addLayout(playersLayout);
        mainLayout->addLayout(buttonLayout);

        setLayout(mainLayout);

        setFixedSize(600, 400);
    }

    NewGameDialog::~NewGameDialog()
    {
        delete mainLayout;
    }

    void NewGameDialog::okPressed()
    {
        if (!verifyForm())
            QMessageBox::warning(this, tr("Error in form."),
                                 tr("There is an error in the form, please check it and try again!"));
        else
        {
            close();
            setResult(QDialog::Accepted);
        }
    }

    void NewGameDialog::cancelPressed()
    {
        close();
        setResult(QDialog::Rejected);
    }

    bool NewGameDialog::verifyForm() const
    {
        if (sizeComboBox->currentIndex() < 0)
            return false;

        int typeIndex = playerDialog1->getType();
        if (typeIndex < 0)
            return false;
        if (mGamePtr->getPlayerTypes()[typeIndex] == "AI" && playerDialog1->getAiType() < 0)
            return false;
        if (!mGamePtr->validateName(playerDialog1->getName().toStdString()))
            return false;

        typeIndex = playerDialog2->getType();
        if (typeIndex < 0)
            return false;
        if (mGamePtr->getPlayerTypes()[typeIndex] == "AI" && playerDialog2->getAiType() < 0)
            return false;
        if (!mGamePtr->validateName(playerDialog2->getName().toStdString()))
            return false;

        return true;
    }
}
