/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "MainWindow.h"

namespace gui
{
    MainWindow::MainWindow() :
        gameWidget(new gui::GameWidget(this))
    {
        createMenus();

        setCentralWidget(gameWidget);
    }

    MainWindow::~MainWindow()
    {
        delete newGameAction;
        delete loadGameAction;
        delete exitAction;
        delete undoGameAction;
        delete redoGameAction;
        delete saveGameAction;
        delete aboutAction;
        delete aboutQtAction;
        delete helpAction;
    }

    void MainWindow::createMenus()
    {
        QMenu *gameMenu = menuBar()->addMenu(tr("&Game"));

        const QIcon newGameIcon = QIcon::fromTheme("applications-games", QIcon(":/new.png"));
        newGameAction = new QAction(newGameIcon, tr("&New Game"), this);
        newGameAction->setShortcuts(QKeySequence::New);
        newGameAction->setStatusTip(tr("Create a new game"));
        connect(newGameAction, &QAction::triggered, gameWidget, &GameWidget::newGame);

        const QIcon loadGameIcon = QIcon::fromTheme("document-open", QIcon(":/open.png"));
        loadGameAction = new QAction(loadGameIcon, tr("L&oad Game"), this);
        loadGameAction->setShortcuts(QKeySequence::Open);
        loadGameAction->setStatusTip(tr("Load a new game"));
        connect(loadGameAction, &QAction::triggered, gameWidget, &GameWidget::loadGame);

        const QIcon exitIcon = QIcon::fromTheme("application-exit", QIcon(":/exit.png"));
        exitAction = new QAction(exitIcon, tr("&Exit"), this);
        exitAction->setShortcuts(QKeySequence::Quit);
        exitAction->setStatusTip(tr("Exit the application"));
        connect(exitAction, &QAction::triggered, this, &MainWindow::close);

        gameMenu->addAction(newGameAction);
        gameMenu->addAction(loadGameAction);
        gameMenu->addAction(exitAction);

        QToolBar *gameToolBar = addToolBar(tr("Game"));
        gameToolBar->setMovable(false);

        const QIcon undoGameIcon = QIcon::fromTheme("edit-undo", QIcon(":/undo.png"));
        undoGameAction = new QAction(undoGameIcon, tr("&Undo"), this);
        undoGameAction->setShortcuts(QKeySequence::Undo);
        undoGameAction->setStatusTip(tr("Undo game by one step"));
        connect(undoGameAction, &QAction::triggered, gameWidget, &GameWidget::undo);
        //undoGameAction->setDisabled(true);

        const QIcon redoGameIcon = QIcon::fromTheme("edit-redo", QIcon(":/redo.png"));
        redoGameAction = new QAction(redoGameIcon, tr("&Redo"), this);
        redoGameAction->setShortcuts(QKeySequence::Redo);
        redoGameAction->setStatusTip(tr("Redo game by one step"));
        connect(redoGameAction, &QAction::triggered, gameWidget, &GameWidget::redo);
        //redoGameAction->setDisabled(true);

        const QIcon saveGameIcon = QIcon::fromTheme("document-save", QIcon(":/save.png"));
        saveGameAction = new QAction(saveGameIcon, tr("&Save"), this);
        saveGameAction->setShortcuts(QKeySequence::Save);
        saveGameAction->setStatusTip(tr("Save the game"));
        connect(saveGameAction, &QAction::triggered, gameWidget, &GameWidget::saveGame);
        //saveGameAction->setDisabled(true);

        gameToolBar->addAction(undoGameAction);
        gameToolBar->addAction(redoGameAction);
        gameToolBar->addAction(saveGameAction);

        QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

        const QIcon aboutIcon = QIcon::fromTheme("help-about", QIcon(":/about.png"));
        aboutAction = new QAction(aboutIcon, tr("&About"), this);
        aboutAction->setStatusTip(tr("Information about this application"));
        connect(aboutAction, &QAction::triggered, this, &MainWindow::about);

        const QIcon aboutQtIcon = QIcon::fromTheme("help-about", QIcon(":/aboutQt.png"));
        aboutQtAction = new QAction(aboutQtIcon, tr("&About Qt"), this);
        aboutQtAction->setStatusTip(tr("Information about Qt"));
        connect(aboutQtAction, &QAction::triggered, this, &MainWindow::aboutQt);

        const QIcon helpIcon = QIcon::fromTheme("help-about", QIcon(":/about.png"));
        helpAction = new QAction(helpIcon, tr("&Help"), this);
        helpAction->setStatusTip(tr("Rules of this game"));
        connect(helpAction, &QAction::triggered, this, &MainWindow::help);

        helpMenu->addAction(aboutAction);
        helpMenu->addAction(aboutQtAction);
        helpMenu->addAction(helpAction);
    }

    void MainWindow::newGame()
    {
        std::cout << "New Game pressed" << std::endl;
        core::Game game(nullptr);
        gui::NewGameDialog ngd(this, &game);
        if (ngd.exec() == QDialog::Accepted)
            std::cout << "Dialog accepted" << std::endl;
        else
            std::cout << "Dialog NOT accepted" << std::endl;
    }

    void MainWindow::loadGame()
    {
        std::cout << "Load Game pressed" << std::endl;
    }

    void MainWindow::closeEvent(QCloseEvent *event)
    {
        std::cout << "Close event triggered." << std::endl;
        QWidget::closeEvent(event);
    }

    void MainWindow::undoGame()
    {
        std::cout << "Undo Game pressed" << std::endl;
    }

    void MainWindow::redoGame()
    {
        std::cout << "Redo Game pressed" << std::endl;
    }

    void MainWindow::saveGame()
    {
        std::cout << "Save Game pressed" << std::endl;
    }

    void MainWindow::about()
    {
        QMessageBox::about(this, tr("About Application"),
                           tr("This is an implementation of <b>Reversi</b> board game. It was created as "
                              "a part of ICP course on VUTBR Faculty of Information Technology.<br>"
                              "Authors : <br>"
                              "xpokor67 - Jan Pokorný - xpokor67@fit.stud.vutbr.cz<br>"
                              "xpolas34 - Tomáš Polášek - xpolas34@fit.stud.vutbr.cz"));
    }

    void MainWindow::aboutQt()
    {
        QMessageBox::aboutQt(this, tr("About Qt"));
    }

    void MainWindow::help()
    {
        QMessageBox::about(this, tr("Help"),
                           tr("Application help menu.<br>"
                              "This is a Reversi board game implementation.<br>"
                              "The game window is divided into 4 areas, theese are : <br>"
                              "\t<b>Menu bar</b> - This bar contains 2 sub-menus (<b>Game</b> and <b>Help</b>).<br>"
                                  "\t\tThe <b>Game</b> menu contains actions to create a new game, load saved game and "
                                  "exit the application.<br>"
                                  "\t\tThe <b>Help</b> menu contains access to this dialog and 2 about dialogs, that "
                                  "contain information about creation of this application.<br>"
                              "\t<b>Command bar</b> - This bar has 3 buttons - Undo, Redo and Save game. Theese buttons "
                                  "only work, when a game is active.<br>"
                              "\t<b>Tab task bar</b> - The currently opened games will be displayed on this bar. The "
                                  "active game will have a green line aroung the tab.<br>"
                              "\t<b>Game display</b> - This is the central part of this application, that displays the "
                                  "current state of the selected game.<br>"
                                  "\t\tThe mouse wheel can be used to zoom onto the playing field."
                                  "\t\tIf no game is selected, then the display will be black.<br>"
                                  "\t\tAfter selecting a game, the display will contain information about it.<br>"
                                  "\t\tThe bottom left contains 3 text areas, that contain information about the current "
                                  "state of the game.<br>"
                                  "\t\t\t<b>Player text</b> - The player text says, whose move it currently is.<br>"
                                  "\t\t\t<b>Status text</b> - This text signalizes, what is the game doing/expecting from "
                                  "the player.<br>"
                                  "\t\t\t<b>Score text</b> - The scores are displayed here.<br>"
                                  "\t\tThe game is controlled by clicking on the positions, the player wants to put "
                                  "their stone on.<br>")
        );
    }
}
