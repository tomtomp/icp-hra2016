/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_NEWGAMEDIALOG_H
#define HRA2016_NEWGAMEDIALOG_H

#include "../core/Game.h"
#include "PlayerDialogWidget.h"

#include <QDialog>
#include <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>

namespace gui
{
    /**
     * New game Dialog.
     */
    class NewGameDialog : public QDialog
    {
        Q_OBJECT

    public:
        /**
         * Construct this dialog and ask the user to fill it.
         * @param parent The parent ptr.
         * @param gamePtr Pointer to the game, used for getting the available options.
         */
        explicit NewGameDialog(QWidget *parent, const core::Game *gamePtr);

        /**
         * Get index of the chosen size.
         * @return The index of the chosen size, or -1, if no size was chosen.
         */
        std::size_t getSizeIndex() const
        {
            return static_cast<std::size_t>(sizeComboBox->currentIndex());
        }

        /**
         * Get the player 1 dialog.
         * @return The dialog for player 1.
         */
        gui::PlayerDialogWidget *getPlayerDialog1() const
        {
            return playerDialog1;
        }

        /**
         * Get the player 2 dialog.
         * @return The dialog for player 2.
         */
        gui::PlayerDialogWidget *getPlayerDialog2() const
        {
            return playerDialog2;
        }

        ~NewGameDialog();

    private slots:
        void okPressed();
        void cancelPressed();
    private:
        /**
         * Verify the data in this form.
         * @return Returns true, if everything is ok.
         */
        bool verifyForm() const;

        /// Game, used for getting choices and verification.
        const core::Game *mGamePtr{nullptr};

        /// Dialog for the first player.
        gui::PlayerDialogWidget *playerDialog1;
        /// Dialog for the second player.
        gui::PlayerDialogWidget *playerDialog2;
        /// Components
        QLabel *sizeLabel{nullptr};
        QComboBox *sizeComboBox{nullptr};
        QPushButton *okButton{nullptr};
        QPushButton *cancelButton{nullptr};
        QHBoxLayout *sizeLayout{nullptr};
        QHBoxLayout *playersLayout{nullptr};
        QHBoxLayout *buttonLayout{nullptr};
        QVBoxLayout *mainLayout{nullptr};
        /// End of cComponents
    protected:
    };
}

#endif //HRA2016_NEWGAMEDIALOG_H
