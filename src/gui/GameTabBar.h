/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAMETABBAR_H
#define HRA2016_GAMETABBAR_H

#include <QTabBar>
#include <QPalette>

namespace gui
{
    /**
     * Tab manager for game viewers.
     */
    class GameTabBar : public QTabBar
    {
        Q_OBJECT

    public:
        /**
         * Construct new game tab widget.
         * @param parent The parent ptr.
         */
        explicit GameTabBar(QWidget *parent);
    private:
    protected:
    };
}

#endif //HRA2016_GAMETABBAR_H
