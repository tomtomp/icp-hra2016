/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "GameWidget.h"

namespace gui
{
    GameWidget::GameWidget(QWidget *parent) :
        QWidget(parent),
        gameTabBar(new gui::GameTabBar(this)),
        gameViewer(new gui::GameViewer(this))
    {
        mainLayout = new QVBoxLayout;

        mainLayout->addWidget(gameTabBar);
        mainLayout->addWidget(gameViewer);

        setLayout(mainLayout);
    }

    GameWidget::~GameWidget()
    {
        delete mainLayout;
    }

    void GameWidget::changedTab(int index)
    {
        gameViewer->setActiveGame(index);
    }

    void GameWidget::closeTab(int index)
    {
        if (gameViewer->closeGame(index))
            gameTabBar->removeTab(index);
    }

    void GameWidget::newGame()
    {
        if (gameViewer->createNewGame())
            gameTabBar->addTab(tr("Game ") + QString::number(++mNumberOfGames));
        gameTabBar->setCurrentIndex(gameTabBar->count() - 1);
    }

    void GameWidget::loadGame()
    {
        QFileDialog fileDialog(this, tr("Please select saved game"), "", tr("Saved game (*.gs)"));
        fileDialog.setFileMode(QFileDialog::ExistingFile);
        fileDialog.setOption(QFileDialog::DontUseNativeDialog, true);

        if (!fileDialog.exec())
            return;

        QString filename = fileDialog.selectedFiles()[0];

        /*
        QString filename = QFileDialog::getOpenFileName(this, tr("Please select the saved game"), "",
                                                        tr("Saved game (*.gs)"));
        */

        if (!filename.isEmpty() && gameViewer->createNewGame(filename))
            gameTabBar->addTab(tr("Game ") + QString::number(++mNumberOfGames));
        gameTabBar->setCurrentIndex(gameTabBar->count() - 1);
    }
    void GameWidget::saveGame()
    {
        gameViewer->saveGame();
    }

    void GameWidget::undo()
    {
        gameViewer->undo();
    }

    void GameWidget::redo()
    {
        gameViewer->redo();
    }
}
