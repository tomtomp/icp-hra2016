/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_CAMERA_H
#define HRA2016_CAMERA_H

#include "GameObject.h"

#include <QMatrix4x4>

namespace gui
{
    /**
     * This class represents a camera.
     */
    class Camera
    {
    public:
        /**
         * Create a camera with given parameters.
         * @param location Location of the camera.
         * @param up Up vector of the camera.
         * @param target What is the camera looking at.
         */
        Camera(const QVector3D location, const QVector3D &up, const QVector3D &target) :
            mViewMatrix(),
            mLocation(location),
            mUp(up),
            mTarget(target)
        {
            lookAt(mLocation, mUp, mTarget);
        }

        /**
         * Make the camera look at given point.
         * @param location New location of this camera.
         * @param up New up vector.
         * @param target New target.
         */
        void lookAt(const QVector3D location, const QVector3D &up, const QVector3D &target);

        /**
         * Set the location of this camera to given point.
         * @param location The new location.
         */
        void setLocation(const QVector3D location);

        /**
         * Move the camera.
         * @param move Movement vector.
         */
        void move(const QVector3D move);

        /**
         * Get the location of this camera.
         * @return The location of this camera.
         */
        const QVector3D &getLocation() const;

        /**
         * Set the target of this camera to given point.
         * @param target The new target.
         */
        void setTarget(const QVector3D target);

        /**
         * Move the target of this camera in given direction.
         * @param targetMove Target movement vector.
         */
        void moveTarget(const QVector3D targetMove);

        /**
         * Get the target of this camera.
         * @return The target of this camera.
         */
        const QVector3D &getTarget() const;

        /**
         * Set the up of this camera to given point.
         * @param up The new up vector.
         */
        void setUp(const QVector3D up);

        /**
         * Get the up vector of this camera.
         * @return The up vector of this camera.
         */
        const QVector3D &getUp() const;

        /**
         * Get the view matrix from this camera.
         * @return View matrix of this camera.
         */
        const QMatrix4x4 &getMatrix();
    private:
        /// The view matrix.
        QMatrix4x4 mViewMatrix;

        /// Location of the camera.
        QVector3D mLocation;

        /// Up vector.
        QVector3D mUp;

        /// Point, the camera is looking at.
        QVector3D mTarget;

        /// Is the recalculation of view matrix required?
        bool mDirty{false};
    protected:
    };

    inline void Camera::setLocation(const QVector3D location)
    {
        mDirty = true;
        mLocation = location;
    }

    inline void Camera::move(const QVector3D move)
    {
        mDirty = true;
        mLocation += move;
    }

    inline const QVector3D &Camera::getLocation() const
    {
        return mLocation;
    }

    inline const QMatrix4x4 &Camera::getMatrix()
    {
        if (mDirty)
        {
            mViewMatrix.setToIdentity();
            mViewMatrix.lookAt(mLocation, mTarget, mUp);
            mDirty = false;
        }

        return mViewMatrix;
    }

    inline void Camera::setTarget(const QVector3D target)
    {
        mDirty = true;
        mTarget = target;
    }

    inline void Camera::moveTarget(const QVector3D targetMove)
    {
        mDirty = true;
        mTarget += targetMove;
    }

    inline const QVector3D &Camera::getTarget() const
    {
        return mTarget;
    }

    inline void Camera::setUp(const QVector3D up)
    {
        mDirty = true;
        mUp = up;
    }

    inline const QVector3D &Camera::getUp() const
    {
       return mUp;
    }

    inline void Camera::lookAt(const QVector3D location, const QVector3D &up, const QVector3D &target)
    {
        mDirty = true;
        mLocation = location;
        mUp = up;
        mTarget = target;
    }
}

#endif //HRA2016_CAMERA_H
