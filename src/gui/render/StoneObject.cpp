/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "StoneObject.h"

namespace gui
{
    StoneObject::StoneObject(const QVector3D &location) :
        GameObject(location),
        mColor{StoneColor::BLACK}
    {
    }

    StoneObject::~StoneObject()
    {

    }
}
