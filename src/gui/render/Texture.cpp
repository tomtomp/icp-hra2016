/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Texture.h"

namespace gui
{
    Texture::Texture(const QString &path) :
        mTextureBuffer(QImage(path), QOpenGLTexture::MipMapGeneration::DontGenerateMipMaps)
    {
        setParameters();
    }

    Texture::Texture(const QImage &image) :
        mTextureBuffer(image, QOpenGLTexture::MipMapGeneration::DontGenerateMipMaps)
    {
        setParameters();
    }

    void Texture::use(QOpenGLFunctions *fun, QOpenGLShaderProgram *program)
    {
        mTextureBuffer.bind(3);
        program->setUniformValue("textureSampler", 3);
    }

    void Texture::unuse()
    {
        mTextureBuffer.release();
    }

    Texture::~Texture()
    {
        mTextureBuffer.destroy();
    }

    void Texture::updateTexture(const QImage &img)
    {
        mTextureBuffer.destroy();
        mTextureBuffer.create();
        setParameters();
        mTextureBuffer.setData(img, QOpenGLTexture::MipMapGeneration::DontGenerateMipMaps);
    }

    void Texture::setParameters()
    {
        mTextureBuffer.setMinificationFilter(QOpenGLTexture::Nearest);
        mTextureBuffer.setMagnificationFilter(QOpenGLTexture::Nearest);
        mTextureBuffer.setWrapMode(QOpenGLTexture::ClampToEdge);
    }

    void Texture::setWrap(QOpenGLTexture::WrapMode wrapMode)
    {
        mTextureBuffer.setWrapMode(wrapMode);
    }
}
