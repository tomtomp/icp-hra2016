/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_MESHLOADER_H
#define HRA2016_MESHLOADER_H

#include "Vertex.h"

#include <string>
#include <cstring>
#include <iostream>

#include <QFile>
#include <QTextStream>

namespace gui
{
    /**
     * PLY mesh loader.
     */
    class MeshLoader
    {
    public:
        /**
         * Construct and load this mesh from given file.
         * @param path Filename of the model (.ply) .
         */
        MeshLoader(const QString &path);

        /**
         * Wrapper for already loaded, or predefined data.
         */
        MeshLoader(const std::vector<gui::Vertex> &vertexData, const std::vector<unsigned int> &indices) :
            mVertexData{vertexData},
            mIndices{indices}
        {}

        /**
         * Get the vertex data.
         * @return The vertex data.
         */
        const std::vector<gui::Vertex> getVertexData() const;

        /**
         * Get the indices.
         * @return The indices.
         */
        const std::vector<unsigned int> getIndices() const;
    private:
        /**
         * Load the .ply model.
         * @param path Path to the model.
         */
        void loadModel(const QString &path);

        /**
         * Parse the given line and create a vertex from it.
         * @param line Line to parse.
         * @return The generated vertex.
         */
        gui::Vertex parseLine(const QByteArray &line);

        /**
         * Add the faces from given line.
         * @param line Line to parse.
         */
        void addFaces(const QByteArray &line);

        /**
         * Check, if given string starts with search string.
         * @param str String to look for the "search" string in.
         * @param search The string we are looking for.
         * @return Returns true, if the string begins with another string.
         */
        static bool strStartsWith(const std::string &str, const std::string &search);

        /// Vector containing vertex data of this model.
        std::vector<gui::Vertex> mVertexData;
        /// Vector containing indices for this model.
        std::vector<unsigned int> mIndices;
    protected:
    };
}

#endif //HRA2016_MESHLOADER_H
