/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_COLLECTIONOBJECT_H
#define HRA2016_COLLECTIONOBJECT_H

#include "GameObject.h"

namespace gui
{
    class CollectionObject : public GameObject
    {
    public:
    private:
    protected:
    };
}

#endif //HRA2016_COLLECTIONOBJECT_H
