/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "MeshLoader.h"

namespace gui {
    MeshLoader::MeshLoader(const QString &path)
    {
        loadModel(path);
    }

    void MeshLoader::loadModel(const QString &path)
    {
        QFile input(path);
        QByteArray line;

        if (!input.open(QIODevice::ReadOnly | QIODevice::Text))
            throw std::runtime_error(std::string("Error, cannot open mesh resource. ") + path.toStdString());

        line = input.readLine();

        //while (!boost::starts_with(line, "element vertex ") && !input.atEnd())
        while (!strStartsWith(line.toStdString(), "element vertex ") && !input.atEnd())
            line = input.readLine();

        if (input.atEnd())
            throw std::runtime_error(std::string("Not enough data in mesh file. ") + path.toStdString());

        QString vertexNumStr(line.cbegin() + std::strlen("element vertex "));
        bool conversionOk{false};
        int vertexNum{vertexNumStr.toInt(&conversionOk)};
        if (!conversionOk)
            throw std::runtime_error(std::string("Cannot convert number of vertexes to number. ") + path.toStdString());

        //while (!boost::starts_with(line, "element face ") && !input.atEnd())
        while (!strStartsWith(line.toStdString(), "element face ") && !input.atEnd())
            line = input.readLine();

        if (input.atEnd())
            throw std::runtime_error(std::string("Not enough data in mesh file. ") + path.toStdString());

        QString faceNumStr(line.cbegin() + std::strlen("element face "));
        int faceNum{faceNumStr.toInt(&conversionOk)};
        if (!conversionOk)
            throw std::runtime_error(std::string("Cannot convert number of faces to number. ") + path.toStdString());

        while (line != "end_header\n" && !input.atEnd())
            line = input.readLine();

        if (input.atEnd())
            throw std::runtime_error(std::string("Not enough data in mesh file. ") + path.toStdString());

        int readVertexes;
        for (readVertexes = 0; readVertexes < vertexNum && !input.atEnd(); ++readVertexes) {
            line = input.readLine();
            mVertexData.push_back(parseLine(line));
        }

        if (readVertexes != vertexNum)
            throw std::runtime_error(std::string("Couldn't read all the vertexes. ") + path.toStdString());

        int readFaces;
        for (readFaces = 0; readFaces < faceNum && !input.atEnd(); ++readFaces) {
            line = input.readLine();
            addFaces(line);
        }

        if (readFaces != faceNum)
            throw std::runtime_error(std::string("Couldn't read all the faces. ") + path.toStdString());
    }

    gui::Vertex MeshLoader::parseLine(const QByteArray &line)
    {
        QTextStream input(line);

        float pX{0}, pY{0}, pZ{0};
        float nX{0}, nY{0}, nZ{0};
        float tU{0}, tV{0};

        input >> pX >> pY >> pZ >> nX >> nY >> nZ >> tU >> tV;

        return gui::Vertex({pX, pY, pZ}, {nX, nY, nZ}, {tU, tV});
    }

    void MeshLoader::addFaces(const QByteArray &line)
    {
        QTextStream input(line);

        int indexNum{0};
        std::vector<unsigned int> indices;
        unsigned int index;

        input >> indexNum;

        if (indexNum != 3 && indexNum != 4)
            throw std::runtime_error(std::string("Only 3 and 4 indices are supported."));

        int numRead;
        for (numRead = 0; numRead < indexNum; ++numRead)
        {
            input >> index;
            indices.push_back(index);
        }

        if (numRead != indexNum)
            throw std::runtime_error(std::string("Not all indices could be read."));

        // 1->2->3
        mIndices.push_back(indices[0]);
        mIndices.push_back(indices[1]);
        mIndices.push_back(indices[2]);

        if (indexNum == 4)
        {
            // 1->3->4
            mIndices.push_back(indices[0]);
            mIndices.push_back(indices[2]);
            mIndices.push_back(indices[3]);
        }
    }

    const std::vector<Vertex> MeshLoader::getVertexData() const
    {
        return mVertexData;
    }

    const std::vector<unsigned int> MeshLoader::getIndices() const
    {
        return mIndices;
    }

    bool MeshLoader::strStartsWith(const std::string &str, const std::string &search)
    {
        if (str.length() >= search.length())
        {
            return str.compare(0, search.length(), search) == 0;
        }
        else
            return false;
    }


}
