/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Texture.h"
#include "Mesh.h"
#include "RenderableObject.h"

#ifndef HRA2016_RENDERABLECOLLECTION_H
#define HRA2016_RENDERABLECOLLECTION_H

namespace gui
{
    /**
     * Collection of objects with the same texture and mesh, but differing positions.
     */
    class RenderableCollection : public RenderableObject
    {
    public:
        RenderableCollection(const RenderableCollection &other) = delete;
        RenderableCollection(RenderableCollection &&other) = delete;

        /**
         * Create new renderable collection with given visual parameters and location.
         * @param meshPtr Pointer to the mesh.
         * @param texturePath Path to the texture.
         * @param program The Shader program used to display this object.
         * @param location Location of this object in 3D space.
         */
        RenderableCollection(gui::Mesh *meshPtr, gui::Texture *texturePtr, const QVector3D &location = {0.0, 0.0, 0.0});

        /**
         * Render this collection of objects.
         * @param fun OpenGL functions of current context.
         * @param program OpenGL shader program to render with.
         * @param vp View + projection matrix.
         * @param shadowVP View-projection matrix used for shadow mapping.
         */
        virtual void render(QOpenGLFunctions *fun, QOpenGLShaderProgram *program, const QMatrix4x4 &vp,
                            const QMatrix4x4 &shadowVP = QMatrix4x4());

        /**
         * Add new object to this collection on specified coordinates.
         * @param obj Object to add to this collection.
         */
        void addObject(gui::GameObject *obj);

        /**
         * Get the objects vector of this collection
         * @return The objects vector of this collection.
         */
        const std::vector<gui::GameObject*> &getObjects() const
        {
            return mObjects;
        }

        /**
         * Get the objects vector of this collection
         * @return The objects vector of this collection.
         */
        std::vector<gui::GameObject*> &getObjects()
        {
            return mObjects;
        }

        virtual ~RenderableCollection();
    private:
    protected:
        /// Vector of objects in this collection.
        std::vector<gui::GameObject*> mObjects;
    };
}

#endif //HRA2016_RENDERABLECOLLECTION_H
