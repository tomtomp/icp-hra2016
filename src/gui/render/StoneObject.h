/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_STONEOBJECT_H
#define HRA2016_STONEOBJECT_H

#include "GameObject.h"

namespace gui
{
    /**
     * Colors of the stones in game.
     */
    enum class StoneColor
    {
        WHITE = 0,
        BLACK = 1
    };

    /**
     * Object representing a single stone in the game.
     */
    class StoneObject : virtual public GameObject
    {
    public:
        /**
         * Create a new stone object on given position.
         * @param location The location of the stone.
         */
        StoneObject(const QVector3D &location = {0.0, 0.0, 0.0});

        /**
         * Set the color of this stone to the given one.
         * @param color The new color.
         */
        void setColor(StoneColor color)
        {
            if (color == StoneColor::BLACK)
                getTrans().setRotate(QQuaternion());
            else
                getTrans().setRotate(QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, 180.0f));


            mColor = color;
        }

        /**
         * Flip this stone to the other side.
         */
        void flipStone()
        {
            getTrans().rotate(QQuaternion::fromEulerAngles(0.0f, 0.0f, 90.0f));

            switch(mColor)
            {
                case StoneColor::WHITE:
                    mColor = StoneColor::BLACK;
                    break;
                case StoneColor::BLACK:
                    mColor = StoneColor::WHITE;
                    break;
            }
        }

        /**
         * Get the color of this stone.
         * @return The color of this stone.
         */
        StoneColor getColor() const
        {
            return mColor;
        }

        virtual ~StoneObject();
    private:
        /// Color of this stone.
        StoneColor mColor;
    protected:
    };
}

#endif //HRA2016_STONEOBJECT_H
