/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Mesh.h"

namespace gui
{
    Mesh::Mesh(const QString &path) :
        mMesh(new gui::MeshLoader(path)),
        mVertexBuffer(QOpenGLBuffer::VertexBuffer),
        mIndexBuffer(QOpenGLBuffer::IndexBuffer)
    {
        initGL();
    }

    Mesh::Mesh(const std::vector<Vertex> &vertexData, const std::vector<unsigned int> &indices) :
        mMesh(new gui::MeshLoader(vertexData, indices)),
        mVertexBuffer(QOpenGLBuffer::VertexBuffer),
        mIndexBuffer(QOpenGLBuffer::IndexBuffer)
    {
        initGL();
    }

    void Mesh::render(QOpenGLFunctions *fun, QOpenGLShaderProgram *program)
    {
        mVertexBuffer.bind();
        //mIndexBuffer.bind();

        // Position
        program->enableAttributeArray(0);
        program->setAttributeBuffer(0, GL_FLOAT, gui::Vertex::getPositionOffset(),
                                    gui::Vertex::POSITION_TUPLE_SIZE, gui::Vertex::getStride());

        // Normal
        program->enableAttributeArray(1);
        program->setAttributeBuffer(1, GL_FLOAT, gui::Vertex::getNormalOffset(),
                                    gui::Vertex::NORMAL_TUPLE_SIZE, gui::Vertex::getStride());

        // Texture UV
        program->enableAttributeArray(2);
        program->setAttributeBuffer(2, GL_FLOAT, gui::Vertex::getTextureOffset(),
                                    gui::Vertex::TEXTURE_TUPLE_SIZE, gui::Vertex::getStride());

        //fun->glDrawElements(GL_TRIANGLE_STRIP, mMesh.getIndices().size(), GL_UNSIGNED_INT, 0);
        fun->glDrawElements(GL_TRIANGLES, mMesh->getIndices().size(), GL_UNSIGNED_INT, &mMesh->getIndices()[0]);

        //mIndexBuffer.release();
        mVertexBuffer.release();
    }

    Mesh::~Mesh()
    {
        mIndexBuffer.destroy();
        mVertexBuffer.destroy();
        if (mMesh)
            delete mMesh;
    }

    void Mesh::initGL()
    {
        if (!mVertexBuffer.create())
            throw std::runtime_error("Unable to create the vertex buffer!");
        if (!mIndexBuffer.create())
            throw std::runtime_error("Unable to create the vertex buffer!");

        if (!mVertexBuffer.bind())
            throw std::runtime_error("Unable to bind the vertex buffer!");
        mVertexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        mVertexBuffer.allocate(&mMesh->getVertexData()[0],
                               static_cast<int>(mMesh->getVertexData().size() * sizeof(gui::Vertex)));
        mVertexBuffer.release();

        if (!mIndexBuffer.bind())
            throw std::runtime_error("Unable to bind the index buffer!");
        mIndexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        mIndexBuffer.allocate(&mMesh->getIndices()[0],
                              static_cast<int>(mMesh->getIndices().size() * sizeof(gui::Vertex)));
        mIndexBuffer.release();
    }
}
