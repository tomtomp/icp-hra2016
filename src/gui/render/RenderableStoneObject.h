/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_RENDERABLESTONEOBJECT_H
#define HRA2016_RENDERABLESTONEOBJECT_H

#include "RenderableObject.h"
#include "StoneObject.h"

namespace gui
{
    class RenderableStoneObject : public RenderableObject, public StoneObject
    {
    public:
        RenderableStoneObject(gui::Mesh *meshPtr = nullptr, gui::Texture *texturePtr = nullptr,
                              const QVector3D &location = {0.0, 0.0, 0.0});
    private:
    protected:
    };
}

#endif //HRA2016_RENDERABLESTONEOBJECT_H
