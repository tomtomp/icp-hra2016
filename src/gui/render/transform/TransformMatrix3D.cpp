/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "TransformMatrix3D.h"

namespace trans
{
    TransformMatrix3D::TransformMatrix3D() :
        mRegenNeeded{false},
        mScale{1.0f, 1.0f, 1.0f}
    {
    }
}

