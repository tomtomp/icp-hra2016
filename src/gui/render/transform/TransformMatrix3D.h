/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_TRANSFORMMATRIX3D_H
#define HRA2016_TRANSFORMMATRIX3D_H

#include <QVector3D>
#include <QMatrix4x4>
#include <QQuaternion>

namespace trans
{
    /**
     * Matrix used for transformations in 3D space.
     */
    class TransformMatrix3D
    {
    public:
        TransformMatrix3D(const TransformMatrix3D &other) = default;
        TransformMatrix3D(TransformMatrix3D &&other) = default;

        /**
         * Create an identity transformation matrix.
         */
        TransformMatrix3D();

        /**
         * Load the identity matrix.
         */
        void loadIdentity();

        /**
         * Scale by given vector.
         * @param scale Vector to scale by.
         */
        void scale(const QVector3D &scale);
        void scale(float dx, float dy, float dz);
        void scale(float magnitude);

        /**
         * Scale by adding given vector.
         * @param scale Vector to add to scale.
         */
        void grow(const QVector3D &growScale);
        void grow(float dx, float dy, float dz);
        void grow(float magnitude);

        /**
         * Rotate using the given quaternion.
         * @param rot Rotation to perform.
         */
        void rotate(const QQuaternion &rot);
        void rotate(float angle, const QVector3D axis);
        void rotate(float angle, float ax, float ay, float az);

        /**
         * Translate by given vector.
         * @param translate Vector to translate by
         */
        void translate(const QVector3D &translate);
        void translate(float dx, float dy, float dz);

        /**
         * Set the translation.
         * @param vec Translation vector.
         */
        void setTranslate(const QVector3D &vec);
        void setTranslate(float x, float y, float z);

        /**
         * Set the translation.
         * @quat Rotation quaternion.
         */
        void setRotate(const QQuaternion &quat);
        void setRotate(float angle, const QVector3D axis);
        void setRotate(float angle, float ax, float ay, float az);

        /**
         * Set the scale.
         * @param vec Scaling vector.
         */
        void setScale(const QVector3D &vec);
        void setScale(float dx, float dy, float dz);
        void setScale(float magnitude);

        /**
         * Get the rotation.
         */
        const QVector3D &getTranslate() const;

        /**
         * Get the scale.
         */
        const QVector3D &getScale() const;

        /**
         * Get the rotation.
         */
        const QQuaternion &getRotate() const;

        /**
         * Get the resulting transformation matrix.
         */
        const QMatrix4x4 &getMatrix();
    private:
        /// Is the regeneration of mResult needed?
        bool mRegenNeeded;
        /// Translation part of this transformation.
        QVector3D mTranslate;
        /// Scaling part of this transformation.
        QVector3D mScale;
        /// Rotation part of this transformation.
        QQuaternion mRotate;
        /// The result matrix of all the transformations.
        QMatrix4x4 mResult;
    protected:
    };

    /*
     * Inline methods implementation
     */
    inline const QMatrix4x4 &TransformMatrix3D::getMatrix()
    {
        if (mRegenNeeded)
        {
            mResult.setToIdentity();
            mResult.translate(mTranslate);
            mResult.rotate(mRotate);
            mResult.scale(mScale);

            mRegenNeeded = false;
        }

        return mResult;
    }

    inline const QVector3D &TransformMatrix3D::getTranslate() const
    {
        return mTranslate;
    }

    inline const QVector3D &TransformMatrix3D::getScale() const
    {
        return mScale;
    }

    inline const QQuaternion &TransformMatrix3D::getRotate() const
    {
        return mRotate;
    }

    inline void TransformMatrix3D::setTranslate(const QVector3D &vec)
    {
        mRegenNeeded = true;
        mTranslate = vec;
    }

    inline void TransformMatrix3D::setRotate(const QQuaternion &quat)
    {
        mRegenNeeded = true;
        mRotate = quat;
    }

    inline void TransformMatrix3D::setScale(const QVector3D &vec)
    {
        mRegenNeeded = true;
        mScale = vec;
    }

    inline void TransformMatrix3D::scale(const QVector3D &scale)
    {
        mRegenNeeded = true;
        mScale *= scale;
    }

    inline void TransformMatrix3D::grow(const QVector3D &growScale)
    {
        mRegenNeeded = true;
        mScale += growScale;
    }

    inline void TransformMatrix3D::rotate(const QQuaternion &rot)
    {
        mRegenNeeded = true;
        mRotate = rot * mRotate;
    }

    inline void TransformMatrix3D::translate(const QVector3D &translate)
    {
        mRegenNeeded = true;
        mTranslate += translate;
    }

    inline void TransformMatrix3D::scale(float dx, float dy, float dz)
    {
        scale(QVector3D{dx, dy, dz});
    }

    inline void TransformMatrix3D::scale(float magnitude)
    {
        scale(QVector3D{magnitude, magnitude, magnitude});
    }

    inline void TransformMatrix3D::grow(float dx, float dy, float dz)
    {
        grow(QVector3D{dx, dy, dz});
    }

    inline void TransformMatrix3D::grow(float magnitude)
    {
        grow(QVector3D{magnitude, magnitude, magnitude});
    }

    inline void TransformMatrix3D::rotate(float angle, const QVector3D axis)
    {
        rotate(QQuaternion::fromAxisAndAngle(axis, angle));
    }

    inline void TransformMatrix3D::rotate(float angle, float ax, float ay, float az)
    {
        rotate(QQuaternion::fromAxisAndAngle(ax, ay, az, angle));
    }

    inline void TransformMatrix3D::translate(float dx, float dy, float dz)
    {
        translate(QVector3D{dx, dy, dz});
    }

    inline void TransformMatrix3D::setTranslate(float x, float y, float z)
    {
        setTranslate(QVector3D{x, y, z});
    }

    inline void TransformMatrix3D::setRotate(float angle, const QVector3D axis)
    {
        setRotate(QQuaternion::fromAxisAndAngle(axis, angle));
    }

    inline void TransformMatrix3D::setRotate(float angle, float ax, float ay, float az)
    {
        setRotate(QQuaternion::fromAxisAndAngle(ax, ay, az, angle));
    }

    inline void TransformMatrix3D::setScale(float dx, float dy, float dz)
    {
        setScale(QVector3D{dx, dy, dz});
    }

    inline void TransformMatrix3D::setScale(float magnitude)
    {
        setScale(QVector3D{magnitude, magnitude, magnitude});
    }

    inline void TransformMatrix3D::loadIdentity()
    {
        mRegenNeeded = true;
        mScale = {1.0f, 1.0f, 1.0f};
        mRotate = QQuaternion();
        mTranslate = {0.0f, 0.0f, 0.0f};
    }
}

#endif //HRA2016_TRANSFORMMATRIX3D_H
