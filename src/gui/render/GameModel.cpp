/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "GameModel.h"

namespace gui
{
    GameModel::GameModel() :
        mGame{new core::Game(this)},
        mState(GameState::BEFORE_GAME)
    {
        gui::NewGameDialog ngd(nullptr, mGame);
        if (ngd.exec() != QDialog::Accepted)
            throw std::runtime_error("New game creation failed, because dialog has not been filled correctly.");

        mGame->chooseBoardSize(ngd.getSizeIndex());
        auto p1Dialog = ngd.getPlayerDialog1();
        mGame->createPlayer(p1Dialog->getName().toStdString(),
                            p1Dialog->getType(),
                            p1Dialog->getAiType());
        auto p2Dialog = ngd.getPlayerDialog2();
        mGame->createPlayer(p2Dialog->getName().toStdString(),
                            p2Dialog->getType(),
                            p2Dialog->getAiType());

        mGame->start();

        std::size_t playFieldSize = mGame->getPlayField().getSize();

        initResources(playFieldSize);
        initObjects(playFieldSize);

        mPlayFieldSize = playFieldSize;
    }

    GameModel::GameModel(QString filename) :
        mState(GameState::BEFORE_GAME)
    {
        QFile savedGame{filename};
        if (!savedGame.open(QIODevice::ReadOnly | QIODevice::Text))
            throw std::runtime_error("Unable to open the saved game!");

        std::string savedGameStr = savedGame.readAll().toStdString();

        mGame = new core::Game(this, savedGameStr);

        mGame->start();

        std::size_t playFieldSize = mGame->getPlayField().getSize();

        initResources(playFieldSize);
        initObjects(playFieldSize);

        mPlayFieldSize = playFieldSize;
    }

    GameModel::~GameModel()
    {
        if (mGame->isRunning())
        {
            mGame->sendMessage(mMsgFactory.createMessage(core::MessageT::END_MSG));
            mGame->stop();
        }

        delete mGame;

        cleanupObjects();
        cleanupResources();
    }

    void GameModel::update()
    {
        switch(mState)
        {
            case GameState::BEFORE_GAME:
                // Before the game has initialized.
                if (isMessagePresent())
                {
                    core::MessageT msgType = getMessageType();
                    if (msgType == core::MessageT::READY_MSG)
                        mState = GameState::READY;
                    else
                    {
                        util::Logger::logDebug(std::string("Unknown state received in the GameModel::update in state"
                                                           " BEFORE_GAME : ") +
                                                           core::Message::getMessageTypeStr(msgType));
                        throw std::runtime_error(std::string("Unknown state received in the GameModel::update in state"
                                                             " BEFORE_GAME : ") +
                                                             core::Message::getMessageTypeStr(msgType));
                    }
                }
                break;
            case GameState::READY:
                // Game is ready for next move.
                mPlayerText->updateText(QString("Player") + QString::number(mGame->getCurrentPlayerID()));

                updatePlayFieldState(mGame->getPlayField());
                updateHints(mGame->getPossibleActions());
                updateScore(mGame->getScores());

                mState = GameState::SEND_NEXT;
                mText->updateText("Game ready");

                break;
            case GameState::SEND_NEXT:
                // Send the next move message.
                mGame->sendMessage(mMsgFactory.createMessage(core::MessageT::NEXT_MSG));
                mState = GameState::AFTER_READY;
                break;
            case GameState::AFTER_READY:
                // Waiting for the game to reply.
                if (isMessagePresent())
                {
                    core::MessageT msgType = getMessageType();
                    switch(msgType)
                    {
                        case core::MessageT::READY_MSG:
                        case core::MessageT::UNDO_FAIL_MSG:
                        case core::MessageT::REDO_FAIL_MSG:
                            mState = GameState::READY;
                            break;
                        case core::MessageT::REQ_MOVE_MSG:
                            mState = GameState::INPUT_REQUIRED;
                            mText->updateText("Waiting for input");
                            break;
                        case core::MessageT::ENDED_MSG:
                            mState = GameState ::ENDED;
                            updateWon(updateScore(mGame->getScores()));
                            mText->updateText("Game ended");
                            break;
                        default:
                            util::Logger::logDebug(std::string("Unknown state received in the GameModel::update in "
                                                               "state AFTER_READY : ") +
                                                               core::Message::getMessageTypeStr(msgType));
                            throw std::runtime_error(std::string("Unknown state received in the GameModel::update in "
                                                                 "state AFTER_READY : ") +
                                                                 core::Message::getMessageTypeStr(msgType));
                    }
                }
                break;
            case GameState::INPUT_REQUIRED:
                // Waiting for user input.
                break;
            case GameState::INPUT_PROCESSING:
                // Processing the input from user.
                if (isMessagePresent())
                {
                    core::MessageT msgType = getMessageType();
                    switch(msgType)
                    {
                        case core::MessageT::READY_MSG:
                            mState = GameState::READY;
                            break;
                        case core::MessageT::INV_MOVE_MSG:
                            mState = GameState::AFTER_READY;
                            break;
                        case core::MessageT::ENDED_MSG:
                            mState = GameState ::ENDED;
                            updateWon(updateScore(mGame->getScores()));
                            mText->updateText("Game ended");
                            break;
                        default:
                            util::Logger::logDebug(std::string("Unknown state received in the GameModel::update in "
                                                                   "state AFTER_READY : ") +
                                                   core::Message::getMessageTypeStr(msgType));
                            throw std::runtime_error(std::string("Unknown state received in the GameModel::update in "
                                                                     "state AFTER_READY : ") +
                                                     core::Message::getMessageTypeStr(msgType));
                    }
                }
                break;
            case GameState::UNDO:
                // Undoing a move.
                if (isMessagePresent())
                {
                    core::MessageT msgType = getMessageType();
                    switch (msgType) {
                        case core::MessageT::READY_MSG:
                            mPlayerWonText->setVisible(false);
                            mState = GameState::READY;
                            break;
                        case core::MessageT::ENDED_MSG:
                        case core::MessageT::UNDO_FAIL_MSG:
                            mState = GameState::UNDO;
                            break;
                        default:
                            util::Logger::logDebug(std::string("Unknown state received in the GameModel::update in "
                                                                   "state UNDO : ") +
                                                   core::Message::getMessageTypeStr(msgType));
                            throw std::runtime_error(std::string("Unknown state received in the GameModel::update in "
                                                                     "state UNDO : ") +
                                                     core::Message::getMessageTypeStr(msgType));
                    }
                }
                break;
            case GameState::REDO:
                // Redoing a move.
                if (isMessagePresent())
                {
                    core::MessageT msgType = getMessageType();
                    switch (msgType) {
                        case core::MessageT::READY_MSG:
                            mState = GameState::READY;
                            break;
                        case core::MessageT::ENDED_MSG:
                        case core::MessageT::REDO_FAIL_MSG:
                            mState = GameState::REDO;
                            break;
                        default:
                            util::Logger::logDebug(std::string("Unknown state received in the GameModel::update in "
                                                                   "state REDO : ") +
                                                   core::Message::getMessageTypeStr(msgType));
                            throw std::runtime_error(std::string("Unknown state received in the GameModel::update in "
                                                                     "state REDO : ") +
                                                     core::Message::getMessageTypeStr(msgType));
                    }
                }
                break;
            case GameState::ENDED:
                // Waiting for user, to close this window.
                break;
        }
    }

    void GameModel::render(QOpenGLFunctions *fun,
                           QOpenGLShaderProgram *mainProg, const QMatrix4x4 &mainVP,
                           QOpenGLShaderProgram *textProg, const QMatrix4x4 &textVP,
                           const QMatrix4x4 &shadowVP)
    {
        mainProg->bind();
        mPlayFieldTileCollection->render(fun, mainProg, mainVP, shadowVP);
        mainProg->setUniformValue("effectType", 1);
        mStonesCollection->render(fun, mainProg, mainVP, shadowVP);
        mainProg->setUniformValue("effectType", 0);
        mainProg->release();

        textProg->bind();
        mScoreText->render(fun, textProg, textVP);
        mPlayerText->render(fun, textProg, textVP);
        mText->render(fun, textProg, textVP);
        mPlayerWonText->render(fun, textProg, textVP);
        textProg->release();
    }

    void GameModel::renderS(QOpenGLFunctions *fun,
                            QOpenGLShaderProgram *shadingProg, const QMatrix4x4 &shadingVP)
    {
        shadingProg->bind();
        mPlayFieldTileCollection->render(fun, shadingProg, shadingVP);
        mStonesCollection->render(fun, shadingProg, shadingVP);
        shadingProg->release();
    }

    void GameModel::initResources(std::size_t size)
    {
        mStoneMesh = new gui::Mesh(":/stone.ply");
        mStoneTexture = new gui::Texture(":/stone.bmp");

        //float sizef = static_cast<float>(size);
        const std::vector<gui::Vertex> RECT_VERTEX_DATA{
            // x, y, z, nx, ny, nz, u, v
            /*
            gui::Vertex{1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, sizef, 0.0f},
            gui::Vertex{-1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
            gui::Vertex{-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, sizef},
            gui::Vertex{1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, sizef, sizef},
            */
            gui::Vertex{1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f},
            gui::Vertex{-1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
            gui::Vertex{-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
            gui::Vertex{1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f},
        };

        const std::vector<unsigned int> RECT_INDICES{
            // Prepared for GL_TRIANGLES
            0, 1, 2,
            0, 2, 3
        };
        mPlayFieldMesh = new gui::Mesh(RECT_VERTEX_DATA, RECT_INDICES);
        mPlayFieldTexture = new gui::Texture(":/tile.bmp");
        mPlayFieldTexture->setWrap(QOpenGLTexture::Repeat);
    }

    void GameModel::cleanupResources()
    {
        delete mPlayFieldTexture;
        delete mPlayFieldMesh;

        delete mStoneTexture;
        delete mStoneMesh;
    }

    void GameModel::initObjects(std::size_t size)
    {
        mStonesCollection = new gui::RenderableCollection(mStoneMesh, mStoneTexture);
        mPlayFieldTileCollection = new gui::RenderableCollection(mPlayFieldMesh, mPlayFieldTexture);

        float step = 2.0f / size;
        float start = step / 2.0f - 1.0f;
        float stoneScale = step / 2.0f - step / 4.0f;
        float tileScale = step / 2.0f;

        for (std::size_t iii = 0; iii < size; ++iii)
        {
            for (std::size_t jjj = 0; jjj < size; ++jjj)
            {
                float posX = start + jjj * step;
                float posY = start + iii * step;
                gui::StoneObject *newStone = new gui::StoneObject(QVector3D{posX, posY, 0.0f});
                newStone->setVisible(false);
                newStone->getTrans().setScale(stoneScale);

                mStones.push_back(newStone);
                mStonesCollection->addObject(newStone);

                gui::GameObject *newTile = new gui::GameObject(QVector3D{posX, posY, 0.0f});
                newTile->getTrans().setScale(tileScale);

                mPlayFieldTiles.push_back(newTile);
                mPlayFieldTileCollection->addObject(newTile);
            }
        }

        /*
        gui::StoneObject *newObj = new gui::StoneObject(QVector3D{0.5f, 0.5f, 1.5f});
        newObj->getTrans().setScale(0.2f);
        mStonesCollection->addObject(newObj);
        */

        mText = new gui::TextObject("Initializing", QFont("Arial", 20), QColor(122, 76, 17), QColor(255, 255, 255, 0),
                                    QVector3D{0.0f, 0.0f, 0.0f});
        mPlayerText = new gui::TextObject("Initializing", QFont("Arial", 20), QColor(122, 76, 17), QColor(255, 255, 255, 0),
                                          QVector3D{0.0f, 50.0f, 0.0f});
        mScoreText = new gui::TextObject("Initializing", QFont("Arial", 20, QFont::Bold), QColor(122, 122, 17), QColor(255, 255, 255, 0),
                                          QVector3D{220.0f, 0.0f, 0.0f});
        mPlayerWonText = new gui::TextObject("Empty", QFont("Arial", 20, QFont::Bold), QColor(122, 122, 17), QColor(255, 255, 255, 0),
                                         QVector3D{220.0f, 50.0f, 0.0f});
        mPlayerWonText->setVisible(false);
    }

    void GameModel::cleanupObjects()
    {
        delete mPlayerWonText;
        delete mScoreText;
        delete mPlayerText;
        delete mText;
        for (gui::StoneObject *obj : mStones)
            delete obj;
        delete mStonesCollection;
        for (gui::GameObject *obj : mPlayFieldTiles)
            delete obj;
        delete mPlayFieldTileCollection;
    }

    void GameModel::updatePlayFieldState(const core::PlayField &pf)
    {
        assert(mStones.size() == pf.getField().size());

        const std::vector<char> &colors = pf.getField();

        for (std::size_t iii = 0; iii < mStones.size(); ++iii)
        {
            char color{colors[iii]};
            switch(color)
            {
                case 1:
                    mStones[iii]->setVisible(true);
                    mStones[iii]->setColor(StoneColor::WHITE);
                    break;
                case 2:
                    mStones[iii]->setVisible(true);
                    mStones[iii]->setColor(StoneColor::BLACK);
                    break;
                case core::PlayField::EMPTY_ID:
                default:
                    mStones[iii]->setVisible(false);
                    break;
            }
        }
    }

    core::MessageT GameModel::getMessageType()
    {
        assert(isMessagePresent());
        return receiveMessage()->getType();
    }

    void GameModel::mousePressEvent(QMouseEvent *event, const QMatrix4x4 &vp, int width, int height)
    {
        if (mState != GameState::INPUT_REQUIRED)
            return;

        static const QVector4D BOTTOM_LEFT_CORNER{1.0f, -1.0f, 0.0f, 1.0f};
        static const QVector4D TOP_RIGHT_CORNER{-1.0f, 1.0f, 0.0f, 1.0f};

        QMatrix4x4 mvp = vp * mPlayFieldTileCollection->getTrans().getMatrix();

        QVector4D projectedBLC = mvp * BOTTOM_LEFT_CORNER;
        projectedBLC /= projectedBLC.w();
        QVector4D projectedTRC = mvp * TOP_RIGHT_CORNER;
        projectedTRC /= projectedTRC.w();

        QVector4D mousePos{static_cast<float>(width - event->x()),
                           static_cast<float>(height - event->y()),
                           0.0f, 1.0f};
        QVector4D projectedMP = mousePos;
        projectedMP.setX((projectedMP.x() / width * 2.0f) - 1.0f);
        projectedMP.setY((projectedMP.y() / height * 2.0f) - 1.0f);

        QVector4D deltaPos = (projectedTRC - projectedBLC);

        QVector4D deltaMouse = (projectedMP - projectedBLC);

        QVector4D perc = deltaMouse / deltaPos;

        int size = static_cast<int>(mGame->getPlayField().getSize());
        float step = 1.0f / size;

        int posX = static_cast<int>(std::floor(perc.x() / step));
        int posY = static_cast<int>(std::floor(perc.y() / step));

        if (posX < 0 || posX >= size || posY < 0 || posY >= size)
            return;

        mGame->sendMessage(mMsgFactory.createCoordsMessage(core::Coords{posX, posY}));
        mState = GameState::INPUT_PROCESSING;

        /*
        std::cout << "Projected BLC : " << projectedBLC.x() << ", " << projectedBLC.y() << ", " << projectedBLC.z() <<
                     ", " << projectedBLC.w() << std::endl;
        std::cout << "Projected TRC : " << projectedTRC.x() << ", " << projectedTRC.y() << ", " << projectedTRC.z() <<
                     ", " << projectedTRC.w() << std::endl;

        std::cout << "Projected Mouse : " << projectedMP.x() << ", " << projectedMP.y() << ", " << projectedMP.z() <<
        ", " << projectedMP.w() << std::endl;

        std::cout << "DeltaPos : " << deltaPos.x() << ", " << deltaPos.y() << ", " << deltaPos.z() <<
        ", " << deltaPos.w() << std::endl;

        std::cout << "DeltaMouse : " << deltaMouse.x() << ", " << deltaMouse.y() << ", " << deltaMouse.z() <<
        ", " << deltaMouse.w() << std::endl;

        std::cout << "Perc : " << perc.x() << ", " << perc.y() << ", " << perc.z() <<
        ", " << perc.w() << std::endl;

        std::cout << "Perc : " << std::floor(perc.x() / step) << ", " << std::floor(perc.y() / step) << std::endl;
        */
    }

    bool GameModel::undo()
    {
        if (!isStatic())
            return false;
            //throw std::runtime_error("Game is not static.");

        mGame->sendMessage(mMsgFactory.createMessage(core::MessageT::UNDO_MSG));

        mState = GameState::UNDO;

        return true;
    }

    bool GameModel::redo()
    {
        if (!isStatic())
            return false;
            //throw std::runtime_error("Game is not static.");

        mGame->sendMessage(mMsgFactory.createMessage(core::MessageT::REDO_MSG));

        mState = GameState::REDO;

        return true;
    }

    bool GameModel::saveGame(QString filename)
    {
        if (!isStatic())
            throw std::runtime_error("Game is not static.");

        QFile saveGame(filename);
        if (!saveGame.open(QIODevice::WriteOnly | QIODevice::Text))
            throw std::runtime_error("Unable to open the save game!");

        try
        {
            saveGame.write(QByteArray::fromStdString(mGame->serialize()));
        } catch(...)
        {
            return false;
        }

        return true;
    }

    void GameModel::updateHints(const std::vector<core::MoveAction> &possibleActions)
    {
        for (gui::GameObject *obj : mPlayFieldTiles)
            obj->setLightness(0);

        for (const core::MoveAction &ma : possibleActions)
        {
            core::Coords hintLocation = ma.getPrimaryStone();
            std::size_t index = hintLocation.first + hintLocation.second * mPlayFieldSize;

            if (index >= mPlayFieldTiles.size())
                throw std::runtime_error("Hint location out of bounds");

            mPlayFieldTiles[index]->setLightness(-2);
        }

    }

    int GameModel::updateScore(const std::vector<std::size_t> &score)
    {
        if (score.size() != 3)
            throw std::runtime_error("Score vector is not the correct length");

        QString scoreText = QString::asprintf("P1:%4ld P2:%4ld", score[1], score[2]);

        mScoreText->updateText(scoreText);

        if (score[1] == score[2])
            return 0;
        else
            return score[1] > score[2] ? 1 : 2;
    }

    void GameModel::updateWon(int playerID)
    {
        if (playerID == 0)
            mPlayerWonText->updateText("It's a draw!");
        else
        {
            auto victor = mGame->getPlayer(static_cast<std::size_t>(playerID));
            mPlayerWonText->updateText(QString("#") + QString::number(playerID) + QString(" ") +
                                       QString::fromStdString(victor->getName()) + QString(" won!"));
        }

        mPlayerWonText->setVisible(true);
    }
}
