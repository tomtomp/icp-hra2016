/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_RENDERABLEOBJECT_H
#define HRA2016_RENDERABLEOBJECT_H

#include "Mesh.h"
#include "Texture.h"
#include "GameObject.h"
#include "TextureLoader.h"
#include "MeshLoader.h"

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>

namespace gui
{
    /**
     * Game object, which can be rendered.
     */
    class RenderableObject : virtual public GameObject
    {
    public:
        /**
         * Create new renderable object with given visual parameters and location.
         * @param meshPtr Pointer to this objects mesh.
         * @param texturePtr Pointer to this objects texture.
         * @param program The Shader program used to display this object.
         * @param location Location of this object in 3D space.
         */
        RenderableObject(gui::Mesh *meshPtr = nullptr, gui::Texture *texturePtr = nullptr, const QVector3D &location = {0.0, 0.0, 0.0});

        /**
         * Render this object.
         * @param fun OpenGL functions of current context.
         * @param program OpenGL shader program to render with.
         * @param vp View + projection matrix.
         * @param shadowVP View-projection matrix used for shadow mapping
         */
        virtual void render(QOpenGLFunctions *fun, QOpenGLShaderProgram *program, const QMatrix4x4 &vp,
                            const QMatrix4x4 &shadowVP = QMatrix4x4());

        /*
         * Set the mesh of this object.
         * @param meshPtr Pointer to the new mesh.
         */
        void setMesh(gui::Mesh *meshPtr = nullptr)
        {
            mMesh = meshPtr;
        }

        /*
         * Set the texture of this object.
         * @param texturePtr Pointer to the new texture.
         */
        void setTexture(gui::Texture *texturePtr = nullptr)
        {
            mTexture = texturePtr;
        }

        virtual ~RenderableObject();
    private:
    protected:
        /// Displayed mesh.
        gui::Mesh *mMesh;
        /// Displayed texture.
        gui::Texture *mTexture;
    };
}

#endif //HRA2016_RENDERABLEOBJECT_H
