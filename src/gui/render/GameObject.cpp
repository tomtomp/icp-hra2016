/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "GameObject.h"

namespace gui
{
    GameObject::GameObject(const QVector3D &location) :
        mVisible{true}
    {
        mTrans.setTranslate(location);
    }
}
