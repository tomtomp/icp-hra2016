/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_TEXTOBJECT_H
#define HRA2016_TEXTOBJECT_H

#include "RenderableObject.h"

#include <QString>
#include <QPainter>

namespace gui
{
    /**
     * Renderable 2D text rectangle.
     */
    class TextObject : public RenderableObject
    {
    public:
        /**
         * Create a text rectangle using given information.
         * @param text Text to display.
         * @param font Font to render the text with.
         * @param color Color of the text.
         * @param background Background color, behind the text.
         * @param location Location to render at.
         */
        TextObject(const QString &text, const QFont &font, const QColor &color,
                   const QColor &backgroud, const QVector3D &location = {0.0, 0.0, 0.0});

        virtual ~TextObject();

        /**
         * Update the displayed text.
         * @param text Text to display.
         * @param font Font to render the text with.
         * @param color Color of the text.
         * @param background Background color, behind the text.
         */
        void updateText(const QString &text, const QFont &font, const QColor &color,
                        const QColor &backgroud);

        /**
         * Update the displayed text. Use the font and colors from previous call.
         * @param text Text to display.
         */
        void updateText(const QString &text);

        static const std::vector<gui::Vertex> RECT_VERTEX_DATA;
        static const std::vector<unsigned int> RECT_INDICES;
    private:
        /**
         * Prepare the texture from given parameters.
         * @param text Text to display.
         * @param font Font to render the text with.
         * @param color Color of the text.
         * @param background Background color, behind the text.
         */
        QImage prepareTexture(const QString &text, const QFont &font, const QColor &color,
                              const QColor &backgroud);

        /// Painter used for rendering the text.
        QPainter mPainter;

        /// Rendered font.
        QFont mFont;
        /// Rendered size of the font.
        int mSize;
        /// Color of the rendered text.
        QColor mColor;
        /// Color of the background.
        QColor mBackground;

        /// Rectangular mesh for the rendering.
        gui::Mesh *mRectMesh{nullptr};
        /// Text is rendered into this texture.
        gui::Texture *mTextTexture{nullptr};
    protected:
    };
}

#endif //HRA2016_TEXTOBJECT_H
