/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "TextureLoader.h"

namespace gui
{
    TextureLoader::TextureLoader(const QString &path)
    {
        loadTexture(path);
    }

    void TextureLoader::loadTexture(const QString &path)
    {
        QFile input(path);

        if (!input.open(QIODevice::ReadOnly))
            throw std::runtime_error(std::string("Error, cannot open texture resource. ") + path.toStdString());

        QByteArray fileContent = input.readAll();

        if (fileContent.size() < 54)
            throw std::runtime_error(std::string("BMP file is too short, unable to read the header. ") + path.toStdString());

        if (fileContent[0] != 'B' || fileContent[1] != 'M')
            throw std::runtime_error(std::string("Given file is not an BMP file. ") + path.toStdString());

        // The magical constants in this part were found on the Wikipedia.
        int *dataPosPtr = reinterpret_cast<int*>(fileContent.begin() + 0x0A);
        int *imageSizePtr = reinterpret_cast<int*>(fileContent.begin() + 0x22);
        int *widthPtr = reinterpret_cast<int*>(fileContent.begin() + 0x12);
        int *heightPtr = reinterpret_cast<int*>(fileContent.begin() + 0x16);

        int dataPos{*dataPosPtr}, imageSize{*imageSizePtr}, width{*widthPtr}, height{*heightPtr};

        if (imageSize == 0)
            imageSize = *widthPtr * *heightPtr * 3;

        if (dataPos  == 0)
            dataPos = 54;

        std::copy(fileContent.cbegin() + dataPos, fileContent.cbegin() + dataPos + imageSize,
                  std::back_insert_iterator<std::vector<unsigned char>>(mData));

        mWidth = static_cast<std::size_t>(width);
        mHeight = static_cast<std::size_t>(height);
    }
}
