/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAMEMODEL_H
#define HRA2016_GAMEMODEL_H

#include <QMouseEvent>
#include <QString>

#include "RenderableCollection.h"
#include "RenderableObject.h"
#include "RenderableStoneObject.h"
#include "StoneObject.h"
#include "Mesh.h"
#include "Texture.h"
#include "TextObject.h"
#include "../NewGameDialog.h"

namespace gui
{
    enum class GameState
    {
        /// Before the game started.
        BEFORE_GAME = 0,
        /// The game is ready.
        READY,
        /// Send the next message.
        SEND_NEXT,
        /// Waiting for the game to reply.
        AFTER_READY,
        /// Waiting for user input.
        INPUT_REQUIRED,
        /// Waiting for the game to process.
        INPUT_PROCESSING,
        /// Undoing a move.
        UNDO,
        /// Redoing a move.
        REDO,
        /// The game has ended.
        ENDED
    };

    /**
     * Class representing the game model and game logic.
     */
    class GameModel : public core::MessageBox
    {
    public:
        /**
         * Construct the game model, ask the user about game details by new game dialog.
         */
        GameModel();

        /**
         * Construct the game model using a saved game. Id the filename is empty, asks the user for the filename.
         * @param filename Name of the file containing the saved game. If empty, the user will be asked for it.
         */
        GameModel(QString filename);

        ~GameModel();

        /**
         * Render the state of this game.
         * @param fun OpenGL functions for the active context.
         * @param mainProg Main shader program.
         * @param mainVP View-projection matrix for the object rendering.
         * @param textProg Shader program used for rendering text.
         * @param textVp View-projection matrix used for text rendering.
         * @param shadowVP View-projection used for shadow mapping
         */
        void render(QOpenGLFunctions *fun,
                    QOpenGLShaderProgram *mainProg, const QMatrix4x4 &mainVP,
                    QOpenGLShaderProgram *textProg, const QMatrix4x4 &textVP,
                    const QMatrix4x4 &shadowVP);

        /**
         * Render the state of game for shading purposes.
         * @param fun OpenGL functions for the active context.
         * @param shadingProg Shading shader program.
         * @param shadingVP View-projection matrix for the shading.
         */
        void renderS(QOpenGLFunctions *fun,
                    QOpenGLShaderProgram *shadingProg, const QMatrix4x4 &shadingVP);

        /**
         * Update the state of this game.
         */
        void update();

        /**
         * Mouse press event.
         * @param event The mouse event.
         * @param vp View-projection matrix.
         * @param width Width of the viewport.
         * @param height Height of the viewport.
         */
        void mousePressEvent(QMouseEvent *event, const QMatrix4x4 &vp, int width, int height);

        /**
         * Send the undo command to the game.
         * @return Returns true, if the undo command was successful.
         */
        bool undo();

        /**
         * Send the redo command to the game.
         * @return Returns true, if the redo command was successful.
         */
        bool redo();

        /**
         * Save the game to given file.
         * @return Returns true, if the game was saved successfully.
         */
        bool saveGame(QString filename);

        /**
         * Is the game in static state? Static state allows to undo/redo and save the game.
         * @return Returns true, if the game is in static state.
         */
        bool isStatic() const
        {
            return mState == GameState::READY || mState == GameState::INPUT_REQUIRED || mState == GameState::ENDED;
        }
    private:
        /**
         * Update the state of objects from given playing field.
         * @param pf Playing field to use.
         */
        void updatePlayFieldState(const core::PlayField &pf);

        /**
         * Update the hint display system.
         * @param possibleActions Vector of possible actions from the game.
         */
        void updateHints(const std::vector<core::MoveAction> &possibleActions);

        /**
         * Update the score display.
         * @param score Vector containing the score.
         * @return Returns the ID of winning player.
         */
        int updateScore(const std::vector<std::size_t> &score);

        /**
         * Update the player won text.
         * @param playerID ID of the victorious player, 0 = draw.
         */
        void updateWon(int playerID);

        /**
         * Initialize all the mesh and texture resources.
         * @param size Size of the playing field.
         */
        void initResources(std::size_t size);

        /**
         * Clean up the resources.
         */
        void cleanupResources();

        /**
         * Initialize all the game objects.
         * @param size Size of the playing field.
         */
        void initObjects(std::size_t size);

        /**
         * Clean up the objects.
         */
        void cleanupObjects();

        /**
         * Receive a message and return its type.
         * @return The type of received message.
         */
        core::MessageT getMessageType();

        /// Mesh for the stones.
        gui::Mesh *mStoneMesh{nullptr};
        /// Texture for the stones.
        gui::Texture *mStoneTexture{nullptr};

        /// Mesh for the playing field.
        gui::Mesh *mPlayFieldMesh{nullptr};
        /// Texture for the playing field.
        gui::Texture *mPlayFieldTexture{nullptr};

        /// Vector containing the stone objects.
        std::vector<gui::StoneObject*> mStones;
        /// Collection of the stones.
        gui::RenderableCollection *mStonesCollection{nullptr};
        /// Vector containing playing field tiles.
        std::vector<gui::GameObject*> mPlayFieldTiles;
        /// Game playing field object.
        gui::RenderableCollection *mPlayFieldTileCollection{nullptr};
        /// Information text.
        gui::TextObject *mText{nullptr};
        /// Current player text.
        gui::TextObject *mPlayerText{nullptr};
        /// Displays who won.
        gui::TextObject *mPlayerWonText{nullptr};
        /// Current score.
        gui::TextObject *mScoreText{nullptr};

        /// Game object.
        core::Game *mGame{nullptr};
        /// Factory used for creating messages.
        core::MessageFactory mMsgFactory;
        /// The game state machine variable.
        GameState mState{GameState::BEFORE_GAME};
        /// Size of the playing field.
        std::size_t mPlayFieldSize{0};
    protected:
    };
}

#endif //HRA2016_GAMEMODEL_H
