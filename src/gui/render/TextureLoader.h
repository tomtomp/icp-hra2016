/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_TEXTURELOADER_H
#define HRA2016_TEXTURELOADER_H

#include <iostream>
#include <vector>

#include <QString>
#include <QFile>
#include <QByteArray>

namespace gui
{
    /**
     * BMP texture loader.
     */
    class TextureLoader
    {
    public:
        /**
         * Load the texture from given file.
         * @param path Path to the BMP file.
         */
        TextureLoader(const QString &path);

        /**
         * Get the texture width.
         * @return The width of this texture.
         */
        std::size_t getWidth() const
        {
            return mWidth;
        }

        /**
         * Get the texture height.
         * @return The height of this texture.
         */
        std::size_t getHeight() const
        {
            return mHeight;
        }

        /**
         * Get the data from this texture.
         * @return Vector containing RGB data.
         */
        const std::vector<unsigned char> &getData() const
        {
            return mData;
        }
    private:

        /**
         * Load the texture from given file.
         * @param path Path to the BMP file.
         */
        void loadTexture(const QString &path);

        /// Width of the texture.
        std::size_t mWidth{0};
        /// Height of the texture.
        std::size_t mHeight{0};
        /// RGB data.
        std::vector<unsigned char> mData;
    protected:
    };
}

#endif //HRA2016_TEXTURELOADER_H
