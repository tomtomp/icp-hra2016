/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "RenderableStoneObject.h"

namespace gui
{
    RenderableStoneObject::RenderableStoneObject(gui::Mesh *meshPtr, gui::Texture *texturePtr,
                                                 const QVector3D &location) :
        GameObject(location),
        RenderableObject(meshPtr, texturePtr),
        StoneObject(location)
    {
    }
}
