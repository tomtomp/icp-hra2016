/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "RenderableCollection.h"

namespace gui
{
    RenderableCollection::RenderableCollection(gui::Mesh *meshPtr, gui::Texture *texturePtr,
                                               const QVector3D &location) :
        RenderableObject(meshPtr, texturePtr, location)
    {
    }

    void RenderableCollection::render(QOpenGLFunctions *fun, QOpenGLShaderProgram *program, const QMatrix4x4 &vp,
                                      const QMatrix4x4 &shadowVP)
    {
        if (getVisible() && mMesh)
        {
            QMatrix4x4 baseMvp = vp * getTrans().getMatrix();
            QMatrix4x4 baseShadowMvp = shadowVP * getTrans().getMatrix();

            if (mTexture)
                mTexture->use(fun, program);

            for (gui::GameObject *obj : mObjects)
            {
                if (obj->getVisible())
                {
                    const QMatrix4x4 &m = obj->getTrans().getMatrix();
                    QMatrix4x4 mvp = baseMvp * m;
                    QMatrix3x3 inverseTransposeModelMatrix = m.normalMatrix();
                    program->setUniformValue("mvp", mvp);
                    program->setUniformValue("m", m);
                    program->setUniformValue("mInvTrans", inverseTransposeModelMatrix);
                    program->setUniformValue("lightness", obj->getLightness());
                    program->setUniformValue("depthBiasMvp", baseShadowMvp * m);

                    mMesh->render(fun, program);
                }
            }

            if (mTexture)
                mTexture->unuse();
        }
    }

    RenderableCollection::~RenderableCollection()
    {
    }

    void RenderableCollection::addObject(gui::GameObject *obj)
    {
        mObjects.push_back(obj);
    }
}


