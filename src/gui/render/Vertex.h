/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_VERTEX_H
#define HRA2016_VERTEX_H

#include <ostream>
#include <QVector3D>
#include <QVector2D>

namespace gui
{
    /**
     * Vertex class containing information about a single vertex in 3D space.
     */
    class Vertex
    {
    public:
        /**
         * Construct new vertex.
         */
        constexpr Vertex();

        /**
         * Construct new vertex with given information.
         */
        constexpr Vertex(const QVector3D &position, const QVector3D &normal, const QVector2D &texture);
        constexpr Vertex(float x, float y, float z, float nx, float ny, float nz, float u, float v);

        constexpr const QVector3D &getPosition() const;
        constexpr const QVector3D &getNormal() const;
        constexpr const QVector2D &getTexture() const;

        constexpr void setPosition(const QVector3D &position);
        constexpr void setNormal(const QVector3D &normal);
        constexpr void setTexture(const QVector2D &texture);

        /// The number of components in position vector.
        static constexpr int POSITION_TUPLE_SIZE{3};
        /// The number of components in normal vector.
        static constexpr int NORMAL_TUPLE_SIZE{3};
        /// The number of components in texture vector.
        static constexpr int TEXTURE_TUPLE_SIZE{2};

        /// Get the offset of position vector.
        static constexpr int getPositionOffset();
        /// Get the offset of normal vector.
        static constexpr int getNormalOffset();
        /// Get the offset of texture vector.
        static constexpr int getTextureOffset();
        /// Get the stride - number of bytes, between 2 vertexes.
        static constexpr int getStride();

        /// Print out this vertex.
        friend std::ostream &operator<<(std::ostream &out, const Vertex &vertex);
    private:
        /// Position of the vertex in 3D space.
        QVector3D mPosition;
        /// Vertex normal.
        QVector3D mNormal;
        /// Texture coordinates.
        QVector2D mTexture;
    protected:
    };

    /*
     * Implementation of inline methods
     */

    constexpr gui::Vertex::Vertex()
    {
    }

    constexpr Vertex::Vertex(const QVector3D &position, const QVector3D &normal, const QVector2D &texture) :
        mPosition(position),
        mNormal(normal),
        mTexture(texture)
    {
    }

    inline constexpr const QVector3D &Vertex::getPosition() const
    {
        return mPosition;
    }

    inline constexpr const QVector3D &Vertex::getNormal() const
    {
        return mNormal;
    }

    inline constexpr const QVector2D &Vertex::getTexture() const
    {
        return mTexture;
    }

    inline constexpr void Vertex::setPosition(const QVector3D &position)
    {
        mPosition = position;
    }

    inline constexpr void Vertex::setNormal(const QVector3D &normal)
    {
        mNormal = normal;
    }

    inline constexpr void Vertex::setTexture(const QVector2D &texture)
    {
        mTexture = texture;
    }

    inline constexpr int Vertex::getPositionOffset()
    {
        return offsetof(Vertex, mPosition);
    }

    inline constexpr int Vertex::getNormalOffset()
    {
        return offsetof(Vertex, mNormal);
    }

    inline constexpr int Vertex::getTextureOffset()
    {
        return offsetof(Vertex, mTexture);
    }

    inline constexpr int Vertex::getStride()
    {
        return sizeof(Vertex);
    }

    constexpr Vertex::Vertex(float x, float y, float z, float nx, float ny, float nz, float u, float v)
    {
        mPosition = QVector3D{x, y, z};
        mNormal = QVector3D{nx, ny, nz};
        mTexture = QVector2D{u, v};
    }
}

#endif //HRA2016_VERTEX_H
