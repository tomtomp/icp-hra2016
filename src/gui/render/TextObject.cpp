/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "TextObject.h"

namespace gui
{
    const std::vector<gui::Vertex> TextObject::RECT_VERTEX_DATA{
        // x, y, z, nx, ny, nz, u, v
        /*
        gui::Vertex{1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f},
        gui::Vertex{-1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
        gui::Vertex{-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        gui::Vertex{1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f},
         */
        gui::Vertex{1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f},
        gui::Vertex{0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
        gui::Vertex{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        gui::Vertex{1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f},
    };

    const std::vector<unsigned int> TextObject::RECT_INDICES{
        // Prepared for GL_TRIANGLES
        0, 1, 2,
        0, 2, 3
    };

    TextObject::TextObject(const QString &text, const QFont &font, const QColor &color,
                           const QColor &background, const QVector3D &location) :
        RenderableObject(),
        mFont(font),
        mColor(color),
        mBackground(background)
    {
        mRectMesh = new gui::Mesh(RECT_VERTEX_DATA, RECT_INDICES);
        setMesh(mRectMesh);

        mTextTexture = new gui::Texture(prepareTexture(text, font, color, background));
        setTexture(mTextTexture);

        getTrans().setTranslate(location);
    }

    TextObject::~TextObject()
    {
        if (mRectMesh)
            delete mRectMesh;
        if (mTextTexture)
            delete mTextTexture;
    }

    void TextObject::updateText(const QString &text, const QFont &font, const QColor &color, const QColor &background)
    {
        mFont = font;
        mColor = color;
        mBackground = background;

        mTextTexture->updateTexture(prepareTexture(text, font, color, background));
    }

    QImage TextObject::prepareTexture(const QString &text, const QFont &font, const QColor &color,
                                      const QColor &background)
    {
        QFontMetrics fm(font);
        int width = fm.width(text);
        int height = fm.height();

        QVector3D scale{static_cast<float>(width), static_cast<float>(height), 0.0f};
        //scale.normalize();

        getTrans().setScale(scale);

        QImage textImage(width, height, QImage::Format_RGBA8888);
        textImage.fill(background);

        mPainter.begin(&textImage);
        mPainter.setBrush(color);
        mPainter.setPen(color);
        mPainter.setFont(font);
        mPainter.drawText(0, height, text);
        mPainter.end();

        return textImage;
    }

    void TextObject::updateText(const QString &text)
    {
        mTextTexture->updateTexture(prepareTexture(text, mFont, mColor, mBackground));
    }
}
