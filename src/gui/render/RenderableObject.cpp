/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "RenderableObject.h"

namespace gui
{
    RenderableObject::RenderableObject(gui::Mesh *meshPtr, gui::Texture *texturePtr,
                                       const QVector3D &location) :
        GameObject(location),
        mMesh(meshPtr),
        mTexture(texturePtr)
    {
    }

    void RenderableObject::render(QOpenGLFunctions *fun, QOpenGLShaderProgram *program, const QMatrix4x4 &vp,
                                  const QMatrix4x4 &shadowVP)
    {
        if (getVisible() && mMesh)
        {
            const QMatrix4x4 &m = getTrans().getMatrix();
            QMatrix4x4 mvp = vp * m;
            QMatrix3x3 inverseTransposeModelMatrix = m.normalMatrix();
            program->setUniformValue("mvp", mvp);
            program->setUniformValue("m", m);
            program->setUniformValue("mInvTrans", inverseTransposeModelMatrix);
            program->setUniformValue("lightness", getLightness());
            program->setUniformValue("depthBiasMvp", shadowVP * m);

            if (mTexture)
                mTexture->use(fun, program);

            mMesh->render(fun, program);

            if (mTexture)
                mTexture->unuse();
        }
    }

    RenderableObject::~RenderableObject()
    {
    }
}
