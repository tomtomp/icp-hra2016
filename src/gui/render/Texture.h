/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_TEXTURE_H
#define HRA2016_TEXTURE_H

#include <QString>
#include <QImage>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

namespace gui
{
    /**
     * Texture for use in object rendering.
     */
    class Texture
    {
    public:
        Texture(const Texture &other) = delete;
        Texture(Texture &&other) = delete;

        /**
         * Load the texture and allocate required data on the GPU.
         * @param path Path to the texture.
         */
        Texture(const QString &path);

        /**
         * Load the texture and allocate required data on the GPU.
         * @param image Image to load the texture from.
         */
        Texture(const QImage &image);

        /**
         * Prepare this texture for rendering and bind it.
         * @param fun OpenGL functions for the active context.
         * @param program OpenGL shader program to use this texture for.
         */
        void use(QOpenGLFunctions *fun, QOpenGLShaderProgram *program);

        /**
         * Stop using this texture.
         */
        void unuse();

        /**
         * Update the texture to the given image.
         * @param img Image to set the texture to.
         */
        void updateTexture(const QImage &img);

        /**
         * Set the wrap mode of this texture to the given mode.
         * @param wrapMode The required mode.
         */
        void setWrap(QOpenGLTexture::WrapMode wrapMode);

        ~Texture();
    private:
        /**
         * Set the texture parameters.
         */
        void setParameters();

        /// Texture buffer for texture data.
        QOpenGLTexture mTextureBuffer;
    protected:
    };
}

#endif //HRA2016_TEXTURE_H
