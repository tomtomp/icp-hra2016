/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAMEOBJECT_H
#define HRA2016_GAMEOBJECT_H

#include "transform/TransformMatrix3D.h"

namespace gui
{
    /**
     * Object inside the game world.
     */
    class GameObject
    {
    public:
        /**
         * Create an object on given location.
         * @param Location of this object.
         */
        GameObject(const QVector3D &location = {0.0, 0.0, 0.0});

        /**
         * Get editable transformation of this object.
         * @return Objects transformation matrix.
         */
        trans::TransformMatrix3D &getTrans()
        {
            return mTrans;
        }

        /**
         * Get transformation of this object.
         * @return Objects transformation matrix.
         */
        const trans::TransformMatrix3D &getTrans() const
        {
            return mTrans;
        }

        /**
         * Is this object visible?
         * @return Returns true, if this object is set as visible.
         */
        bool getVisible() const
        {
            return mVisible;
        }

        /**
         * Set this object visibility flag to given value.
         * @param isVisible Value to set the visibility to.
         */
        void setVisible(bool isVisible)
        {
            mVisible = isVisible;
        }

        /**
         * Set the lightness of this object.
         * @param lightness The new lightness value.
         */
        void setLightness(int lightness)
        {
            mLightness = lightness;
        }

        /**
         * Get the lightness value of this object.
         * @return The lightness value of this object.
         */
        int getLightness() const
        {
            return mLightness;
        }
    private:
        ///Transformation of this object.
        trans::TransformMatrix3D mTrans;

        /// Is this object visible?
        bool mVisible;

        /// Lightness of this object, 0 = default.
        int mLightness{0};
    protected:
    };
}

#endif //HRA2016_GAMEOBJECT_H
