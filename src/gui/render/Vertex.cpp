/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Vertex.h"

namespace gui
{
    std::ostream &operator<<(std::ostream &out, const Vertex &vertex)
    {
        out << "(" << vertex.mPosition.x() << ", " << vertex.mPosition.y() << ", " << vertex.mNormal.z() << ")" <<
        "[" << vertex.mNormal.x() << ", " << vertex.mNormal.y() << ", " << vertex.mNormal.z() << ")" <<
        "{" << vertex.mTexture.x() << ", " << vertex.mTexture.y() << "}";
        return out;
    }
}
