/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_MESH_H
#define HRA2016_MESH_H

#include "MeshLoader.h"
#include "Vertex.h"

#include <QString>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

namespace gui
{
    /**
     * Graphic mesh object.
     */
    class Mesh
    {
    public:
        Mesh(const Mesh &other) = delete;
        Mesh(Mesh &&other) = delete;

        /**
         * Load and allocate required data on the GPU.
         * @param path Path to the mesh.
         */
        Mesh(const QString &path);

        /**
         * Load and allocate required data on the GPU.
         * @param vertexData The vertex data.
         * @param indices Indices used for rendering.
         */
        Mesh(const std::vector<gui::Vertex> &vertexData, const std::vector<unsigned int> &indices);

        /**
         * Render this mesh, using given shader program.
         * @param fun OpenGL functions for the current context.
         * @param program Shader program to use.
         */
        void render(QOpenGLFunctions *fun, QOpenGLShaderProgram *program);

        ~Mesh();
    private:
        /**
         * Allocate the data on the GPU.
         */
        void initGL();

        /// Loaded mesh file.
        gui::MeshLoader *mMesh{nullptr};

        /// Buffer holding vertexes of this mesh.
        QOpenGLBuffer mVertexBuffer;

        /// Buffer holding indexes of this mesh.
        QOpenGLBuffer mIndexBuffer;
    protected:
    };
}

#endif //HRA2016_MESH_H
