/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GUIBASE_H
#define HRA2016_GUIBASE_H

#include "GameWidget.h"
#include "NewGameDialog.h"

#include <iostream>

#include <QMainWindow>
#include <QStatusBar>
#include <QMenuBar>
#include <QToolBar>
#include <QMessageBox>
#include <QVBoxLayout>

namespace gui
{
    /**
     * GUI main window class.
     */
    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        /**
         * Construct new main window.
         */
        MainWindow();

        /**
         * Destruct main window and all of its components.
         */
        ~MainWindow();
    private slots:
        void newGame();
        void loadGame();
        void undoGame();
        void redoGame();
        void saveGame();
        void about();
        void aboutQt();
        void help();
    private:
        /**
         * Create the menus.
         */
        void createMenus();

        /**
         * Called on application quit.
         */
        void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

        /// Widget containing game views.
        gui::GameWidget *gameWidget;

        /// Components
        QAction *newGameAction{nullptr};
        QAction *loadGameAction{nullptr};
        QAction *exitAction{nullptr};
        QAction *undoGameAction{nullptr};
        QAction *redoGameAction{nullptr};
        QAction *saveGameAction{nullptr};
        QAction *aboutAction{nullptr};
        QAction *aboutQtAction{nullptr};
        QAction *helpAction{nullptr};
        /// End of components
    protected:
    };
}

#endif // HRA2016_GUIBASE_H
