/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "MessageBox.h"

namespace core
{
    MessageBox::MessageBox()
    {

    }

    void MessageBox::sendMessage(std::shared_ptr<const core::Message> msg)
    {
        std::lock_guard<std::mutex> lock(mBoxMutex);
        mBox.push(msg);
        mMessageReceivedCV.notify_one();
    }

    std::shared_ptr<const Message> MessageBox::receiveMessage()
    {
        std::lock_guard<std::mutex> lock(mBoxMutex);
        if (mBox.empty())
            throw std::runtime_error("Message box is empty, cannot receive message.");
        auto msg = mBox.front();
        mBox.pop();
        return msg;
    }

    void MessageBox::waitForMessage()
    {
        std::unique_lock<std::mutex> lock(mBoxMutex);
        mMessageReceivedCV.wait(lock, [&]{
            return !mBox.empty();
        });
    }
}
