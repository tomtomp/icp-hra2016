/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "BaseInterface.h"

namespace core
{
    void BaseInterface::cbGameReady()
    {
        util::Logger::logDebug("Setting ready flag.");
        mReady = true;
        mWaiterCV.notify_one();
    }

    void BaseInterface::cbInputRequest()
    {
        util::Logger::logDebug("Setting input flag.");
        mInput = true;
        mWaiterCV.notify_one();
    }

    bool BaseInterface::getResetReady()
    {
        bool ready{mReady.exchange(false)};
        return ready;
    }

    bool BaseInterface::getResetInput()
    {
        bool input{mInput.exchange(false)};
        return input;
    }

    void BaseInterface::waitForChange()
    {
        std::unique_lock<std::mutex> lock(mWaiterM);
        mWaiterCV.wait(lock, [&] {
            return mReady || mInput || mGameEnded;
        });
    }

    ActionT BaseInterface::getResetAction()
    {
        if (mGameEnded)
            return ActionT::GAME_ENDED;

        bool ready{mReady.exchange(false)};
        bool input{mInput.exchange(false)};

        if (ready && input)
            return ActionT::ERR;

        if (ready)
            return ActionT::GAME_READY;

        if (input)
            return ActionT::INPUT_REQUEST;

        return ActionT::NONE;
    }

    BaseInterface::BaseInterface() :
        mReady(false),
        mInput(false)
    {
    }
}



