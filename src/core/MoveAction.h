/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_MOVEACTION_H
#define HRA2016_MOVEACTION_H

#include <string>
#include <cstdlib>
#include <utility>
#include <vector>
#include <cassert>
#include <array>

#include "interfaces/Serializable.h"

#include "Types.h"

#include "PlayField.h"

namespace core
{
    class GameSimulation;

    /**
     * Action for adding stones.
     */
    class MoveAction : public core::Serializable
    {
    public:
        /**
         * Construct MoveAction from given coordinates.
         * @param x X coordinate.
         */
        MoveAction(const Coords &c, std::size_t playerID) :
            mPrimaryStone(c),
            mPlayerID(playerID)
        {}

        /**
         * Construct this action from given string representation.
         * @param str String representation of the action.
         */
        MoveAction(const std::string &str)
        {
            unserialize(str);
        }

        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const override;

        /**
         * Performs this action.
         * @param pf Playing field.
         */
        void doAction(core::PlayField &pf) const;

        /**
         * Undo this action.
         * @param pf Playing field.
         */
        void undoAction(core::PlayField &pf) const;

        /**
         * Get the primary stone coordinates.
         * @return Coordinates of the primary stone.
         */
        Coords getPrimaryStone() const
        {
            return mPrimaryStone;
        }

        /**
         * Get the vector of the secondary stones.
         * @return Vector of the secondary stones.
         */
        /*
        const std::vector<core::Coords> getSecondaryStones()
        {
            return mSecondaryStones;
        }
         */

        /**
         * Does this action have any secondary stones?
         * @return Returns true, if there are secondary stones.
         */
        bool hasSecondaryStones() const
        {
            //return !mSecondaryStones.empty();
            return mSecondaryStonesCounter != 0;
        }

        /**
         * Get player ID of the player who made this move.
         * @return Player ID.
         */
        std::size_t getPlayerID() const
        {
            return mPlayerID;
        }
    private:
        friend class core::GameSimulation;

        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override;

        /**
         * Add coords for secondary stone position. Stones will be switched
         *  between the primary and secondary stones
         * @param c Coordinates of the secondary stone.
         */
        void addSecondaryStone(Coords c)
        {
            assert(mSecondaryStonesCounter < 8);
            //mSecondaryStones.push_back(c);
            mSecondaryStones[mSecondaryStonesCounter++] = c;
        }

        /// Coordinates of the primary move.
        Coords mPrimaryStone;

        /// ID of the player who makes this move.
        std::size_t mPlayerID;

        /// Vector of stone, between which the stones will roll over. 8 directions possible.
        //std::vector<Coords> mSecondaryStones;
        std::array<Coords, 8> mSecondaryStones;

        /// Counter for the secondary stones.
        std::size_t mSecondaryStonesCounter{0};
    protected:
    };
}



#endif //HRA2016_MOVEACTION_H
