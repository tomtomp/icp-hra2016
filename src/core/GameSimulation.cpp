/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "GameSimulation.h"

namespace core
{
    const std::vector<core::Coords> GameSimulation::DIRECTIONS{
        core::Coords(1, 0),
        core::Coords(1, 1),
        core::Coords(0, 1),
        core::Coords(-1, 1),
        core::Coords(-1, 0),
        core::Coords(-1, -1),
        core::Coords(0, -1),
        core::Coords(1, -1)
    };

    bool GameSimulation::undoPlay(core::PlayField &pf, core::ActionHolder &ah) const
    {
        if (!ah.canGetCurrTop())
            return false;
        else
        {
            core::MoveAction action = ah.getCurrTop();

            action.undoAction(pf);

            ah.prec();

            return true;
        }
    }

    bool GameSimulation::redoPlay(core::PlayField &pf, core::ActionHolder &ah) const
    {
        if (!ah.succ())
            return false;
        else
        {
            core::MoveAction action = ah.getCurrTop();

            action.doAction(pf);

            return true;
        }
    }

    std::vector<core::MoveAction> GameSimulation::getAllPlays(const core::PlayField &pf, char playerID) const
    {
        std::vector<MoveAction> result;

        std::size_t playFieldSize = pf.getSize();

        for (std::size_t jjj = 0; jjj < playFieldSize; ++jjj)
        {
            for (std::size_t iii = 0; iii < playFieldSize; ++iii)
            {
                if (pf.isFieldEmpty(core::Coords(iii, jjj)))
                {
                    core::MoveAction newAction(core::Coords(iii, jjj), static_cast<std::size_t>(playerID));
                    fillAction(newAction, pf);

                    if (canMakePlay(newAction, pf))
                        result.push_back(newAction);
                }
            }
        }

        return result;
    }

    void GameSimulation::fillAction(core::MoveAction &action, const core::PlayField &pf) const
    {
        if (canMakePlay(action, pf))
            return;

        core::Coords start = action.getPrimaryStone();
        core::Coords newSecondary;
        char myID = static_cast<char>(action.getPlayerID());
        char currID;
        const char EMPTY{pf.EMPTY_ID};
        bool first;

        /*
        for (const core::Coords &direction : DIRECTIONS)
        {
            newSecondary = findDirectionStone(static_cast<char>(action.getPlayerID()), direction,
                                              start, pf);
            if (newSecondary != start)
                action.addSecondaryStone(newSecondary);
        }
         */

        // Horizontal right
        newSecondary = core::Coords{start.first + 1, start.second};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.first++;
            first = false;
        }

        // Horizontal left
        newSecondary = core::Coords{start.first - 1, start.second};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.first--;
            first = false;
        }

        // Vertical up
        newSecondary = core::Coords{start.first, start.second + 1};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.second++;
            first = false;
        }

        // Vertical down
        newSecondary = core::Coords{start.first, start.second - 1};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.second--;
            first = false;
        }

        // Diagonal right up
        newSecondary = core::Coords{start.first + 1, start.second + 1};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.first++;
            newSecondary.second++;
            first = false;
        }

        // Diagonal left up
        newSecondary = core::Coords{start.first - 1, start.second + 1};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.first--;
            newSecondary.second++;
            first = false;
        }

        // Diagonal right down
        newSecondary = core::Coords{start.first + 1, start.second - 1};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.first++;
            newSecondary.second--;
            first = false;
        }

        // Diagonal left down
        newSecondary = core::Coords{start.first - 1, start.second - 1};
        first = true;
        while (pf.isInPlayingField(newSecondary))
        {
            currID = pf.getStoneOID(newSecondary);

            if (currID == myID && !first)
            {
                action.addSecondaryStone(newSecondary);
                break;
            }
            else if (currID == myID || currID == EMPTY)
                break;

            newSecondary.first--;
            newSecondary.second--;
            first = false;
        }
    }

    core::Coords GameSimulation::findDirectionStone(char playerID, const core::Coords &direction,
                                                    const core::Coords &start, const core::PlayField &pf) const
    {
        core::Coords curCoords{start};
        const char EMPTY{pf.EMPTY_ID};
        char currID{0};
        bool first{true};

        advanceCoords(curCoords, direction);

        while (pf.isInPlayingField(curCoords))
        {
            currID = pf.getStoneOID(curCoords);

            if (currID == playerID && !first)
                return curCoords;
            else if (currID == playerID)
                return start;
            else if (currID == EMPTY)
                return start;

            advanceCoords(curCoords, direction);
            first = false;
        }

        return start;
    }

    bool GameSimulation::makePlay(const core::MoveAction &action, core::PlayField &pf,
                                  core::ActionHolder &ah) const
    {
        core::MoveAction fullAction = action;
        fillAction(fullAction, pf);

        if (!canMakePlay(fullAction, pf))
        {
#ifndef NDEBUG
            util::Logger::logDebug(std::string("Cannot make this play! : ") +
                                   std::to_string(pf.isFieldEmpty(fullAction.getPrimaryStone())) +
                                   std::to_string(fullAction.hasSecondaryStones()));
#endif
            return false;
        }

        fullAction.doAction(pf);
        ah.pushAfterCurrTop(fullAction);

        return true;
    }

    bool GameSimulation::canMakePlay(const core::MoveAction &action, const core::PlayField &pf) const
    {
        return pf.isFieldEmpty(action.getPrimaryStone()) && action.hasSecondaryStones();
    }
}
