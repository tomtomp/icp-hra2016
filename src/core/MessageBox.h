/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_MESSAGEBOX_H
#define HRA2016_MESSAGEBOX_H

#include <queue>
#include <memory>
#include <condition_variable>

#include "messages/Message.h"
#include "Types.h"

namespace core
{
    /**
     * Used for thread-safe message sending.
     */
    class MessageBox
    {
    public:
        MessageBox(const MessageBox &other) = delete;
        MessageBox(MessageBox &&other) = delete;

        /**
         * Construct new message box.
         */
        MessageBox();

        /**
         * Send a message to this object.
         * @param msg Message to send.
         */
        void sendMessage(std::shared_ptr<const core::Message> msg);

        /**
         * Look into the message box and get the next message. Non-blocking.
         * @return The next message, or nullptr, if no message is present.
         */
        std::shared_ptr<const core::Message> receiveMessage();

        /**
         * Are there any messages in this message box?
         * @return If there are any messages in this message box, returns true.
         */
        bool isMessagePresent()
        {
            std::lock_guard<std::mutex> lock(mBoxMutex);
            return !mBox.empty();
        }

        /**
         * Clear the message box.
         */
        void clearMessageBox()
        {
            std::lock_guard<std::mutex> lock(mBoxMutex);
            std::queue<std::shared_ptr<const core::Message> >().swap(mBox);
            mMessageReceivedCV.notify_all();
        }

        /**
         * Wait until there is a message to receive.
         */
        void waitForMessage();
    private:
        /// Box for the messages.
        std::queue<std::shared_ptr<const core::Message> > mBox;

        /// Condition variable triggered when a message is received.
        std::condition_variable mMessageReceivedCV;

        /// Mutex for accessing the message box.
        std::mutex mBoxMutex;
    protected:
    };
}

#endif //HRA2016_MESSAGEBOX_H
