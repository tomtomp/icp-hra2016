/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Message.h"

namespace core
{
    Message::Message(core::MessageT type) :
        mType(type)
    {
    }

    std::string Message::getMessageTypeStr(MessageT type)
    {
        switch (type)
        {
            case MessageT::NEXT_MSG:
                return "Next turn";
            case MessageT::REDO_MSG:
                return "Redo";
            case MessageT::REDO_FAIL_MSG:
                return "Redo failed";
            case MessageT::UNDO_MSG:
                return "Undo";
            case MessageT::UNDO_FAIL_MSG:
                return "Undo failed";
            case MessageT::PUT_MSG:
                return "Put";
            case MessageT::INV_MOVE_MSG:
                return "Invalid move";
            case MessageT::REQ_MOVE_MSG:
                return "Requesting move";
            case MessageT::READY_MSG:
                return "Ready";
            case MessageT::END_MSG:
                return "End";
            case MessageT::ENDED_MSG:
                return "Ended";
            default:
                return "Unknown";
        }
    }
}

