/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_MESSAGE_H
#define HRA2016_MESSAGE_H

#include <string>

namespace core
{
    /**
     * Message types.
     */
    enum class MessageT
    {
        NEXT_MSG = 0,
        REDO_MSG,
        REDO_FAIL_MSG,
        UNDO_MSG,
        UNDO_FAIL_MSG,
        PUT_MSG,
        INV_MOVE_MSG,
        REQ_MOVE_MSG,
        READY_MSG,
        END_MSG,
        ENDED_MSG,
        UNK_MSG
    };

    /**
     * Message base class, can be sent to a message box.
     */
    class Message
    {
    public:
        /**
         * Create a message with given type.
         */
        Message(MessageT type);

        /**
         * Get the type of this message.
         * @return Type of this message.
         */
        MessageT getType() const
        {
            return mType;
        }

        virtual ~Message() {};

        /**
         * Get the message type as a readable string.
         * @param type Type of the message.
         * @return String representation of the type.
         */
        static std::string getMessageTypeStr(MessageT type);
    private:
        /// Type of the message.
        MessageT mType;
    protected:
    };
}

#endif //HRA2016_MESSAGE_H
