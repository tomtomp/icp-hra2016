/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "CoordsMessage.h"

namespace core
{
    CoordsMessage::CoordsMessage(const core::Coords &c) :
        Message(MessageT::PUT_MSG),
        mCoords(c)
    {
    }

    Coords CoordsMessage::getCoordsFromMessage(const std::shared_ptr<const Message> &msg)
    {
        std::shared_ptr<const core::CoordsMessage> cMsg = std::dynamic_pointer_cast<const core::CoordsMessage>(msg);

        if (!cMsg)
            throw std::bad_cast();

        return cMsg->getCoords();
    }
}
