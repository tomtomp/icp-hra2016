/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "MessageFactory.h"

namespace core
{
    std::shared_ptr<const core::Message> MessageFactory::createMessage(core::MessageT type) const
    {
        if (type == MessageT::PUT_MSG)
        {
            util::Logger::logDebug("Cannot create coordinates message as normal message!");
            throw std::runtime_error("Cannot create coordinates message as normal message!");
        }

        return std::shared_ptr<const Message>(new core::Message(type));
    }

    std::shared_ptr<const core::Message> MessageFactory::createCoordsMessage(const core::Coords &c) const
    {
        return std::shared_ptr<const Message>(new core::CoordsMessage(c));
    }
}
