/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_COORDSMESSAGE_H
#define HRA2016_COORDSMESSAGE_H

#include <memory>
#include <typeinfo>
#include "Message.h"
#include "../Types.h"

namespace core
{
    class CoordsMessage : public Message
    {
    public:
        /**
         * Create a message containing given coordinates.
         */
        CoordsMessage(const core::Coords &c);

        /**
         * Get coordinates contained in this message.
         * @return Coordinates Contained in this message.
         */
        core::Coords getCoords() const
        {
            return mCoords;
        }

        /**
         * Get coordinates from given message.
         * @param msg Message to get the coordinates from.
         * @return Returns the coordinates from the message.
         */
        static core::Coords getCoordsFromMessage(const std::shared_ptr<const core::Message> &msg);

        virtual ~CoordsMessage() {};
    private:
        /// Coordinates contained in this message.
        core::Coords mCoords;
    protected:
    };
}

#endif //HRA2016_COORDSMESSAGE_H
