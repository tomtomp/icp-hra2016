/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_MESSAGEFACTORY_H
#define HRA2016_MESSAGEFACTORY_H

#include <memory>

#include "../../util/Logger.h"
#include "../Types.h"
#include "Message.h"
#include "CoordsMessage.h"

namespace core
{
    /**
     * Factory used for creating messages.
     */
    class MessageFactory
    {
    public:
        /**
         * Create unspecialized message type.
         * @param type Type of the message.
         * @return Newly constructed message ptr.
         */
        std::shared_ptr<const core::Message> createMessage(MessageT type) const;

        /**
         * Create coordinates message.
         * @param c Coordinates to insert to the message.
         * @return Newly constructed message ptr.
         */
        std::shared_ptr<const core::Message> createCoordsMessage(const core::Coords &c) const;
    private:
    protected:
    };
}

#endif //HRA2016_MESSAGEFACTORY_H
