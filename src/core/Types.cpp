/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Types.h"

namespace core
{
    void advanceCoords(core::Coords &c, const core::Coords &direction)
    {
        c.first += direction.first;
        c.second += direction.second;
    }

    std::string getCoordsStr(const core::Coords &c)
    {
        std::stringstream ss;

        ss << "[" << c.first << " , " << c.second << "]";
        return ss.str();
    }
}

