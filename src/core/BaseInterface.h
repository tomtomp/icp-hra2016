/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_BASEINTERFACE_H
#define HRA2016_BASEINTERFACE_H

#include "../util/Logger.h"

#include <atomic>
#include <mutex>
#include <condition_variable>

namespace core
{
    enum class ActionT
    {
        NONE = 0,
        GAME_READY,
        INPUT_REQUEST,
        GAME_ENDED,
        ERR = 99
    };

    /**
     * Base class for any interface.
     */
    class BaseInterface
    {
    public:
        BaseInterface();

        /**
         * Callback method, called when game core is in ready state.
         */
        void cbGameReady();

        /**
         * Callback method, called when game core is in input request state.
         */
        void cbInputRequest();
    private:
        /// Signalizes, when the game is in ready state.
        volatile std::atomic_bool mReady;

        /// Signalizes, when the game is requesting input.
        volatile std::atomic_bool mInput;

        /// Did the game end?
        volatile std::atomic_bool mGameEnded;

        /// Used for waiting for a change.
        std::condition_variable mWaiterCV;

        /// Mutex used for waiting for change.
        std::mutex mWaiterM;
    protected:
        /**
         * Get and reset the "game ready" signal.
         * @return Returns the state of the signal.
         */
        bool getResetReady();

        /**
         * Get and reset the "request input" signal.
         * @return Returns the state of the signal.
         */
        bool getResetInput();

        /**
         * Wait for change of any signal (ready or input request).
         */
        void waitForChange();

        /**
         * Reset the game ended flag.
         */
        void resetGameEnded()
        {
            mGameEnded = false;
        }

        /**
         * Set the game ended flag.
         */
        void setGameEnded()
        {
            mGameEnded = true;
        }

        /**
         * Get action signaled by game core.
         * @return Action requested by the game core. If ActionT::ERR is returned, both signals were set.
         */
        ActionT getResetAction();
    };
}

#endif //HRA2016_BASEINTERFACE_H
