/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "PlayerHolder.h"
#include "players/HumanPlayer.h"
#include "players/PlayerFactory.h"
#include "players/AIPlayer.h"

namespace core
{
    core::PlayerHolder::PlayerHolder(std::size_t maxPlayers) :
        mMaxPlayers{maxPlayers}
    {
        if (maxPlayers > 127)
        {
            util::Logger::logError("Cannot create a player holder for that many players!");
            throw std::runtime_error("Cannot create a player holder for that many players!");
        }
    }

    std::string PlayerHolder::serialize() const
    {
        if (!isFull())
        {
            util::Logger::logError("Cannot serialize unfinished player holder!");
            throw std::runtime_error("Cannot serialize unfinished player holder!");
        }

        std::stringstream ss;

        for (const std::shared_ptr<const core::Player> &player : mPlayers)
            ss << player->serialize() << "\n";

        return ss.str();
    }

    void PlayerHolder::unserialize(const std::string &str)
    {
        std::stringstream ss(str);
        std::string playerStr;

        mPlayers.clear();
        mCurrPlayers = 0;
        mCurrPlayerID = 1;

        while (std::getline(ss, playerStr))
        {
            mCurrPlayers++;

            if (mCurrPlayers > mMaxPlayers)
            {
                util::Logger::logError("Too many players to load!");
                throw std::runtime_error("Too many players to load!");
            }

            mPlayers.push_back(mPlayerFactory.unserializePlayer(playerStr, mCurrPlayers));
        }
    }

    void PlayerHolder::addPlayer(const std::string &type, const std::string &name, const std::string aiType)
    {
        std::shared_ptr<core::Player> player{nullptr};

        if (type == core::HumanPlayer::TYPE_NAME)
        {
            player = mPlayerFactory.createHuman(name, mCurrPlayers + 1);
            if (!player)
            {
                util::Logger::logError("Failed to create human player.");
                throw std::runtime_error("Failed to create human player");
            }
        }
        else if (type == core::AIPlayer::TYPE_NAME)
        {
            player = mPlayerFactory.createAI(name, mCurrPlayers + 1, aiType);
            if (!player)
            {
                util::Logger::logError("Failed to create AI player.");
                throw std::runtime_error("Failed to create AI player");
            }
        }

        mPlayers.push_back(player);
        mCurrPlayers++;
    }

    void PlayerHolder::loadFromString(const std::string &str)
    {
        unserialize(str);
    }
}
