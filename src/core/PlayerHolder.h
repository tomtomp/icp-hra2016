/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_PLAYERHOLDER_H
#define HRA2016_PLAYERHOLDER_H

#include <memory>

#include "interfaces/Serializable.h"
#include "players/Player.h"
#include "players/PlayerFactory.h"

namespace core
{
    class PlayerHolder : public core::Serializable
    {
    public:
        /**
         * Create an empty player holder with maximum number of players.
         * @param maxPlayers Maximum players of players.
         */
        PlayerHolder(std::size_t maxPlayers);

        /**
         * Add player with given parameters.
         * @param type Type of the player.
         * @param name Name of the player.
         * @param aiType Type of the AI.
         */
        void addPlayer(const std::string &type, const std::string &name, const std::string aiType = "");


        /**
         * Is this player holder full?
         * @return Returns true, if it is full.
         */
        bool isFull() const
        {
            return mMaxPlayers == mCurrPlayers;
        }

        /**
         * Get the number of players remaining, to be added.
         * @return Number of players remaining, to be added.
         */
        std::size_t getNumRemaining() const
        {
            return mMaxPlayers - mCurrPlayers;
        }

        /**
         * Get the available AI types.
         * @return Vector containing names of the AIs.
         */
        const std::vector<std::string> &getAiTypes() const
        {
            return mPlayerFactory.getAiTypes();
        }

        /**
         * Get ID of the player currently playing.
         * @return ID of the currently playing player.
         */
        char getCurrPlayerID() const
        {
            if (!isFull())
            {
                util::Logger::logError("Cannot access unfinished player holder!");
                throw std::runtime_error("Cannot access unfinished player holder!");
            }

            //return static_cast<std::size_t>(mCurrPlayerID);
            return mCurrPlayerID;
        }

        /**
         * Get the currently playing player.
         * @return The currently playing player.
         */
        std::shared_ptr<const core::Player> getCurrentPlayer() const
        {
            if (!isFull())
            {
                util::Logger::logError("Cannot access unfinished player holder!");
                throw std::runtime_error("Cannot access unfinished player holder!");
            }

            return mPlayers[mCurrPlayerID - 1];
        }

        /**
         * Get player by ID.
         * @param playerID ID of the player.
         * @return The player with given ID.
         */
        std::shared_ptr<const core::Player> getPlayer(std::size_t playerID) const
        {
            if (playerID < 1 || playerID > mMaxPlayers)
            {
                util::Logger::logError("Player ID out of range!");
                throw std::runtime_error("Player ID out of range!");
            }

            return mPlayers[playerID - 1];
        }

        /**
         * Advance to the next player.
         */
        void advancePlayer()
        {
            if (!isFull())
            {
                util::Logger::logError("Cannot access unfinished player holder!");
                throw std::runtime_error("Cannot access unfinished player holder!");
            }

            mCurrPlayerID = static_cast<char>((mCurrPlayerID % mMaxPlayers) + 1);
        }

        /**
         * Advance by given number of turns.
         */
        void advancePlayer(std::size_t turns)
        {
            if (!isFull())
            {
                util::Logger::logError("Cannot access unfinished player holder!");
                throw std::runtime_error("Cannot access unfinished player holder!");
            }

            mCurrPlayerID = static_cast<char>(((mCurrPlayerID + turns + 1) % mMaxPlayers) + 1);
        }

        /**
         * Get the list of players.
         * @return The list of players.
         */
        const std::vector<std::shared_ptr<const core::Player> > &getListOfPlayers() const
        {
            if (!isFull())
            {
                util::Logger::logError("Cannot access unfinished player holder!");
                throw std::runtime_error("Cannot access unfinished player holder!");
            }

            return mPlayers;
        }

        /**
         * Load players from string.
         * @param str String to load the players from.
         */
        void loadFromString(const std::string &str);

        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const override;
    private:
        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override;

        /// List of players.
        std::vector<std::shared_ptr<const core::Player> > mPlayers;

        /// ID of the player currently playing.
        char mCurrPlayerID{1};

        /// Current number of players.
        std::size_t mCurrPlayers{0};

        /// Maximum number of players.
        std::size_t mMaxPlayers{2};

        /// Factory for creating players.
        core::PlayerFactory mPlayerFactory;
    protected:
    };
}

#endif //HRA2016_PLAYERHOLDER_H
