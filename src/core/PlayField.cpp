/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "PlayField.h"

namespace core
{
    constexpr std::size_t PlayField::MAX_PLAYERS;
    constexpr char PlayField::EMPTY_ID;

    PlayField::PlayField(std::size_t size)
    {
        setSize(size);
    }

    std::string PlayField::serialize() const
    {
        std::stringstream ss;
        ss << mSize << ":";
        for (decltype(mField.size()) iii = 0; iii < mField.size(); ++iii)
        {
            ss << static_cast<int>(mField[iii]);
            if (iii != mField.size() - 1)
                ss << ',';
        }

        ss << "\n";

        return ss.str();
    }

    void PlayField::unserialize(const std::string &str)
    {
        std::istringstream is(str);
        size_t size{0};
        is >> size;
        util::Logger::logDebug(std::string("PlayField unserializing : ") + str);

        mSize = size;

        if (is.peek() != ':')
        {
            util::Logger::logError("Wrong syntax for the playing field string.");
            throw std::runtime_error("Wrong syntax for the PlayField string (newline).");
        }
        is.ignore();

        size_t max{size * size};
        size_t got{0};
        int ownerID{0};

        mField.clear();

        while (is >> ownerID)
        {
            got++;
            if (got > max)
            {
                util::Logger::logError("Too many values for playing field.");
                throw std::runtime_error("Too many values for PlayField.");
            }

            if (is.peek() != ',' && is.peek() != EOF)
            {
                util::Logger::logError("Wrong syntax for the playing field string.");
                throw std::runtime_error("Wrong syntax for the PlayField string (comma).");
            }
            is.ignore();

            if (!isIdOk(ownerID))
            {
                util::Logger::logError("Cannot load playing field, one of the values is out of range.");
                throw std::runtime_error("Cannot load playing field, one of the values is out of range.");
            }

            mField.push_back(ownerID);
        }
    }

    PlayField::PlayField(std::string str)
    {
        unserialize(str);
    }

    void PlayField::setSize(std::size_t size)
    {
        mSize = size;
        mField = std::vector<char>(size * size, EMPTY_ID);

        if (size)
        {
            if (size % 2 != 0)
            {
                util::Logger::logDebug("Cannot create even sized playing field!");
                throw std::runtime_error("Cannot create even sized playing field!");
            }

            // Setup the playing field.
            std::size_t half = size / 2;
            setStoneOID({half - 1, half - 1}, 1);
            setStoneOID({half, half}, 1);
            setStoneOID({half, half - 1}, 2);
            setStoneOID({half - 1, half}, 2);
        }
    }

    void PlayField::flipBetween(const core::Coords &c1, const core::Coords &c2, char playerID)
    {
        core::Coords direction{c2.first - c1.first, c2.second - c1.second};

#ifndef NDEBUG
        util::Logger::logDebug(std::string("Flipping between : ") + getCoordsStr(c1) + " and " + getCoordsStr(c2));
        util::Logger::logDebug(std::string("Direction : ") + getCoordsStr(direction));
#endif

        // direction.first == 0 || direction.second == 0 || direction.first == direction.second
        if (direction.first && direction.second && std::abs(direction.first) != std::abs(direction.second))
        {
            util::Logger::logError("Cannot flip between 2 stones, they are not aligned properly!");
            throw std::runtime_error("Cannot flip between 2 stones, they are not aligned properly! : ");
        }

        // Normalize the "vector"
        if (direction.first)
            direction.first /= std::abs(direction.first);
        if (direction.second)
            direction.second /= std::abs(direction.second);

#ifndef NDEBUG
        util::Logger::logDebug(std::string("Normalized direction : ") + getCoordsStr(direction));
#endif

        core::Coords currCoords{c1};
        advanceCoords(currCoords, direction);

        while (isInPlayingField(currCoords) && currCoords != c2)
        {
            setStoneOID(currCoords, playerID);
            advanceCoords(currCoords, direction);
        }
        util::Logger::logDebug("Done with flipBetween.");
    }

    bool PlayField::isIdOk(char playerID)
    {
        return playerID >=0 && playerID <= static_cast<char>(MAX_PLAYERS);
    }
}


