/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_SERIALIZABLE_H
#define HRA2016_SERIALIZABLE_H

#include <string>

namespace core
{
    class Serializable
    {
    public:
        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const = 0;
    private:
    protected:
        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) = 0;
    };
}

#endif //HRA2016_SERIALIZABLE_H
