/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_TYPES_H
#define HRA2016_TYPES_H

#include <string>
#include <cstdlib>
#include <utility>
#include <sstream>

namespace core
{
    /// Coordinates type for the playing field.
    using Coords = std::pair<long long, long long>;

    /**
     * Advance coordinates by given vector.
     * @param c Starting coordinates, will be changed.
     * @param direction Directional vector.
     */
    void advanceCoords(core::Coords &c, const core::Coords &direction);

    /**
     * Get string version of coordinate.
     * @param c Coordinate to stringify.
     * @return String version of given coordinate.
     */
    std::string getCoordsStr(const core::Coords &c);
}

#endif //HRA2016_TYPES_H
