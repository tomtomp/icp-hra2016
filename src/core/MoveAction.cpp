/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "MoveAction.h"

namespace core
{
    std::string MoveAction::serialize() const
    {
        std::stringstream ss;
        ss << mPlayerID << "." << mPrimaryStone.first << "," << mPrimaryStone.second << ":";
        /*
        auto lastIndex = mSecondaryStones.cend();
        for (auto it = mSecondaryStones.cbegin(); it < lastIndex; ++it)
         */
        for (std::size_t iii = 0; iii < mSecondaryStonesCounter; ++iii)
        {
            ss << mSecondaryStones[iii].first << "," << mSecondaryStones[iii].second;
            if (iii != mSecondaryStonesCounter - 1)
                ss << ";";
        }

        return ss.str();
    }

    void MoveAction::unserialize(const std::string &str)
    {
        /**
         * MoveAction string syntax:
         *      ID.x1,y1:x2,y2;x3,y3;...;xn,yn
         *      where n <= 8
         */

        std::stringstream ss(str);
        std::size_t num1{0}, num2{0};
        Coords coords{0, 0};
        std::size_t playerID{0};

        if (! (ss >> playerID))
        {
            util::Logger::logError("Wrong action string syntax.");
            throw std::runtime_error("Wrong MoveAction string syntax (playerID).");
        }

        if (playerID > 2)
        {
            util::Logger::logError("Wrong action string syntax.");
            throw std::runtime_error("Wrong MoveAction string syntax (playerID out of range).");
        }

        mPlayerID = playerID;

        if (ss.peek() != '.')
        {
            util::Logger::logError("Wrong action string syntax.");
            throw std::runtime_error("Wrong MoveAction string syntax (playerID dot).");
        }
        ss.ignore();

        for (int iii = 0; iii <= 8 && ss; ++iii)
        {
            if (! (ss >> num1))
            {
                if (iii)
                    break;
                util::Logger::logError("Wrong action string syntax.");
                throw std::runtime_error("Wrong MoveAction string syntax (num1).");
            }

            if (ss.peek() != ',')
            {
                util::Logger::logError("Wrong action string syntax.");
                throw std::runtime_error("Wrong MoveAction string syntax (comma).");
            }
            ss.ignore();

            if (! (ss >> num2))
            {
                util::Logger::logError("Wrong action string syntax.");
                throw std::runtime_error("Wrong MoveAction string syntax (num2).");
            }

            if (iii == 0)
            {
                mPrimaryStone = {num1, num2};

                if (ss.peek() != ':')
                {
                    util::Logger::logError("Wrong action string syntax.");
                    throw std::runtime_error("Wrong MoveAction string syntax (colon).");
                }
                ss.ignore();
            }
            else
            {
                if (ss.peek() != ';' && ss.peek() != EOF)
                {
                    util::Logger::logError("Wrong action string syntax.");
                    throw std::runtime_error("Wrong MoveAction string syntax (semicolon).");
                }
                ss.ignore();

                coords = {num1, num2};
                //mSecondaryStones.push_back(coords);
                addSecondaryStone(coords);
            }
        }
    }

    void MoveAction::doAction(core::PlayField &pf) const
    {
#ifndef NDEBUG
        util::Logger::logDebug(std::string("Started doAction : ") + serialize());
#endif
        //for (const core::Coords &secondary : mSecondaryStones)
        for (std::size_t iii = 0; iii < mSecondaryStonesCounter; ++iii)
            pf.flipBetween(mPrimaryStone, mSecondaryStones[iii], static_cast<char>(mPlayerID));
        pf.setStoneOID(mPrimaryStone, static_cast<char>(mPlayerID));
#ifndef NDEBUG
        util::Logger::logDebug("Done with doAction.");
#endif
    }

    void MoveAction::undoAction(core::PlayField &pf) const
    {
#ifndef NDEBUG
        util::Logger::logDebug(std::string("Started undoAction : ") + serialize());
#endif
        char otherPlayerID = static_cast<char>((mPlayerID % pf.MAX_PLAYERS) + 1);
        //for (const core::Coords &secondary : mSecondaryStones)
        for (std::size_t iii = 0; iii < mSecondaryStonesCounter; ++iii)
            pf.flipBetween(mPrimaryStone, mSecondaryStones[iii], otherPlayerID);
        pf.setStoneOID(mPrimaryStone, pf.EMPTY_ID);
#ifndef NDEBUG
        util::Logger::logDebug("Done with undoAction.");
#endif
    }
}
