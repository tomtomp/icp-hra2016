/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_AIRANDOM_H
#define HRA2016_AIRANDOM_H

#include "AIPlayer.h"

namespace core
{
    /**
     * AI player that playes randomly
     */
    class AIRandom : public AIPlayer
    {
    public:
        /**
         * Create AI player with given name.
         */
        AIRandom(const std::string &name, std::size_t playerID);

        /// Name of this type
        static const std::string TYPE_NAME;

        /**
         * Get the players move action. Gets all possible moves and choose the first one
         * @param pf Current state of the playing field.
         * @return Coords of the move
         */
        virtual Coords getMoveCoords(const core::PlayField &pf) const override;

        /**
         * Get name of this player type.
         * @return Returns the name as string.
         */
        virtual const std::string &getTypeName() const override{
            return TYPE_NAME;
        }

    private:
    protected:
    };
}

#endif //HRA2016_AIRANDOM_H
