/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "PlayerFactory.h"

namespace core
{

    std::shared_ptr<core::Player> PlayerFactory::createHuman(const std::string &name, std::size_t playerID)
    {
        std::shared_ptr<core::HumanPlayer> player{new core::HumanPlayer(name, playerID)};
        return player;
    }

    std::shared_ptr<core::Player> PlayerFactory::createAI(const std::string &name, std::size_t playerID,
                                                          const std::string &typeName)
    {
        util::Logger::logDebug(std::string("AI class name selected " + typeName));
        return createAiByClassName(typeName, name, playerID);
    }

    std::shared_ptr<core::Player> PlayerFactory::unserializePlayer(const std::string &str, std::size_t playerID)
    {
        std::shared_ptr<core::Player> player{nullptr};
        // Parse the serialized string.
        std::regex e("^(.+):([^:]+)$");
        std::smatch match;
        std::regex_match(str, match, e);

        if (match.size() != 3)
        {
            util::Logger::logError(std::string("Unable to read player : ") + str);
            throw std::runtime_error("Player could not be read!");
        }

        std::string playerType(match[1]);

        std::string playerName(match[2]);

        if (playerType == core::HumanPlayer::TYPE_NAME)
        {
            player = std::shared_ptr<core::Player>{new core::HumanPlayer(playerName, playerID)};
        }
        else
        {
            player = createAiByClassName(playerType, playerName, playerID);
        }

        return player;
    }

    std::shared_ptr<AIPlayer> PlayerFactory::createAiByClassName(const std::string &className, const std::string &name,
                                                                 std::size_t playerID)
    {
        if(className == AIRandom::TYPE_NAME)
        {
            std::shared_ptr<core::AIPlayer> player{new core::AIRandom(name, playerID)};
            util::Logger::logDebug(std::string("Constructed AI class name " + AIRandom::TYPE_NAME));
            return player;
        }
        if(className == AIAlphaBeta::TYPE_NAME)
        {
            std::shared_ptr<core::AIPlayer> player{new core::AIAlphaBeta(name, playerID)};
            util::Logger::logDebug(std::string("Constructed AI class name " + AIAlphaBeta::TYPE_NAME));
            return player;
        }
        else
        {
            util::Logger::logError(std::string("Cannot find class with name" + className ));
            throw std::runtime_error("Cannot find class with name " + className );
        }
        return nullptr;
    }

    const std::vector<std::string> &PlayerFactory::getAiTypes() const
    {
        static const std::vector<std::string> aiTypes{
            AIRandom::TYPE_NAME,
            AIAlphaBeta::TYPE_NAME,
            // Add new AIs here...
        };

        return aiTypes;
    }
}



