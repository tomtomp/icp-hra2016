/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "AIRandom.h"

namespace core
{
    const std::string AIRandom::TYPE_NAME{"AIRandom"};

    AIRandom::AIRandom(const std::string &name, std::size_t playerID) :
        AIPlayer(name, playerID)
    {

    }

    Coords AIRandom::getMoveCoords(const PlayField &pf) const
    {
        srand(time(0));
        std::vector<core::MoveAction> moves = mGameSimulator.getAllPlays(pf, getPlayerID());
        size_t randNum = rand() % moves.size();
        return moves[randNum].getPrimaryStone();
    }
}
