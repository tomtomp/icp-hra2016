/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_PLAYER_H
#define HRA2016_PLAYER_H

#include "../interfaces/Serializable.h"

#include "../PlayField.h"
#include "../MoveAction.h"

#include <string>

namespace core
{
    /**
     * Abstract player class.
     */
    class Player : public core::Serializable
    {
    public:
        /**
         * Create a player with given name.
         * @param name Name of the player.
         * @param requiresInput Does this player require input from UI?
         */
        Player(const std::string &name, std::size_t playerID, bool requiresInput) :
            mName(name),
            mPlayerID(playerID),
            mRequiresInput(requiresInput)
        {};

        /**
         * Get the players move action.
         * @param pf Current state of the playing field.
         * @return Coords of the move.
         */
        virtual Coords getMoveCoords(const core::PlayField &pf) const = 0;

        /**
         * Get name of this player type.
         * @return Returns the name as string.
         */
        virtual const std::string &getTypeName() const = 0;

        /**
         * Name getter.
         * @return Returns the name of the player.
         */
        const std::string &getName() const
        {
            return mName;
        }

        /**
         * Get the players id.
         * @return ID of the player.
         */
        std::size_t getPlayerID() const
        {
            return mPlayerID;
        }

        /**
         * Does this player type require input from UI?
         * @return Returns true, if this player requires input from UI.
         */
        bool getRequiresInput() const
        {
            return mRequiresInput;
        }

        virtual ~Player()
        {

        }
    private:
        /// Name of the player.
        const std::string mName;

        /// ID of the player.
        std::size_t mPlayerID;

        /// Does this player type require input from UI?
        bool mRequiresInput;
    protected:
    };
}


#endif //HRA2016_PLAYER_H
