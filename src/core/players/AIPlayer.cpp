/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "AIPlayer.h"

namespace core
{
    const std::string AIPlayer::TYPE_NAME{"AI"};

    AIPlayer::AIPlayer(const std::string &name, std::size_t playerID) :
        Player(name, playerID, false), mGameSimulator{}
    {

    }

}
