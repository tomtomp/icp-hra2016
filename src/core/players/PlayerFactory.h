/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_PLAYERFACTORY_H
#define HRA2016_PLAYERFACTORY_H

#include <regex>
#include <string>
#include <iostream>
#include <memory>

#include "Player.h"
#include "HumanPlayer.h"
#include "AIRandom.h"
#include "AIAlphaBeta.h"

namespace core
{
    class PlayerFactory
    {
    public:
        /**
         * Create a new human player with given name.
         * @param name Name of the player.
         * @return The player object.
         */
        std::shared_ptr<core::Player> createHuman(const std::string &name, std::size_t playerID);

        /**
         * Create a new AI player with given name, using given configuration string.
         * @param name Name of the player.
         * @param playerID Player ID of the new player.
         * @param typeName Type name of the requested AI.
         * @return The player object.
         */
        std::shared_ptr<core::Player> createAI(const std::string &name, std::size_t playerID,
                                               const std::string &typeName);

        /**
         * Create a new AI player with given name, using given configuration string.
         * @param name Name of the player.
         * @param config Configuration string for AI.
         * @return The player object.
         */
        std::shared_ptr<core::Player> unserializePlayer(const std::string &str, std::size_t playerID);

        /**
         * Get the Ai types.
         * @return vector containing Ai type names.
         */
        const std::vector<std::string> &getAiTypes() const;
    private:
        /**
          * Create a new AI player by className with given name and id
          * @param Class name
          * @param Player name
          * @param Player id
          * @param config Configuration string.
          * @return The player object.
          */
         std::shared_ptr<core::AIPlayer> createAiByClassName(const std::string & className, const std::string &name,
                                                             std::size_t playerID);
    protected:
    };
}

#endif //HRA2016_PLAYERFACTORY_H
