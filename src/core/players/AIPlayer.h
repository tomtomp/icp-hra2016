/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_AIPLAYER_H
#define HRA2016_AIPLAYER_H

#include "Player.h"
#include "../GameSimulation.h"

namespace core
{
    /**
     * Player controlled by AI.
     */
    class AIPlayer : public Player
    {
    public:
        /**
         * Create AI player with given name.
         */
        AIPlayer(const std::string &name, std::size_t playerID);

        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const override
        {
            std::stringstream ss;
            ss << getTypeName() << ":" << getName();
            return ss.str();
        }

        /// Name of this type
        static const std::string TYPE_NAME;
    private:
    protected:

        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override
        {
            // Not required.
        }

        /// Game simulator
        core::GameSimulation mGameSimulator;
    };
}

#endif //HRA2016_AIPLAYER_H
