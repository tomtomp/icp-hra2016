/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "HumanPlayer.h"

namespace core
{
    const std::string HumanPlayer::TYPE_NAME{"Human"};

    HumanPlayer::HumanPlayer(const std::string &name, std::size_t playerID) :
        Player(name, playerID, true)
    {

    }
}


