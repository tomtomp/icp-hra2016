/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "AIAlphaBeta.h"

namespace core
{
    const decltype(std::chrono::milliseconds()) AIAlphaBeta::MAX_TIME_MS = std::chrono::milliseconds(10000);
    const std::size_t AIAlphaBeta::MAX_DEPTH{5};
    const int AIAlphaBeta::START_ALPHA{-100000};
    const int AIAlphaBeta::START_BETA{100000};
    const std::string AIAlphaBeta::TYPE_NAME{"AIAlphaBeta"};

    AIAlphaBeta::AIAlphaBeta(const std::string &name, std::size_t playerID) :
        AIPlayer(name, playerID)
    {
        /*
        for (int jjj = 0; jjj < 12; ++jjj)
        {
            std::stringstream ss;
            for (int iii = 0; iii < 12; ++iii)
                ss << getWeight(iii, jjj, 12) << " ";
            util::Logger::logDebug(ss.str());
        }
        */
    }

    void printPF(const core::PlayField &pf)
    {
#ifndef NDEBUG
        std::size_t size = pf.getSize();
        util::Logger::logDebug("Starting pf printing.");
        for (std::size_t jjj = 0; jjj < size; ++jjj)
        {
            std::stringstream ss;
            for (std::size_t iii = 0; iii < size; ++iii)
            {
                char value = pf.getStoneOID(core::Coords(iii, jjj));
                if (value == 0)
                    ss << " ";
                else if (value == 1)
                    ss << "X";
                else
                    ss << "O";
                ss << " ";
            }
            util::Logger::logDebug(ss.str());
        }
        util::Logger::logDebug("Ending pf printing.");
#endif
    }

    Coords AIAlphaBeta::getMoveCoords(const PlayField &pf) const
    {
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;
        const auto start = system_clock::now();
        auto now = system_clock::now();

        char myPlayerID = static_cast<char>(getPlayerID());
        char currPlayerID = myPlayerID;
        PlayField currentPF = pf;
        ActionHolder myActionHolder;

        std::vector<TreeHelper> stack;

        core::Coords bestPlay;

        // We shouldn't throw any exceptions out...
        try
        {
            util::Logger::logDebug("##Starting AIAlphaBeta##");
            stack.push_back(TreeHelper(mGameSimulator.getAllPlays(currentPF, myPlayerID), true));

            // Checking the maximum run time.
            while (!stack.empty() && duration_cast<milliseconds>(now - start) < MAX_TIME_MS)
            {
                util::Logger::logDebug("Starting new iteration.");

                TreeHelper &currentTop = stack.back();
                if (currentTop.pruneAvailable() || !currentTop.moreActions() || currentTop.getDepth() == MAX_DEPTH)
                {
                    // We are done with current node.
                    util::Logger::logDebug("We are done with current node.");

                    // We are finished.
                    if (stack.size() == 1)
                    {
                        util::Logger::logDebug("We are finished.");
                        break;
                    }

                    TreeHelper lastNode = currentTop;
                    stack.pop_back();
                    TreeHelper &newTop = stack.back();

                    //if(currentTop.getNumberOfPlays() == 0 && currentTop.getDepth() != MAX_DEPTH)
                    if(currentTop.getNumberOfPlays() == 0)
                    {
                        util::Logger::logDebug("No more plays, need to check for victory.");
                        newTop.sendNewValue(getPlayFieldValue(currentPF, myPlayerID, true));
                    }
                    else if (currentTop.getDepth() == MAX_DEPTH)
                    {
                        util::Logger::logDebug("Maximum depth reached.");
                        /*
                        newTop.sendNewValue(getPlayFieldValue(currentPF, myPlayerID) +
                                            10 * mGameSimulator.getAllPlays(currentPF, myPlayerID).size() -
                                            10 * mGameSimulator.getAllPlays(currentPF, advancePlayer(myPlayerID)).size());
                        */
                        newTop.sendNewValue(getPlayFieldValue(currentPF, myPlayerID));
                    }
                    else
                    {
                        util::Logger::logDebug("Normal node.");
                        newTop.sendNewValue(lastNode.getValue());
                    }

                    if (!mGameSimulator.undoPlay(currentPF, myActionHolder))
                        throw std::runtime_error("Unable to undo");

                    currPlayerID = advancePlayer(currPlayerID);
                }
                else
                {
                    // We should go to a child node.

                    util::Logger::logDebug("Going to a child node.");

                    std::size_t currentDepth = currentTop.getDepth();

                    const core::MoveAction &currMoveAction = currentTop.getNextMoveAction();

                    printPF(currentPF);
                    if (!mGameSimulator.makePlay(currMoveAction, currentPF, myActionHolder))
                        throw std::runtime_error(std::string("Unable to make the play : ")+ currMoveAction.serialize());
                    printPF(currentPF);

                    currPlayerID = advancePlayer(currPlayerID);

                    // If we are one level above the maximum depth, then there is no need to generate possible actions.
                    if (currentDepth == MAX_DEPTH - 1)
                    {
                        //util::Logger::logDebug("We are one step from maximum depth, no need to generate plays.");
                        stack.push_back(TreeHelper(mGameSimulator.getAllPlays(currentPF, currPlayerID),
                                                   !currentTop.maximize(),
                                                   currentDepth + 1,
                                                   currentTop.alpha(), currentTop.beta())
                        );
                    }
                    else
                    {
                        util::Logger::logDebug("Generating all plays.");
                        stack.push_back(TreeHelper(mGameSimulator.getAllPlays(currentPF, currPlayerID),
                                                   !currentTop.maximize(),
                                                   currentDepth + 1,
                                                   currentTop.alpha(), currentTop.beta())
                        );
                    }
                }

                now = system_clock::now();
            }
            if(duration_cast<milliseconds>(now - start) >= MAX_TIME_MS)
                util::Logger::logInfo("Time ran out.");
        } catch(std::runtime_error &e)
        {
            util::Logger::logDebug(std::string("Exception occurred in AlphaBeta AI : ") + e.what());
        } catch(...)
        {
            util::Logger::logDebug("Unknown exception occurred in AlphaBeta AI!");
        }

        if (stack.size() >= 1)
        {
            // The node on the first index is the root node.
            TreeHelper &rootNode = stack.front();

            util::Logger::logDebug("Found the root node.");

            if (rootNode.canGetBestAction())
            {
                MoveAction bestMoveAction = rootNode.getBestAction();
                bestPlay = bestMoveAction.getPrimaryStone();

#ifndef NDEBUG
                util::Logger::logDebug(std::string("Got the best action from the root node : ") +
                                       bestMoveAction.serialize());
                printPF(pf);
#endif

                // If we found a valid move, then use it.
                if (mGameSimulator.canMakePlay(bestMoveAction, pf))
                {
                    util::Logger::logDebug("##We found a viable move##");
                    return bestPlay;
                }
            }
        }

        // Else fall back to the random AI.
        util::Logger::logError("##We did not find a viable move, falling back to random##");
        srand(time(0));
        std::vector<core::MoveAction> moves = mGameSimulator.getAllPlays(pf, myPlayerID);
        size_t randNum = rand() % moves.size();
        return moves[randNum].getPrimaryStone();
    }

    int AIAlphaBeta::getPlayFieldValue(const core::PlayField &pf, char playerID, bool noMoreMoves)
    {
        if (noMoreMoves)
        {
            const std::vector<std::size_t> &scores = pf.getScores();
            if (scores[playerID] > scores[advancePlayer(playerID)])
                return START_BETA;
            else
                return START_ALPHA;
        }

        int score{0};
        std::size_t size = pf.getSize();

        for (std::size_t jjj = 0; jjj < size; ++jjj)
        {
            for (std::size_t iii = 0; iii < size; ++iii)
            {
                char currPlayerID = pf.getStoneOID(iii, jjj);
                int playerWeight{currPlayerID == playerID ? 1 : -1};
                int positionWeight = getWeight(iii, jjj, size);

                score += playerWeight * positionWeight;
            }
        }

        return score;
    }

    int AIAlphaBeta::getWeight(std::size_t x, std::size_t y, std::size_t size)
    {
        assert(size > 2);
        std::size_t maxCoord = size - 1;

        // The "magical constants" in this code were taken from :
        // https://reversiworld.wordpress.com/category/weighted-square-value/
        if (x > 2 && x < maxCoord - 2 && y > 2 && y < maxCoord - 2)
        {
            return 6;
        }
        else if (x >= 2 && x <= maxCoord - 2 && y >= 2 && y <= maxCoord - 2)
        {
            if ((x > 2 && x < maxCoord - 2) || (y > 2 && y < maxCoord - 2))
                return 6;
            else
                return 15;
        }
        else if (x >= 1 && x <= maxCoord - 1 && y >= 1 && y <= maxCoord - 1)
        {
            if ((x > 1 && x < maxCoord - 1) || (y > 1 && y < maxCoord - 1))
                return -5;
            else
                return -20;
        }
        else
        {
            if ((x > 0 && x < maxCoord) || (y > 0 && y < maxCoord))
            {
                if ((x > 2 && x < maxCoord - 2) || (y > 2 && y < maxCoord - 2))
                    return 5;
                else if ((x > 1 && x < maxCoord - 1) || (y > 1 && y < maxCoord - 1))
                    return 20;
                else
                    return -20;
            }
            else
                return 60;
        }
    }
}
