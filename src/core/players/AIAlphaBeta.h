/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_AIALPHABETA_H
#define HRA2016_AIALPHABETA_H

#include <chrono>

#include "AIPlayer.h"
#include "../GameSimulation.h"

namespace core
{
    class AIAlphaBeta : public core::AIPlayer
    {
    public:
        /**
         * Create AI player with given name.
         * @param name Name of this player.
         * @param playerID ID of this player.
         */
        AIAlphaBeta(const std::string &name, std::size_t playerID);

        /**
         * Get the players move action. Gets all possible moves and choose the first one
         * @param pf Current state of the playing field.
         * @return Coords of the move
         */
        virtual Coords getMoveCoords(const core::PlayField &pf) const override;

        /**
         * Get name of this player type.
         * @return Returns the name as string.
         */
        virtual const std::string &getTypeName() const override
        {
            return TYPE_NAME;
        }

        /// Maximum time in milliseconds, the "getMoveCoords" should run.
        static const decltype(std::chrono::milliseconds()) MAX_TIME_MS;

        /// The depth, this algorithm will search.
        static const std::size_t MAX_DEPTH;

        /// The starting alpha value, should be the minimum.
        static const int START_ALPHA;

        /// The starting beta value, should be the maximum.
        static const int START_BETA;

        /// Name of this type
        static const std::string TYPE_NAME;
    private:
        /// Helper class for AlphaBeta Pruning algorithm
        class TreeHelper
        {
        public:
            TreeHelper(std::vector<core::MoveAction> &&possibleActions, bool maximize, std::size_t depth = 1,
                       int alpha = START_ALPHA, int beta = START_BETA) :
                mPossibleActions{possibleActions},
                mDepth{depth},
                mMaximize(maximize),
                mAlpha{alpha},
                mBeta{beta}
            {
                mValue = mMaximize ? mAlpha : mBeta;
#ifndef NDEBUG
                util::Logger::logDebug("Printing possible actions : ");
                for (const core::MoveAction &ma : mPossibleActions)
                    util::Logger::logDebug(ma.serialize());
#endif
            };

            /// Alpha getter.
            int alpha() const { return mAlpha; }
            /// Beta getter.
            int beta() const { return mBeta; }

            /// Are there any more possible actions?
            bool moreActions() const { return mLastUsed < mPossibleActions.size(); }

            /// Get the next move action and increment the counter.
            const core::MoveAction &getNextMoveAction()
            {
                assert(mLastUsed < mPossibleActions.size());
                return mPossibleActions[mLastUsed++];
            }

            /// Getting a new value from lower node.
            void sendNewValue(int value)
            {
                if (mMaximize)
                {
                    if (value > mAlpha)
                    {
                        mBestUsed = mLastUsed - 1;
                        mAlpha = value;
                        mValue = value;
                    }
                }
                else
                {
                    if (value < mBeta)
                    {
                        mBestUsed = mLastUsed - 1;
                        mBeta = value;
                        mValue = value;
                    }
                }
            }

            /// Get the value of this node.
            int getValue() const { return mValue; }

            /// Should we prune?
            bool pruneAvailable() const { return mAlpha >= mBeta; }

            /// Check, if it is possible to get the best action.
            bool canGetBestAction() const
            {
                return (
                    mPossibleActions.size() != 0 &&
                    mBestUsed < mPossibleActions.size()
                );
            }

            /// Get the best action so far.
            const core::MoveAction &getBestAction() const
            {
                assert(mBestUsed < mPossibleActions.size());
                return mPossibleActions[mBestUsed];
            }

            /// Get the type of the node, true == maximizer.
            bool maximize() const { return mMaximize; }

            /// Get the depth of this node.
            std::size_t getDepth() const { return mDepth; }

            /// Get the size of the possible plays vector.
            std::size_t getNumberOfPlays() const { return mPossibleActions.size(); }
        private:
            /// Vector of possible actions.
            std::vector<core::MoveAction> mPossibleActions;
            /// Index of the last used action + 1.
            std::size_t mLastUsed{0};
            /// Index of the best action used so far.
            std::size_t mBestUsed{0};
            /// Depth of this node.
            std::size_t mDepth{2};
            /// Value of this node.
            int mValue;
            /// Is this a maximizer or minizer node?
            bool mMaximize{false};
            /// Current alpha value.
            int mAlpha{START_ALPHA};
            /// Current beta value.
            int mBeta{START_BETA};
        protected:
        };

        /**
         * Get the value of given playing field. Used in AlphaBeta.
         * @param pf Playing field to score.
         * @param playerID From which players perspective.
         * @param noMoreMoves If set to true, checks for victory only.
         * @return The value of the playing field score from the players perspective.
         */
        static int getPlayFieldValue(const core::PlayField &pf, char playerID, bool noMoreMoves = false);

        /**
         * Advance the player ID, works only for 2 players.
         * @param playerID ID of the current player.
         * @param playerNum Number of players.
         * @return The new player ID.
         */
        static char advancePlayer(char playerID)
        {
            return playerID % 2 + 1;
        }

        /**
         * Weight the position of stone.
         * @param x The X coordinate.
         * @param y The Y coordinate.
         * @param size The size of the playing field.
         * @return The weight of the position.
         */
        static int getWeight(std::size_t x, std::size_t y, std::size_t size);

        /// Game simulator used by this AI.
        core::GameSimulation mGameSimulator;
    protected:
    };
}

#endif //HRA2016_AIALPHABETA_H
