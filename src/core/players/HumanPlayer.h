/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_HUMANPLAYER_H
#define HRA2016_HUMANPLAYER_H

#include "../../util/Logger.h"
#include "Player.h"

#include <string>

namespace core
{
    /**
     * Player controlled by human.
     */
    class HumanPlayer : public Player
    {
    public:
        /**
         * Create human player with given name.
         */
        HumanPlayer(const std::string &name, std::size_t playerID);

        /**
         * Getting a move from human player is not allowed.
         */
        virtual Coords getMoveCoords(const core::PlayField &pf) const override
        {
            util::Logger::logDebug("Human get move action called!");
            throw std::runtime_error("Human get move action called!");
        }

        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const override
        {
            std::stringstream ss;
            ss << TYPE_NAME << ":" << getName();
            return ss.str();
        }

        virtual const std::string &getTypeName() const override
        {
            return TYPE_NAME;
        }

        /// Name of this type
        static const std::string TYPE_NAME;
    private:
        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override
        {
            // To do.
        }
    protected:
    };
}



#endif //HRA2016_HUMANPLAYER_H
