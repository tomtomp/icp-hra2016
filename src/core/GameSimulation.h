/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAMESIMULATION_H
#define HRA2016_GAMESIMULATION_H

#include "ActionHolder.h"
#include "PlayField.h"

namespace core
{
    class GameSimulation
    {
    public:
        /**
         * Make a play using given information. Uses the action on the playing field and adds the action to the vector.
         * @param action Action to take.
         * @param pf Playing field to play on.
         * @param actionVector Vector of actions to add action to.
         * @return Returns true, if everything went ok, else returns false.
         */
        bool makePlay(const core::MoveAction &action, core::PlayField &pf,
                      core::ActionHolder &ah) const;

        /**
         * Undo play.
         * @param pf Playing field to undo the action on.
         * @param ah Action holder containing the action.
         * @return Returns false, if there are no more actions to undo.
         */
        bool undoPlay(core::PlayField &pf, core::ActionHolder &ah) const;

        /**
         * Redo play.
         * @param pf Playing field to redo the action on.
         * @param ah Action holder containing the action.
         * @return Returns false, if there are no more actions to redo.
         */
        bool redoPlay(core::PlayField &pf, core::ActionHolder &ah) const;

        /**
         * Is the action a valid one?
         * @param action Action to try.
         * @param pf Current playing field.
         * @return Returns true, if the action can be made.
         */
        bool canMakePlay(const core::MoveAction &action, const core::PlayField &pf) const;

        /**
         * Find all of the available plays.
         * @param pf Playing field to search on.
         * @param playerID ID of the player to search plays for.
         * @return Vector containing all of the plays.
         */
        std::vector<core::MoveAction> getAllPlays(const core::PlayField &pf, char playerID) const;

        /**
         * Are there any more plays for given player?
         * @param playerID ID of the player.
         * @param pf Playing field.
         * @return Returns true, if there are no more plays available.
         */
        bool gameFinished(const core::PlayField pf, char playerID) const
        {
            return getAllPlays(pf, playerID).empty();
        }

        /**
         * Fill the secondary stones of given action, if there are any secondary stones, returns the original action.
         * @param action Action to fill.
         * @param pf Current playing field.
         */
        void fillAction(core::MoveAction &action, const core::PlayField &pf) const;

        /**
         * Get the first stone of given color in the direction from given coordinates.
         * @param playerID PlayerID of the searched stones.
         * @param direction Direction to search in - for example {1, 0} is to the right.
         * @param start Coordinates where to start from, this stone will be skipped.
         * @param pf Playing field to search on.
         * @return Returns coordinates of the first stone of given playerID in given direction without interruptions.
         *           If no stones were found, returns the starting coordinates.
         */
        core::Coords findDirectionStone(char playerID, const core::Coords &direction,
                                        const core::Coords &start, const core::PlayField &pf) const;
    private:
        static const std::vector<core::Coords> DIRECTIONS;
    protected:
    };
}


#endif //HRA2016_GAMESIMULATION_H
