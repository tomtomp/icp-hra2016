/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_PLAYFIELD_H
#define HRA2016_PLAYFIELD_H

#include <string>
#include <vector>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <algorithm>

#include "../util/Logger.h"
#include "interfaces/Serializable.h"

#include "Types.h"

namespace core
{
    /**
     * The Reversi playing field.
     */
    class PlayField : public core::Serializable
    {
    public:
        /**
         * Create the playing field with given size.
         * @param size The size of the playing field.
         */
        PlayField(std::size_t size = 0);

        /**
         * Create the playing field from the given string representation.
         * @param str String representation of the playing field.
         */
        PlayField(std::string str);

        /**
         * Set the size of the PlayField to the given value.
         *  !! RESETS THE PLAYING FIELD!!
         * @param size The new size of the playing field.
         */
        void setSize(std::size_t size);

        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const override;

        /**
         * Get player ID of the stone on the given coordinates.
         * @param c Coordinates of the requested players stone id.
         * @return Player ID of the stone on the given coordinates.
         */
        char getStoneOID(const core::Coords &c) const
        {
            return mField[getIndex(c)];
        }

        /**
         * Adapter for the other getStoneOID function.
         * @param x The X coordinate.
         * @param y The Y coordinate.
         * @return The value on the given coordinates.
         */
        char getStoneOID(std::size_t x, std::size_t y) const
        {
            return getStoneOID(core::Coords(x, y));
        }

        /**
         * Set the owner ID of stone on given coordinates.
         * @param c Coordinates of the stone.
         * @param id ID of the new owner.
         */
        void setStoneOID(const core::Coords &c, char id)
        {
            if (!isIdOk(id))
            {
                util::Logger::logDebug("Id is out of range.");
                throw std::runtime_error("Id is out of range.");
            }

            mField[getIndex(c)] = id;
        }

        /**
         * Get scores for each player ID.
         * @return List containing number of fields with each of the IDs. Number of empty fields are on index = 0.
         */
        std::vector<std::size_t> getScores() const
        {
            std::vector<std::size_t> result(MAX_PLAYERS + 1, 0);

            for (char c : mField)
                result[c]++;

            return result;
        }

        /**
         * Is the given field empty?
         * @param c Coordinates to test on.
         * @return Returns true, if the field is empty.
         */
        bool isFieldEmpty(core::Coords c) const
        {
            return getStoneOID(c) == 0;
        }

        /**
         * Get the size of the playing field.
         * @return The size of the playing field.
         */
        std::size_t getSize() const
        {
            return mSize;
        }

        /**
         * Get the playing field.
         * @return The playing field.
         */
        const std::vector<char> &getField() const
        {
            return mField;
        }

        /**
         * Get the playing field.
         * @return The playing field.
         */
        std::vector<char> &getField()
        {
            return mField;
        }

        /**
         * Flip stones between 2 given stones to the player ID.
         * @param c1 The first coordinate.
         * @param c2 The second coordinate.
         * @param playerID The new player ID.
         */
        void flipBetween(const core::Coords &c1, const core::Coords &c2, char playerID);

        /**
         * Determine, if the coordinate is in the playing field.
         * @param c Coordinates to test.
         * @param pf Playing field to test on.
         * @return Returns true, of the coordinates are withing playing field.
         */
        bool isInPlayingField(const core::Coords &c) const
        {
            std::size_t fieldSize = getSize();
            return c.first >= 0 && c.first < static_cast<long long>(fieldSize) &&
                   c.second >= 0 && c.second < static_cast<long long>(fieldSize);
        }

        /**
         * Check, if the given ID is valid.
         * @param playerID ID to check.
         * @return Returns true, if the ID is valid, else returns false.
         */
        bool isIdOk(char playerID);

        /// Maximum number of players supported by this playing field.
        static constexpr std::size_t MAX_PLAYERS{2};

        /// ID returned, if there is no stone.
        static constexpr char EMPTY_ID{0};
    private:
        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override;

        /**
         * Get 1D index.
         * @param c Coords to calculate index for.
         * @return The index.
         */
        std::size_t getIndex(const core::Coords &c) const
        {
            auto index = c.second * mSize + c.first;

            if (index >= mSize * mSize)
            {
                util::Logger::logDebug("Stone coordinates out of bounds!");
                throw std::runtime_error("Stone coordinates out of bounds!");
            }

            return index;
        }

        /// The playing field.
        std::vector<char> mField;

        /// Size of the playing field.
        std::size_t mSize{0};
    protected:
    };
}

#endif //HRA2016_PLAYFIELD_H
