/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "ActionHolder.h"

namespace core
{
    core::ActionHolder::ActionHolder() :
        mBeforeFirst(true),
        mCurrTop(0)
    {

    }

    ActionHolder::ActionHolder(const std::string &str) :
        ActionHolder()
    {
        unserialize(str);
    }

    core::MoveAction ActionHolder::getCurrTop() const
    {
        if (mActionStack.empty())
        {
            util::Logger::logDebug("Cannot access empty action holder!");
            throw std::runtime_error("Cannot access empty action holder!");
        }

        if (mCurrTop < 0 || mCurrTop >= mActionStack.size() || mBeforeFirst)
        {
            util::Logger::logDebug("Accessing action holder out of bounds!");
            throw std::runtime_error("Accessing action holder out of bounds!");
        }

        return mActionStack[mCurrTop];
    }

    std::string ActionHolder::serialize() const
    {
        std::stringstream ss;

        if (mBeforeFirst || mActionStack.empty())
            return "";
        else
        {
            for (auto it = mActionStack.cbegin();
                 it <= mActionStack.cbegin() + mCurrTop; ++it)
                ss << it->serialize() << "\n";
        }

        /*
        for (const core::MoveAction &action : mActionStack)
            ss << action.serialize() << "\n";
        */

        return ss.str();
    }

    void ActionHolder::unserialize(const std::string &str)
    {
        std::stringstream ss(str);
        std::string actionStr;

        while (std::getline(ss, actionStr))
            push(core::MoveAction(actionStr));

        resetCurrTop();
    }

    void ActionHolder::push(const core::MoveAction &action)
    {
        assert(!mBeforeFirst || mCurrTop == 0);

        mActionStack.push_back(action);
    }

    void ActionHolder::pushAfterCurrTop(const core::MoveAction &action)
    {
        assert(!mBeforeFirst || mCurrTop == 0);

        if (mActionStack.empty())
            mActionStack.push_back(action);
        else
        {
            if (mBeforeFirst)
                mActionStack.clear();
            else
                mActionStack.erase(mActionStack.begin() + mCurrTop + 1, mActionStack.end());
            mActionStack.push_back(action);
        }
        if (!succ())
        {
            util::Logger::logDebug("Failed to succ after pushing!");
            throw std::runtime_error("Failed to succ after pushing!");
        }
    }

    bool ActionHolder::prec()
    {
        assert(!mBeforeFirst || mCurrTop == 0);

        if (mActionStack.empty() || mCurrTop == 0)
        {
            mCurrTop = 0;
            if (mBeforeFirst)
                return false;
            else
            {
                mBeforeFirst = true;
                return true;
            }
        }
        else
        {
            mCurrTop--;
            return true;
        }


    }

    bool ActionHolder::succ()
    {
        assert(!mBeforeFirst || mCurrTop == 0);

        if (mActionStack.empty())
            return false;
        else if (mCurrTop == 0 && mBeforeFirst)
        {
            mCurrTop = 0;
            mBeforeFirst = false;
        }
        else if (mCurrTop == mActionStack.size() - 1)
            return false;
        else
            mCurrTop++;
        return true;
    }
}
