/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GAME_H
#define HRA2016_GAME_H

#include <cstddef>

#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <vector>

#include <atomic>

#include "../util/ThreadPrinter.h"
#include "../util/Logger.h"

#include "interfaces/Serializable.h"
#include "MessageBox.h"
#include "GameSimulation.h"
#include "ActionHolder.h"
#include "PlayerHolder.h"
#include "players/PlayerFactory.h"
#include "players/AIPlayer.h"
#include "messages/MessageFactory.h"
#include "messages/CoordsMessage.h"

namespace core
{
    enum class GameState
    {
        READY = 0,
        GETTING_INPUT,
        SIMULATING,
        ENDED,
        UNDO,
        REDO,
        UNK
    };

    /**
     * The main game class, contains all of the game logic.
     */
    class Game : public core::Serializable, public core::MessageBox
    {
    public:
        Game(const Game&) = delete;
        Game(Game&&) = delete;

        /**
         * Create new game with the given board size.
         * @param msgBox Message box of the interface.
         */
        Game(core::MessageBox *msgBox);

        /**
         * Create new game with the given board size.
         * @param msgBox Message box of the interface.
         * @param savedGame Content of the saved game file.
         */
        Game(core::MessageBox *msgBox, const std::string &savedGame);

        /**
         * Is the thread currently running?
         * @return Returns true, if the game thread is running.
         */
        bool isRunning() const
        {
            return mThread.joinable();
        }

        /**
         * Get scores for each player ID.
         * @return List containing number of fields with each of the IDs. Number of empty fields are on index = 0.
         */
        std::vector<std::size_t> getScores() const
        {
            return mPlayField.getScores();
        }

        /**
         * Start the game thread.
         */
        void start();

        /**
         * Stop the game thread, when the game is ready.
         */
        void stop();

        /**
         * Get the ID of the currently playing player.
         * @return Returns the ID of the currently playing player.
         */
        std::size_t getCurrentPlayerID() const
        {
            return mPlayers.getCurrPlayerID();
        }

        /**
         * Get player by ID.
         * @param id ID of the requested player.
         * @return Returns a constant reference to requested player.
         */
        const std::shared_ptr<const core::Player> getPlayer(std::size_t id) const
        {
            return mPlayers.getPlayer(id);
        }

        /**
         * Create a string from this object.
         * Format :
         *   Player1_str\n
         *   Player2_str\n
         *   \n
         *   PlayField_str\n
         *   \n
         *   CommandBuffer_str\n
         * @return String representation of this object.
         */
        virtual std::string serialize() const override final;

        /**
         * Get the available board sizes.
         * @return The board sizes.
         */
        const std::vector<std::size_t> &getBoardSizes() const
        {
            return BOARD_SIZES;
        }

        /**
         * Choose the board size on the given index.
         * @param choice Index of the selected board size.
         */
        void chooseBoardSize(std::size_t choice);

        /**
         * Get the available player types.
         * @return List of player types to choose from.
         */
        const std::vector<std::string> &getPlayerTypes() const
        {
            return PLAYER_TYPES;
        }

        /**
         * Create a player with given choices.
         * @param name Name of the new player.
         * @param playerType Choice of the player type.
         * @param aiType Choice of the AI type.
         */
        void createPlayer(const std::string &name, std::size_t playerType, std::size_t aiType = 0);

        /**
         * Get the list of available AI types.
         * @return The list of available AI types.
         */
        const std::vector<std::string> &getAiTypes() const
        {
            return mPlayers.getAiTypes();
        }

        /**
         * Get the current state of the game engine.
         * @return The current state of the game.
         */
        core::GameState getGameState() const
        {
            return mGameState;
        }
        
        const core::PlayField &getPlayField() const
        {
            return mPlayField;
        }

        /**
         * Get the list of possible actions. Only available after the game sends the READY_MSG.
         * @return The vector of possible moves.
         */
        std::vector<core::MoveAction> getPossibleActions() const
        {
            return mPossibleActions;
        }

        /**
         * Destroy this object. Automatically stops the thread.
         */
        virtual ~Game();

        /**
         * Validate given name.
         * @param name Name to validate.
         * @return Returns true, if the name is valid.
         */
        static bool validateName(const std::string &name);

        /// Number of players.
        static const std::size_t PLAYER_NUM{2};
    private:
        /**
         * Thread main function.
         */
        void threadMain();

        /**
         * Message processing when in ready state.
         * @return Returns the next state.
         */
        core::GameState processStateReady();

        /**
         * Message processing, when the game has ended.
         * @return Returns the next state.
         */
        core::GameState processStateEnded();

        /**
         * Message processing when in getting_input state.
         * @param c Output variable, if the next state is simulating it will be filled with correct value.
         * @return Returns the next state.
         */
        core::GameState processStateGettingInput(core::Coords &c);

        /**
         * Message processing when in simulating state.
         * @param c Coords to be used in current turn.
         * @return Returns the next state.
         */
        core::GameState processStateSimulating(const core::Coords &c);

        /**
         * Message processing when in undo/redo state.
         * @param doRedo If set to true, redo will be done, else undo will be done.
         * @return Returns the next state.
         */
        core::GameState processStateUndoRedo(bool doRedo);

        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override final;

        /**
         * Reset the game ended flag.
         */
        void resetGameShouldEnd()
        {
            mGameShouldEnd = false;
        }

        /**
         * Set the game ended flag.
         */
        void setGameShouldEnd()
        {
            mGameShouldEnd = true;
        }

        /// Should the game end?
        volatile std::atomic_bool mGameShouldEnd;

        /// Current state of game FSM
        volatile std::atomic<core::GameState> mGameState;

        /// User interface base pointer, used for sending messages.
        core::MessageBox *mMsgBox;

        /// Thread, in which this object runs.
        std::thread mThread;

        /// Size of the board.
        std::size_t mBoardSize{0};

        /// Currently playing player ID.
        std::size_t mCurrentPlayerID{0};

        /// Stack containing the actions.
        core::ActionHolder mActionHolder;

        /// Playing field.
        core::PlayField mPlayField;

        /// Vector of players.
        core::PlayerHolder mPlayers;

        /// List of available AI files.
        std::vector<std::string> mAiList;

        /// Number of players already added.
        std::size_t mActivePlayers{0};

        /// Message factory.
        core::MessageFactory mMsgFactory;

        /// Game simulator.
        core::GameSimulation mGameSimulator;

        /// Vector of possible actions, available when the game is ready.
        std::vector<core::MoveAction> mPossibleActions;

        /// Selection of board sizes.
        static const std::vector<std::size_t> BOARD_SIZES;

        /// Selection of player types.
        static const std::vector<std::string> PLAYER_TYPES;
    protected:
    };
}


#endif //HRA2016_GAME_H
