/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_ACTIONHOLDER_H
#define HRA2016_ACTIONHOLDER_H

#include <stack>

#include <cassert>

#include "interfaces/Serializable.h"
#include "MoveAction.h"

namespace core
{
    class ActionHolder : public core::Serializable
    {
    public:
        /**
         * Create an empty action holder.
         */
        ActionHolder();

        /**
         * Construct action holder from string.
         * @param str String to construct from.
         */
        ActionHolder(const std::string &str);

        /**
         * Is the action holder empty?
         * @return Returns true, if the action holder is empty.
         */
        bool empty() const
        {
            return mActionStack.empty();
        }

        /**
         * Add given action to the top of the stack.
         * @param action Action to add.
         */
        void push(const core::MoveAction &action);

        /**
         * Add given action after the current top pointer and set the pointer to the new action.
         * @param action Action to add.
         */
        void pushAfterCurrTop(const core::MoveAction &action);

        /**
         * If possible, move the current top one down. if not, return false.
         * @return Returns true, if the action succeeded.
         */
        bool prec();

        /**
         * If possible, move the current top one up. if not, return false.
         * @return Returns true, if the action succeeded.
         */
        bool succ();

        /**
         * Reset the current top index to the actual top.
         */
        void resetCurrTop()
        {
            if (mActionStack.empty())
            {
                mCurrTop = 0;
                mBeforeFirst = true;
            }
            else
            {
                mCurrTop = mActionStack.size() - 1;
                mBeforeFirst = false;
            }
        }

        /**
         * Get the currently top-most action.
         * @return Action which is currently selected as the top one.
         */
        core::MoveAction getCurrTop() const;

        /**
         * Is the current top gettable?
         * @return If the current top is gettable, returns true..
         */
        bool canGetCurrTop() const
        {
            return !((mActionStack.empty()) || (mCurrTop < 0 || mCurrTop >= mActionStack.size() || mBeforeFirst));
        }

        /**
         * Create a string from this object.
         * @return String representation of this object.
         */
        virtual std::string serialize() const override;
    private:
        /**
         * Create an object from this string.
         * @param str String representation of this object.
         */
        virtual void unserialize(const std::string &str) override;

        /// Is the current top before the first element?
        bool mBeforeFirst;

        /// The current top index.
        std::size_t mCurrTop;

        /// Stack containing all of the actions.
        std::deque<core::MoveAction> mActionStack;
    protected:
    };
}

#endif //HRA2016_ACTIONHOLDER_H
