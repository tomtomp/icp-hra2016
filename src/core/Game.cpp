/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Game.h"

namespace core
{
    const std::vector<std::size_t> Game::BOARD_SIZES{4, 6, 8, 10, 12};
    const std::vector<std::string> Game::PLAYER_TYPES{"Human", "AI"};

    Game::Game(core::MessageBox *msgBox) :
        mGameShouldEnd{true},
        mGameState{core::GameState::ENDED},
        mMsgBox(msgBox),
        mPlayers{2}
    {
    }

    Game::Game(core::MessageBox *msgBox, const std::string &savedGame) :
        Game(msgBox)
    {
        unserialize(savedGame);
    }

    void Game::start()
    {
        util::Logger::logDebug("Starting game thread.");
        if (! mThread.joinable())
            mThread = std::thread(&Game::threadMain, this);

        resetGameShouldEnd();
        mGameState = core::GameState::READY;
    }

    void Game::threadMain()
    {
        /**
         * Game state FSM :
         *             >NEXT_MSG  >PUT_MSG | AI player plays
         *          ready => getting_input => simulating => ready
         *                        <REQ_MOVE_MSG
         *
         *                        UNDO|REDO
         */
        mGameState = core::GameState::READY;

        core::GameState current = mGameState;
        core::GameState next = current;
        core::Coords coords{-1, -1};

        while (!mGameShouldEnd)
        {
            switch(current)
            {
                case core::GameState::READY:
                    next = processStateReady();
                    break;
                case core::GameState::GETTING_INPUT:
                    next = processStateGettingInput(coords);
                    break;
                case core::GameState::SIMULATING:
                    next = processStateSimulating(coords);
                    break;
                case core::GameState::ENDED:
                    next = processStateEnded();
                    break;
                case core::GameState::UNDO:
                    next = processStateUndoRedo(false);
                    break;
                case core::GameState::REDO:
                    next = processStateUndoRedo(true);
                    break;
                default:
                    util::Logger::logDebug("Game engine in unknown state, returning to ready...");
                    next = core::GameState::READY;
                    break;
            }

            current = next;
            mGameState = next;
        }

        mGameState = core::GameState::ENDED;
        mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::ENDED_MSG));
    }

    core::GameState Game::processStateEnded()
    {
        std::shared_ptr<const core::Message> msg{nullptr};
        core::MessageT type = core::MessageT::UNK_MSG;

        util::Logger::logDebug("Sending interface ENDED_MSG.");
        mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::ENDED_MSG));

        waitForMessage();
        msg = receiveMessage();
        type = msg->getType();

        switch (type)
        {
            case core::MessageT::END_MSG:
                util::Logger::logDebug(std::string("Game going from ENDED to actually ending"));
                mGameShouldEnd = true;
                return core::GameState::ENDED;
            case core::MessageT::UNDO_MSG:
                util::Logger::logDebug(std::string("Game going from ENDED to UNDO"));
                return core::GameState::UNDO;
            case core::MessageT::REDO_MSG:
                util::Logger::logDebug(std::string("Game going from ENDED to REDO"));
                return core::GameState::REDO;
            default:
                util::Logger::logDebug(std::string("Game received wrong message in ENDED state! : ") +
                                       std::to_string(static_cast<int>(type)));
                mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::UNK_MSG));
                return core::GameState::ENDED;
        }
    }

    core::GameState Game::processStateReady()
    {
        std::shared_ptr<const core::Message> msg{nullptr};
        core::MessageT type = core::MessageT::UNK_MSG;

        mPossibleActions = mGameSimulator.getAllPlays(mPlayField, mPlayers.getCurrPlayerID());

        util::Logger::logDebug("Sending interface READY_MSG.");
        mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::READY_MSG));

        waitForMessage();
        msg = receiveMessage();
        type = msg->getType();

        switch (type)
        {
            case core::MessageT::NEXT_MSG:
                //if (mGameSimulator.gameFinished(mPlayField, mPlayers.getCurrPlayerID()))
                if (mPossibleActions.empty())
                {
                    util::Logger::logDebug(std::string("Game going from READY to ENDED"));
                    mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::ENDED_MSG));
                    return core::GameState::ENDED;
                }
                else
                {
                    util::Logger::logDebug(std::string("Game going from READY to GETTING_INPUT"));
                    return core::GameState::GETTING_INPUT;
                }
            case core::MessageT::END_MSG:
                util::Logger::logDebug(std::string("Game going from READY to ENDED"));
                mGameShouldEnd = true;
                return core::GameState::ENDED;
            case core::MessageT::UNDO_MSG:
                util::Logger::logDebug(std::string("Game going from READY to UNDO"));
                return core::GameState::UNDO;
            case core::MessageT::REDO_MSG:
                util::Logger::logDebug(std::string("Game going from READY to REDO"));
                return core::GameState::REDO;
            default:
                util::Logger::logDebug(std::string("Game received wrong message in READY state! : ") +
                                       std::to_string(static_cast<int>(type)));
                mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::UNK_MSG));
                return core::GameState::READY;
        }

        return core::GameState::UNK;
    }

    core::GameState Game::processStateGettingInput(core::Coords &c)
    {

        auto player = mPlayers.getCurrentPlayer();
        core::Coords moveCoords{-1, -1};

        if (player->getRequiresInput())
        {
            // Player requires input from interface.

            std::shared_ptr<const core::Message> msg{nullptr};
            core::MessageT type = core::MessageT::UNK_MSG;

            util::Logger::logDebug("Sending interface a message, requesting user input.");
            mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::REQ_MOVE_MSG));

            util::Logger::logDebug("Waiting for interface reply.");
            waitForMessage();

            util::Logger::logDebug("Received message from interface, expecting a PUT_MSG.");
            msg = receiveMessage();
            type = msg->getType();

            switch (type)
            {
                case core::MessageT::PUT_MSG:
                    util::Logger::logDebug(std::string("Game received PUT_MSG."));
                    moveCoords = core::CoordsMessage::getCoordsFromMessage(msg);
                    break;
                case core::MessageT::END_MSG:
                    util::Logger::logDebug(std::string("Game going from GETTING_INPUT to ENDED"));
                    mGameShouldEnd = true;
                    return core::GameState::ENDED;
                case core::MessageT::UNDO_MSG:
                    util::Logger::logDebug(std::string("Game going from GETTING_INPUT to UNDO"));
                    return core::GameState::UNDO;
                case core::MessageT::REDO_MSG:
                    util::Logger::logDebug(std::string("Game going from GETTING_INPUT to REDO"));
                    return core::GameState::REDO;
                default:
                    util::Logger::logDebug(std::string("Game received wrong message in GETTING_INPUT state! : ") +
                                           core::Message::getMessageTypeStr(type));
                    mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::UNK_MSG));
                    return core::GameState::GETTING_INPUT;
            }
        }
        else
        {
            // Player is able to generate input.
            moveCoords = player->getMoveCoords(mPlayField);
        }

        c = moveCoords;

        return core::GameState::SIMULATING;
    }

    core::GameState Game::processStateSimulating(const core::Coords &c)
    {
        core::MoveAction action(c, mPlayers.getCurrPlayerID());

        if (!mGameSimulator.makePlay(action, mPlayField, mActionHolder))
        {
            if (!mPlayers.getCurrentPlayer()->getRequiresInput())
                util::Logger::logDebug("Coordinates received from AI are not valid!");
            else
            {
                util::Logger::logDebug("Coordinates received from interface are not valid!");
                mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::INV_MOVE_MSG));
            }
            return core::GameState::GETTING_INPUT;
        }

        util::Logger::logDebug("Finished making the play.");
        mPlayers.advancePlayer();
        util::Logger::logDebug("Advanced player.");
        return core::GameState::READY;
    }

    core::GameState Game::processStateUndoRedo(bool doRedo)
    {

        if (doRedo)
        {
            try
            {
                if (!mGameSimulator.redoPlay(mPlayField, mActionHolder))
                {
                    util::Logger::logDebug("Game cannot redo any more steps!");
                    mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::REDO_FAIL_MSG));
                }
                else
                    mPlayers.advancePlayer();
            } catch (std::runtime_error &e)
            {
                util::Logger::logDebug(std::string("Game failed to redo ! : ") + e.what());
                mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::REDO_FAIL_MSG));
            }
        }
        else
        {
            try
            {
                if (!mGameSimulator.undoPlay(mPlayField, mActionHolder))
                {
                    util::Logger::logDebug("Game cannot undo any more steps!");
                    mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::UNDO_FAIL_MSG));
                }
                else
                    mPlayers.advancePlayer();
            } catch (std::runtime_error &e)
            {
                util::Logger::logDebug(std::string("Game failed to undo ! : ") + e.what());
                mMsgBox->sendMessage(mMsgFactory.createMessage(core::MessageT::UNDO_FAIL_MSG));
            }
        }

        util::Logger::logDebug("Game done with UndoRedo.");

        return core::GameState::READY;
    }

    Game::~Game()
    {
        util::Logger::logDebug("Ending game thread.");
        if (mThread.joinable())
            mThread.join();
        util::Logger::logDebug("Stopped game thread.");

        util::Logger::logDebug("Ended game thread.");
    }

    std::string Game::serialize() const
    {
        /**
         * Syntax :
         *   Player1_str\n
         *   Player2_str\n
         *   \n
         *   PlayField_str\n
         *   \n
         *   CommandBuffer_str\n
         */

        std::stringstream ss;

        ss << mPlayers.serialize() << "\n";

        ss << mPlayField.serialize() << "\n";

        ss << mActionHolder.serialize() << "\n";

        return ss.str();
    }

    void Game::unserialize(const std::string &str)
    {
        /**
         * Syntax :
         *   Player1_str\n
         *   Player2_str\n
         *   \n
         *   PlayField_str\n
         *   \n
         *   CommandBuffer_str\n
         */

        std::stringstream ss(str);
        std::stringstream ac;
        std::string line;

        util::Logger::logDebug(std::string("Unserializing : ") + str);

        while (std::getline(ss, line) && !line.empty())
            ac << line << "\n";
        mPlayers.loadFromString(ac.str());

        if (!ss || !mPlayers.isFull())
        {
            util::Logger::logDebug("Not enough data in saved game string.");
            throw std::runtime_error("Not enough data in saved game string.");
        }

        ac.str("");
        ac.clear();

        std::getline(ss, line, '\n');
        mPlayField = core::PlayField(line);

        std::getline(ss, line, '\n');
        if (!line.empty())
        {
            util::Logger::logDebug("No empty new line after playField.");
            throw std::runtime_error("No empty new line after playField.");
        }

        std::size_t numberOfActions = 0;
        while (std::getline(ss, line) && !line.empty())
        {
            numberOfActions++;
            ac << line << "\n";
        }
        mActionHolder = core::ActionHolder(ac.str());

        mPlayers.advancePlayer(numberOfActions);
    }

    void Game::stop()
    {
        setGameShouldEnd();
    }

    void Game::chooseBoardSize(std::size_t choice)
    {
        if (choice < 0 || choice >= BOARD_SIZES.size())
        {
            util::Logger::logError("Board size choice out of range!");
            throw std::runtime_error("Board size choice out of range!");
        }

        std::size_t chosenSize = BOARD_SIZES[choice];

        mPlayField.setSize(chosenSize);
    }

    void Game::createPlayer(const std::string &name, std::size_t playerType, size_t aiType)
    {
        if (mActivePlayers >= PLAYER_NUM)
        {
            util::Logger::logError("Tried to add too many players!");
            throw std::runtime_error("Tried to add too many players!");
        }

        if (playerType < 0 || playerType >= PLAYER_TYPES.size())
        {
            util::Logger::logError("Player type out of range!");
            throw std::runtime_error("Player type out of range!");
        }

        if (PLAYER_TYPES[playerType] == core::HumanPlayer::TYPE_NAME)
            mPlayers.addPlayer(core::HumanPlayer::TYPE_NAME, name);
        else if (PLAYER_TYPES[playerType] == core::AIPlayer::TYPE_NAME)
        {
            const std::vector<std::string> &aiTypes = mPlayers.getAiTypes();

            if (aiType < 0 || aiType >= aiTypes.size())
            {
                util::Logger::logError("AI type out of range!");
                throw std::runtime_error("AI type out of range!");
            }

            mPlayers.addPlayer(core::AIPlayer::TYPE_NAME, name, aiTypes[aiType]);
        }
    }

    bool Game::validateName(const std::string &name)
    {
        return !std::regex_search(name,std::regex(R"/(\s|\:)/"));
    }
}
