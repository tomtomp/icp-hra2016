/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "CLI.h"

namespace cli
{

    const std::vector<std::string> CLI::MENU_OPTIONS[]{
        std::vector<std::string>{"New game", "Load game", "Save game", "Exit"}, //Menu
        std::vector<std::string>{"Put stone", "Undo", "Redo", "Main menu"} // Game
    };

    const std::string CLI::SAVED_GAMES_PATH = "./saves/";
    const std::string CLI::SAVED_GAMES_EXTENSION = ".gs";

    CLI::CLI():
        mGame{nullptr}, mMessageFactory{}
    {
        // We cannot use boost filesystem, because the source files are too big.
        /*
        // Check for saves folder
        if ( !boost::filesystem::exists( CLI::SAVED_GAMES_PATH ) ){
            boost::filesystem::create_directory(CLI::SAVED_GAMES_PATH);
        }
        if ( !boost::filesystem::is_directory( CLI::SAVED_GAMES_PATH ) ){
            throw new std::runtime_error("./saves/ should be directoy!");
        }
        */
    }

    CLI::~CLI()
    {
        if(mGame != nullptr)
            delete mGame;
    }

    void CLI::run()
    {
        //Print welcome msg
        std::cout << getWelcomeString() << std::endl;

        STATE state = STATE::MENU;
        while(1){
            switch (state){
                case STATE::MENU:
                    state = stateMenu();
                break;
                case STATE::GAME:
                    state = stateGame();
                break;
                case STATE::WAIT:
                    state = stateWait();
                break;
                case STATE::ENDED:
                    state = stateEnded();
                break;
                case STATE::EXIT:
                    return;
                break;
            }
        }
    }

    CLI::STATE CLI::stateWait()
    {
        util::Logger::logDebug("Interface waiting for message.");
        waitForMessage();
        util::Logger::logDebug("Interface got message in wait.");
        std::shared_ptr<const core::Message>  msg = receiveMessage();
        util::Logger::logDebug(std::string("Message type : ") + core::Message::getMessageTypeStr(msg->getType()));
        switch (msg->getType()) {
            case core::MessageT::READY_MSG:                
                mGame->sendMessage(mMessageFactory.createMessage(core::MessageT::NEXT_MSG));
                return CLI::STATE::WAIT;
            case core::MessageT::REQ_MOVE_MSG:
                return CLI::STATE::GAME;
            case core::MessageT::UNDO_FAIL_MSG:
                std::cout << "Cannot undo!" << std::endl;
                return CLI::STATE::WAIT;
            case core::MessageT::REDO_FAIL_MSG:
                std::cout << "Cannot redo!" << std::endl;
                return CLI::STATE::WAIT;
            case core::MessageT::INV_MOVE_MSG:
                std::cout << "Invalid move!" << std::endl;
                return CLI::STATE::WAIT;
            case core::MessageT::ENDED_MSG:
                return CLI::STATE::ENDED;

        }
    }

    CLI::STATE CLI::stateMenu()
    {
        int choisedOption = askForOption(CLI::MENU_OPTIONS[0]);
        try{
            switch (choisedOption) {
                case 0:                    
                    return cmdNewGame();
                case 1:
                    return cmdLoadGame();
                case 2:
                    return cmdSaveGame();
                case 3:
                    return cmdStop();
            }
        }catch(std::runtime_error e){
            std::cout << e.what() << std::endl;
            util::Logger::logError("Interface going from state Menu to state Menu cause error " + std::string(e.what()) );
            return STATE::MENU;
        }
    }

    CLI::STATE CLI::stateGame()
    {
        renderField(mGame->getPlayField());
        std::cout << "Player: " << mGame->getPlayer(mGame->getCurrentPlayerID()).get()->getName() << "[" << CLI::getPlayerSymbol(mGame->getCurrentPlayerID()) << "] is on the move." << std::endl;
        int choisedOption = askForOption(CLI::MENU_OPTIONS[1]);
        switch (choisedOption) {
            case 0:
                return cmdPut();
            case 1:
                return cmdUndo();
            case 2:
                return cmdRedo();
            case 3:
                return STATE::MENU;
        }
    }

    CLI::STATE  CLI::cmdNewGame()
    {
        util::Logger::logInfo("Interface creating new game");
        destroyGame();
        mGame = new core::Game(this);
        newGameDialog();
        clearMessageBox();
        mGame->start();
        std::cout << "--------------------------------------------------" << std::endl;
        return CLI::STATE::WAIT;
    }

    CLI::STATE CLI::cmdLoadGame()
    {
        util::Logger::logInfo("Interface loading new game");
        auto savedGames = getSavedGames();
        int choise = askForOption(savedGames);
        if(choise == savedGames.size() -1)
            return CLI::STATE::MENU;

        util::Logger::logInfo("Loading game: " + savedGames[choise]);

        std::ifstream file(CLI::SAVED_GAMES_PATH + savedGames[choise]);
        std::string gameString((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

        destroyGame();
        mGame = new core::Game(this,gameString);
        clearMessageBox();
        mGame->start();
        return CLI::STATE::WAIT;

    }

    CLI::STATE CLI::cmdSaveGame()
    {
        std::string saveName;
        bool ok = true;
        do{
            std::cout << "Name your save: ";
            std::getline(std::cin,saveName);
            if(!(ok = std::regex_search(saveName, std::regex(R"/(^[\w,\s-]+$)/")))){
                std::cout << "Invalid name! Please don't be so creative. No extension allowed" << std::endl;
                util::Logger::logError("Bad save name entered: " + saveName);
            }
        }while(!ok);

        std::ofstream saveFile;
        saveFile.open(CLI::SAVED_GAMES_PATH + saveName + CLI::SAVED_GAMES_EXTENSION);
        saveFile << mGame->serialize() << std::endl;
        std::cout << "Game saved!\n" << std::endl;
        util::Logger::logInfo("Game saved: " + saveName);
        return CLI::STATE::GAME;
    }

    CLI::STATE CLI::cmdStop()
    {
        util::Logger::logInfo("Interface shuting down");
        if(mGame != nullptr && mGame->isRunning()){
            mGame->sendMessage(mMessageFactory.createMessage(core::MessageT::END_MSG));
            util::Logger::logDebug("Interface sent message END");
            mGame->stop();
        }
        return CLI::STATE::EXIT;
    }

    CLI::STATE CLI::cmdUndo()
    {
        mGame->sendMessage(mMessageFactory.createMessage(core::MessageT::UNDO_MSG));
        util::Logger::logDebug("Interface sent message UNDO");
        return CLI::STATE::WAIT;
    }

    CLI::STATE CLI::cmdRedo()
    {
        mGame->sendMessage(mMessageFactory.createMessage(core::MessageT::REDO_MSG));
        util::Logger::logDebug("Interface sent message REDO");
        return CLI::STATE::WAIT;
    }

    CLI::STATE CLI::cmdPut()
    {
        std::string input;
        int x,y;
        std::smatch matches;
        std::regex r(R"/(^([a-z])(\d?\d)$)/",std::regex::icase);
        do{
            std::cout << "Enter coords [a-z][0-26]:";
            std::getline(std::cin,input);
        }while(!std::regex_search(input,matches,r));
        x = cli::CLI::letter2int(std::string(matches[1]).c_str()[0]);
        y = std::stoi(matches[2]);
        mGame->sendMessage(mMessageFactory.createCoordsMessage(core::Coords{x,y}));
        util::Logger::logDebug("Interface sent message put coord " + std::to_string(x) + "-" + std::to_string(y) );
        return CLI::STATE::WAIT;
    }

    CLI::STATE CLI::stateEnded()
    {
        int winnerID = 0;
        std::vector<std::size_t> scores = mGame->getScores();
        if(scores[1] < scores[2])
            winnerID = 2;
        if(scores[1] > scores[2])
            winnerID = 1;

        std::cout << "\nGame end" << std::endl;
        renderField(mGame->getPlayField());
        std::cout << "Scores" << std::endl;
        std::cout << "Player " << mGame->getPlayer(1)->getName() << "[" << getPlayerSymbol(1) << "] - " << scores[1] <<  std::endl;
        std::cout << "Player " << mGame->getPlayer(2)->getName() << "[" << getPlayerSymbol(2) << "] - " << scores[2] <<  std::endl;
        if(winnerID > 0){
            std::cout << mGame->getPlayer(winnerID)->getName() << " wins!" << std::endl;
        }else{
            std::cout << "The game is a draw, you both lose" << std::endl;
        }
        std::cout << "--------------------------------------------------" << std::endl;
        return CLI::STATE::MENU;
    }

    bool CLI::strEndsWith(const std::string &str, const std::string &search)
    {
        if (str.length() >= search.length())
        {
            return str.compare(str.length() - search.length(), search.length(), search) == 0;
        }
        else
            return false;
    }

    const std::vector<std::string> CLI::getSavedGames() const
    {
        std::vector<std::string> options;

        // Cannot use boost filesystem, because the source files are too big.
        /*
        boost::filesystem::directory_iterator end_iter;
        for ( boost::filesystem::directory_iterator dir_itr( CLI::SAVED_GAMES_PATH ); dir_itr != end_iter; ++dir_itr ){
            if ( boost::filesystem::is_regular_file( dir_itr->status() )  && dir_itr->path().extension() == CLI::SAVED_GAMES_EXTENSION ){
                options.push_back(dir_itr->path().filename().string());
            }
        }
        */

        // Found on : http://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c

        DIR *dir;
        dirent *ent;
        if ((dir = opendir (CLI::SAVED_GAMES_PATH.c_str())) != NULL)
        {
            while ((ent = readdir (dir)) != NULL)
            {
                // If the entity is a regular file.
                if (ent->d_type == DT_REG)
                {
                    std::string filename(ent->d_name);
                    if (strEndsWith(filename, CLI::SAVED_GAMES_EXTENSION))
                        options.push_back(filename);
                }
            }
            closedir (dir);
        }
        // If we were not able to open the directory, then no saves will be available

        options.push_back("Main menu");
        return options;
    }


    std::string CLI::askForPlayerName(int playerNum, std::function<bool(const std::string&)> validateName)
    {
        std::string playerName;
        bool ok = true;
        do{
            std::cout << "Enter name for player #" << playerNum  << ":";
            std::getline(std::cin,playerName);
            if(!(ok = validateName(playerName))){
                std::cout << "Invalid name! Please don't be so creative" << std::endl;
                util::Logger::logError("player #" + std::to_string(playerNum) + " inputed bad name: " + playerName);
            }
        }while(!ok);
        util::Logger::logInfo("player #" + std::to_string(playerNum) + " inputed name: " + playerName);
        std::cout << std::endl;
        return playerName;
    }

    void CLI::destroyGame()
    {
        if(mGame != nullptr){
            cmdStop();
            delete mGame;
        }        
        mGame = nullptr;
    }

    void CLI::newGameDialog()
    {
        std::string playerName;
        int playerType = 0;
        int aiType = 0;

        // Ask and chose a board size
        std::vector<std::string> boardSizes;
        for(int i = 0; i < mGame->getBoardSizes().size(); i++)
            boardSizes.push_back(std::to_string(mGame->getBoardSizes()[i]) + "x" + std::to_string(mGame->getBoardSizes()[i]));
        mGame->chooseBoardSize(askForOption(boardSizes,"Choose board size"));

        // Ask (twice) for player info
        for(std::size_t i = 1; i <= mGame->PLAYER_NUM; i++){
            playerName = askForPlayerName(i, core::Game::validateName);
            playerType = askForOption(mGame->getPlayerTypes(),"Choose player type");
            if(playerType == mGame->getPlayerTypes().size()-1 ){
                aiType = askForOption(mGame->getAiTypes(),"Choose AI");
            }            
            mGame->createPlayer(playerName,playerType,aiType);
        }
    }

    int CLI::askForOption(std::vector<std::string> options, std::string question)
    {
        int choise = 0;
        do{
            std::cout << question << std::endl;
            for(int i = 0; i< options.size(); i++){
                std::cout << "["<<i+1<<"] " << options[i] << std::endl;
            }
            std::cout << "Please enter number between 1 to " << options.size() << ":";
            std::cin >> choise;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }while(choise < 1 || choise > options.size());

        choise--;
        std::cout << std::endl;
        util::Logger::logInfo("On question " + question + " selected " + options[choise]);
        return choise;
    }


    const void CLI::renderField(const core::PlayField &field)
    {
        std::cout << std::endl;
        size_t size = field.getSize();
        std::cout << "   ";
        for(int i = 0; i < size; i++){
            std::cout << int2letter(i) << " ";
        }
        std::cout << std::endl;

        char stoneOID;
        for(int y = 0; y < size; y++){
            if(y -10 < 0)
                std::cout << " ";
            std::cout << y << " ";
            for(int x = 0; x < size; x++){
                stoneOID =field.getStoneOID(core::Coords{x,y});
                if(stoneOID == field.EMPTY_ID){
                    std::cout << " ";                
                }else{
                    std::cout << CLI::getPlayerSymbol(stoneOID);
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    const char CLI::getPlayerSymbol(int playerID)
    {
        if(playerID == 1){
            return 'X';
        }else if(playerID == 2){
            return '0';
        }else{
            return 'A' - 1 + playerID;
        }
    }

    int CLI::letter2int(char l){
        return std::tolower(l) - 97; // Sexy syntax
    }

    char CLI::int2letter(int i)
    {
        if(i < 0 || i > 26){
            return '0';
        }
        return (static_cast<char>(i + 97));
    }
}

