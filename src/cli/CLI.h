/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_CLIBase_H
#define HRA2016_CLIBase_H

#include <vector>
#include <iostream>
#include <functional>

#include "../core/Game.h"
#include "../core/MessageBox.h"
#include "../core/messages/MessageFactory.h"
#include "../util/Logger.h"

// Cannot use boost filesystem, because the sources are too big.
//#include "boost/filesystem/operations.hpp"

extern "C"
{
    #include <dirent.h>
};

namespace cli
{

    /**
     * Command line interface implementation.
     */
    class CLI : public core::MessageBox
    {
        enum STATE{MENU,GAME,WAIT,EXIT, ENDED};

    public:
        CLI(const CLI &other) = delete;
        CLI(CLI &&other) = delete;

        /**
         * Create new command line interface.
         */
        CLI();

        ~CLI();

        /**
         * Run the command line interface.
         */
        void run();

        /**
         * Return a welcome message
         * @return Welcome message
         */
        std::string getWelcomeString() const{
            return std::string("\n\n/-----------------\\\n|HRA2016 - Reversi|\n|  for ICP 2016   |\n\\-----------------/\n\n");
        }

        /**
         * Transform int to letter . 0 -> A, 1 -> B ....
         * @see int2letter
         * @param l
         * @return int
         */
        static int letter2int(char l);

        /**
         * Transform letter to int . A -> 0, B -> 1 ....
         * @see letter2int
         * @param i
         * @return letter
         */
        static char int2letter(int i);

    private:

        /**
         * Do a ini. dialog with player/
         */
        void newGameDialog();

        /**
         * Prompt a user to selected a option from list
         * @param options List od options
         * @param question Optinal question "Chose option" is defualt
         * @return index of selected option
         */
        int askForOption(std::vector<std::string> options, std::string question = "Choose option");

        /**
         * Prompt user to enter player name
         * @param playerNum Index of player
         * @param validateName function for validation
         * @return
         */
        std::string askForPlayerName(int playerNum, std::function<bool(const std::string&)> validateName);

        /**
         * Does the given string end with another string?
         * @param str The complete string.
         * @param search The searched ending.
         * @return Returns true, if the str ends with searched string.
         */
        static bool strEndsWith(const std::string &str, const std::string &search);

        /**
         * Proper delete a game object
         */
        void destroyGame();

        /**
         * Represent a satate when CLI wait for game input
         * @return next state
         */
        CLI::STATE stateWait();

        /**
         * Represent a satate when user is in menu
         * @return next state
         */
        CLI::STATE stateMenu();

        /**
         * Represent a satate when user is in game
         * @return next state
         */
        CLI::STATE stateGame();

        /**
         * Represent a satate when CLI ends
         * @return next state
         */
        CLI::STATE stateEnded();

        /**
         * Command new game
         * @return state that should CLI follow
         */
        CLI::STATE cmdNewGame();

        /**
         * Command load game
         * @return state that should CLI follow
         */
        CLI::STATE cmdLoadGame();

        /**
         * Command save game
         * @return state that should CLI follow
         */
        CLI::STATE cmdSaveGame();

        /**
         * Command stop
         * @return state that should CLI follow
         */
        CLI::STATE cmdStop();

        /**
         * Command undo
         * @return state that should CLI follow
         */
        CLI::STATE cmdUndo();

        /**
         * Command redo
         * @return state that should CLI follow
         */
        CLI::STATE cmdRedo();

        /**
         * Command put
         * @return state that should CLI follow
         */
        CLI::STATE cmdPut();

        /**
         * Get saved option from default path
         * @see SAVED_GAMES_PATH
         * @return
         */
        const std::vector<std::string> getSavedGames() const;

        /**
         * Render a field to CL
         * @param field
         */
        const void renderField(const core::PlayField &field);

        /**
         * Core game object
         */
        core::Game *mGame;

        /**
         * Message factory for making messages
         */
        core::MessageFactory mMessageFactory;

        /**
         * Returns a game symbol for player.
         * Return X, O or any char from alphabet, but not always unique.
         * @param playerID
         * @return Game symbol
         */
        static const char getPlayerSymbol(int playerID);

        /**
         * Menu options
         */
        static const std::vector<std::string> MENU_OPTIONS[];

        /**
         * Path where are saved games
         */
        static const std::string SAVED_GAMES_PATH;

        /**
         * Saved games file extension
         */
        static const std::string SAVED_GAMES_EXTENSION;

    protected:
    };
}


#endif //HRA2016_CLI_H
