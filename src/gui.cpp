/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "gui.h"

/**
 * Main function for the GUI.
 */
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    gui::MainWindow mainWindow;
    mainWindow.resize(800, 600);
    mainWindow.show();

    return app.exec();
}
