/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_GUI_H
#define HRA2016_GUI_H

#include "gui/MainWindow.h"
#include "gui/render/MeshLoader.h"
#include "gui/render/TextureLoader.h"

#include <QApplication>

#include <iostream>

#endif //HRA2016_GUI_H
