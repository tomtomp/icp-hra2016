/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_CLI_H
#define HRA2016_CLI_H

#include "core/ActionHolder.h"
#include "cli/CLI.h"
#include "core/Game.h"
#include "util/Logger.h"

#include <iostream>

#endif //HRA2016_CLI_H
