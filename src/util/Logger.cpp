/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "Logger.h"

namespace util
{
    const std::string Logger::FILENAME("out.log");
    Logger *LOGGER{nullptr};

    Logger::Logger() :
        mOut(FILENAME, std::ios_base::app)
    {
        mOut << "\nStarting a new log : " << getTimeString() << " : " << std::endl;
    }

    void Logger::log(const std::string &message, const std::string &type)
    {
        std::string time{getTimeString()};

        std::unique_lock<std::mutex> lock(mOutMutex);

        mOut << "[" << type << "]\t: " << time << " : " << message << std::endl;
    }

    Logger &Logger::instance()
    {
        static Logger logger;
        return logger;
    }
}
