/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_LOGGER_H
#define HRA2016_LOGGER_H

#include <string>
#include <iostream>
#include <ctime>
#include <iomanip>
#include <fstream>
#include <mutex>

namespace util
{
    /**
     * Thread-safe logger.
     * Usage :
     *  1) Include header.
     *  2) Use the static methods :
     *      util::Logger::logInfo("Hello Log");
     */
    class Logger
    {
    public:
        /**
         * Log given message as info.
         * @param message Message to print out.
         */
        static void logInfo(const std::string &message)
        {
            instance().log(message, "INFO");
        }

        /**
         * Log given message as error.
         * @param message Message to print out.
         */
        static void logError(const std::string &message)
        {
            instance().log(message, "ERROR");
        }

        /**
         * Log given message as debug.
         * @param message Message to print out.
         */
        static void logDebug(const std::string &message)
        {
#ifndef NDEBUG
            instance().log(message, "DEBUG");
#endif
        }

        /// Name of the logging file.
        static const std::string FILENAME;
    private:
        Logger(const Logger &other) = delete;
        Logger(Logger &&other) = delete;

        /**
         * Create a new logger.
         */
        Logger();

        /**
         * Write given message to the logging file.
         * @param message Message to write out.
         */
        void log(const std::string &message, const std::string &type);

        /// Logging file.
        std::ofstream mOut;

        /// Securing thread-safe behaviour.
        std::mutex mOutMutex;

        /**
         * Get a logger instance. Create one, if none exists.
         * @return Returns the logger object.
         */
        static Logger &instance();

        /**
         * Get current time as a string;
         */
        static std::string getTimeString()
        {
            auto t = std::time(nullptr);
            auto tm = *std::localtime(&t);

            std::stringstream ss;
            ss << std::put_time(&tm, "%d.%m.%Y %H:%M:%S");

            return ss.str();
        }
    protected:
    };
}

#endif //HRA2016_LOGGER_H
