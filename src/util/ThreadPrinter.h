/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#ifndef HRA2016_THREADPRINTER_H
#define HRA2016_THREADPRINTER_H

#include <iostream>
#include <sstream>

namespace util
{
    /**
     * This printer first acumulates all of the input and releases it all in once.
     */
    class ThreadPrinter
    {
    public:
        ThreadPrinter(const ThreadPrinter&) = delete;
        ThreadPrinter(ThreadPrinter&&) = delete;

        /**
         * Get the output stream, where all of the input will eventualy go.
         * @param os Output stream.
         */
        ThreadPrinter(std::ostream &os) :
            mOs(os) { };

        /**
         * Puts the input to the accumulator
         */
        template<typename T>
        ThreadPrinter &operator<<(const T &t) {
            mSs << t;
            return *this;
        }

        /**
         * Immediately print the content of input buffer to the output.
         */
        void flush()
        {
            mOs << mSs.rdbuf() << std::flush;

            mSs.str(std::string());
            mSs.clear();
        }

        /**
         * Destruct this object, but first print all of the input in the buffer.
         */
        ~ThreadPrinter()
        {
            mOs << mSs.rdbuf() << std::flush;
        }
    private:
        /// Where all the input will go.
        std::ostream &mOs;

        /// The accumulator.
        std::stringstream mSs;
    protected:
    };
}

#endif //HRA2016_THREADPRINTER_H
