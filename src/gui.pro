CONFIG += release c++14
QT += widgets opengl
QMAKE_CXXFLAGS += -DNDEBUG
LIBS += $$PWD/../lib/libcore.a -lGL -static-libstdc++
MOC_DIR = $$PWD/../build/
OBJECTS_DIR = $$PWD/../build/
INCLUDE_PATH += $$PWD/ $$PWD/../

RESOURCES += $$PWD/gui/GuiResources.qrc

SOURCES += \
    $$PWD/gui.cpp $$PWD/core/MessageBox.cpp \
    $$PWD/gui/MainWindow.cpp $$PWD/gui/GameTabBar.cpp  \
    $$PWD/gui/NewGameDialog.cpp $$PWD/gui/PlayerDialogWidget.cpp  \
    $$PWD/gui/GameWidget.cpp $$PWD/gui/GameViewer.cpp  \
    $$PWD/gui/render/GameModel.cpp $$PWD/gui/render/GameObject.cpp \
    $$PWD/gui/render/transform/TransformMatrix3D.cpp \
    $$PWD/gui/render/MeshLoader.cpp \
    $$PWD/gui/render/Vertex.cpp $$PWD/gui/render/TextureLoader.cpp \
    $$PWD/gui/render/RenderableObject.cpp \
    $$PWD/gui/render/CollectionObject.cpp $$PWD/gui/render/RenderableCollection.cpp \
    $$PWD/gui/render/Camera.cpp $$PWD/gui/render/Mesh.cpp \
    $$PWD/gui/render/Texture.cpp $$PWD/gui/render/TextObject.cpp \
    $$PWD/gui/render/StoneObject.cpp \
    $$PWD/gui/render/RenderableStoneObject.cpp

HEADERS += \
    $$PWD/gui.h $$PWD/util/Logger.h $$PWD/core/MessageBox.h \
    $$PWD/gui/MainWindow.h $$PWD/gui/GameTabBar.h \
    $$PWD/gui/NewGameDialog.h $$PWD/gui/PlayerDialogWidget.h \
    $$PWD/gui/GameWidget.h $$PWD/gui/GameViewer.h \
    $$PWD/gui/render/GameModel.h \
    $$PWD/gui/render/GameObject.h \
    $$PWD/gui/render/transform/TransformMatrix3D.h \
    $$PWD/gui/render/MeshLoader.h $$PWD/gui/render/Vertex.h \
    $$PWD/gui/render/TextureLoader.h $$PWD/gui/render/RenderableObject.h \
    $$PWD/gui/render/CollectionObject.h \
    $$PWD/gui/render/RenderableCollection.h $$PWD/gui/render/Camera.h \
    $$PWD/gui/render/Mesh.h $$PWD/gui/render/Texture.h \
    $$PWD/gui/render/TextObject.h $$PWD/gui/render/StoneObject.h \
    $$PWD/gui/render/RenderableStoneObject.h
