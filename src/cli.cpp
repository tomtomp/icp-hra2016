/*
 * Task ICP - HRA2016: Reversi game in C++ for ICP 2015/2016
 * Created by: xpolas34
 * Description: This file contains the core Game class.
 */

#include "cli.h"

#define TRY_EXC(E) \
    try { E } catch (std::runtime_error &e) \
        {std::cout << "Error occured : " << e.what() << std::endl;}

/**
 * Main function for the CLI.
 */
int main()
{
    cli::CLI interface;
    interface.run();

    return 0;

    core::MessageBox msgBox;
    core::MessageFactory msgFactory;
    std::shared_ptr<const core::Message> msg;
    core::Game game(&msgBox);

    game.chooseBoardSize(1);

    game.createPlayer("Thomas", 0);
    game.createPlayer("Bob", 0);

    game.start();

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    if (msg->getType() == core::MessageT::READY_MSG)
    {
        auto playField = game.getPlayField();
        std::size_t playFieldSize = playField.getSize();
        for (std::size_t jjj = 0; jjj < playFieldSize; ++jjj)
        {
            for (std::size_t iii = 0; iii < playFieldSize; ++iii)
            {
                switch (playField.getStoneOID({iii, jjj}))
                {
                    case 1:
                        std::cout << "X";
                        break;
                    case 2:
                        std::cout << "O";
                        break;
                    default:
                        std::cout << " ";
                        break;
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
    }

    game.sendMessage(msgFactory.createMessage(core::MessageT::NEXT_MSG));

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    game.sendMessage(msgFactory.createCoordsMessage({1, 2}));

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    if (msg->getType() == core::MessageT::INV_MOVE_MSG)
    {
        msgBox.waitForMessage();
        msg = msgBox.receiveMessage();
        std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

        game.sendMessage(msgFactory.createCoordsMessage({4, 2}));
    }

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    if (msg->getType() == core::MessageT::READY_MSG)
    {
        auto playField = game.getPlayField();
        std::size_t playFieldSize = playField.getSize();
        for (std::size_t jjj = 0; jjj < playFieldSize; ++jjj)
        {
            for (std::size_t iii = 0; iii < playFieldSize; ++iii)
            {
                switch (playField.getStoneOID({iii, jjj}))
                {
                    case 1:
                        std::cout << "X";
                        break;
                    case 2:
                        std::cout << "O";
                        break;
                    default:
                        std::cout << " ";
                        break;
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
    }

    game.sendMessage(msgFactory.createMessage(core::MessageT::NEXT_MSG));

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    game.sendMessage(msgFactory.createCoordsMessage({5, 2}));

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    if (msg->getType() == core::MessageT::INV_MOVE_MSG)
    {
        /*
         * ...
         */
    }

    if (msg->getType() == core::MessageT::READY_MSG)
    {
        auto playField = game.getPlayField();
        std::size_t playFieldSize = playField.getSize();
        for (std::size_t jjj = 0; jjj < playFieldSize; ++jjj)
        {
            for (std::size_t iii = 0; iii < playFieldSize; ++iii)
            {
                switch (playField.getStoneOID({iii, jjj}))
                {
                    case 1:
                        std::cout << "X";
                        break;
                    case 2:
                        std::cout << "O";
                        break;
                    default:
                        std::cout << " ";
                        break;
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
    }

    game.sendMessage(msgFactory.createMessage(core::MessageT::END_MSG));

    msgBox.waitForMessage();
    msg = msgBox.receiveMessage();
    std::cout << "Received : " << core::Message::getMessageTypeStr(msg->getType()) << std::endl;

    return 0;
}
