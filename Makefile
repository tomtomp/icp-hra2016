# Found on : http://stackoverflow.com/questions/592620/check-if-a-program-exists-from-a-bash-script
CXX=$(shell command -v g++-5.3 || command -v g++ || { echo >&2 "Unable to find g++."; exit 1; })
AR=ar
RM=rm -f
CXXFLAGS=-std=c++14 -O3 -DNDEBUG -fPIC
LDLIBS=-pthread -static-libstdc++
AR_FLAGS=rvs
QMAKE=$(shell command -v qmake-qt5 || find /usr/local/share/Qt/* -name qmake | grep -i "bin/qmake" || command -v qmake || { echo >&2 "Unable to find qmake."; exit 1; })
QMAKE_LIBS=$(shell $(QMAKE) -query QT_INSTALL_LIBS)
QMAKE_PLUGINS=$(shell $(QMAKE) -query QT_INSTALL_PLUGINS)

DOXYGEN_SRC= dox

CORE_SRC= src/core/Game.cpp src/core/players/Player.cpp \
          src/core/MoveAction.cpp src/core/PlayField.cpp \
          src/core/MoveAction.cpp \
          src/util/Logger.cpp src/core/players/HumanPlayer.cpp \
          src/core/players/AIPlayer.cpp \
          src/core/players/AIRandom.cpp \
          src/core/players/PlayerFactory.cpp src/core/GameSimulation.cpp \
          src/core/Types.cpp src/core/MessageBox.cpp \
          src/core/messages/Message.cpp src/core/messages/MessageFactory.cpp \
          src/core/messages/CoordsMessage.cpp \
          src/core/ActionHolder.cpp src/core/PlayerHolder.cpp \
          src/core/players/AIAlphaBeta.cpp
CORE_OBJS=$(subst .cpp,.o, $(CORE_SRC))
CORE_DEPEND=.depend_core
CORE_LIB= lib/libcore.a

CLI_SRC= src/cli.cpp src/core/MessageBox.cpp \
         src/cli/CLI.cpp
CLI_OBJS=$(subst .cpp,.o, $(CLI_SRC))
CLI_DEPEND=.depend_cli
CLI_OUT= bin/hra2016-cli

GUI_PRO= src/gui.pro
GUI_MAKE= gui.make
GUI_OUT= bin/hra2016

PACK_NAME=xpolas34.zip

all: cli gui

doxygen: $(DOXYGEN_SRC)
	doxygen $(DOXYGEN_SRC)

$(CORE_LIB): $(CORE_OBJS)
	@mkdir -p lib
	$(AR) $(AR_FLAGS) $(CORE_LIB) $^

depend_core: .depend_core

.depend_core: $(CORE_SRC)
	$(RM) $(CORE_DEPEND)
	$(CXX) $(CXXFLAGS) -MM $^>>$(CORE_DEPEND);

include: $(CORE_DEPEND)

cli: $(CLI_OBJS) $(CORE_LIB)
	@mkdir -p bin
	$(CXX) -o $(CLI_OUT) $^ $(LDLIBS)

depend_cli: .depend_cli
.depend_cli: $(CLI_SRC)
	$(RM) $(CLI_DEPEND)
	$(CXX) $(CXXFLAGS) -MM $^>>$(CLI_DEPEND);

include: $(CLI_DEPEND)

gui: $(CORE_LIB)
	@mkdir -p build
	@mkdir -p bin
	$(QMAKE) $(GUI_PRO) -o build/$(GUI_MAKE) "QMAKE_CXX=$(CXX)" "QMAKE_LINK=$(CXX)"
	@cd build; \
	LD_LIBRARY_PATH=`$(QMAKE) -query QT_INSTALL_LIBS` make -f $(GUI_MAKE)
	cp build/gui $(GUI_OUT)

clean:
	$(RM) $(CORE_OBJS) $(CORE_LIB)
	$(RM) -r doc/*
	$(RM) $(CLI_OBJS)
	$(RM) -r build
	$(RM) $(PACK_NAME)
	$(RM) saves
	$(RM) bin/*

pack:
	$(RM) $(PACK_NAME)
	zip -r $(PACK_NAME) src/* examples/* $(CORE_DEPEND) $(CLI_DEPEND) $(DOXYGEN_SRC) \
	    CMakeLists.txt contributors Makefile README.md include

run: cli gui
	rm -f saves
	ln -s examples saves
	rm -f ./bin/libs
	ln -s $(QMAKE_LIBS) ./bin/libs
	rm -f ./bin/plugins
	ln -s $(QMAKE_PLUGINS) ./bin/plugins
	rm -f ./bin/qt.conf
	echo "[Paths]" >> ./bin/qt.conf
	echo "plugins/" >> ./bin/qt.conf
	LD_LIBRARY_PATH=./bin/libs $(GUI_OUT) &
	$(CLI_OUT)
