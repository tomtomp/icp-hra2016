cmake_minimum_required(VERSION 2.4)
project(HRA2016)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

if ( CMAKE_SYSTEM_NAME MATCHES "Windows" )
    include_directories(./include/)
endif()

#find_package(Boost REQUIRED COMPONENTS system filesystem)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL REQUIRED)
find_package(OpenGL REQUIRED)

include_directories(${OPENGL_INCLUDE_DIRS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -pthread")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -pedantic -g -lGL")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -pedantic -O3 -DNDEBUG")

set(CORE_SOURCE_FILES src/core/Game.cpp src/core/Game.h src/util/ThreadPrinter.h src/core/players/Player.cpp
                      src/core/players/Player.h src/core/MoveAction.cpp src/core/MoveAction.h src/core/PlayField.cpp
                      src/core/PlayField.h src/core/interfaces/Serializable.h
                      src/core/Types.h src/util/Logger.h src/core/MoveAction.cpp src/core/MoveAction.h
                      src/util/Logger.cpp src/core/players/HumanPlayer.cpp src/core/players/HumanPlayer.h
                      src/core/players/AIPlayer.cpp src/core/players/AIPlayer.h
                      src/core/players/AIRandom.cpp src/core/players/AIRandom.h
                      src/core/players/PlayerFactory.h src/core/players/PlayerFactory.cpp src/core/GameSimulation.cpp src/core/GameSimulation.h
                      src/core/Types.cpp src/core/MessageBox.cpp src/core/MessageBox.h src/core/messages/Message.cpp
                      src/core/messages/Message.h src/core/messages/MessageFactory.cpp
                      src/core/messages/MessageFactory.h src/core/messages/CoordsMessage.cpp
                      src/core/messages/CoordsMessage.h src/core/ActionHolder.cpp src/core/ActionHolder.h src/core/PlayerHolder.cpp src/core/PlayerHolder.h src/core/players/AIAlphaBeta.cpp src/core/players/AIAlphaBeta.h)
add_library(core ${CORE_SOURCE_FILES})

set(CLI_SOURCE_FILES src/cli.cpp src/cli.h src/util/Logger.h src/core/MessageBox.cpp src/core/MessageBox.h
                     src/cli/CLI.cpp src/cli/CLI.h)
add_executable(hra2016-cli ${CLI_SOURCE_FILES})
target_link_libraries(hra2016-cli core)

set(GUI_SOURCE_FILES src/gui.cpp src/gui.h src/util/Logger.h src/core/MessageBox.cpp src/core/MessageBox.h
        src/gui/MainWindow.cpp src/gui/MainWindow.h src/gui/GameTabBar.cpp src/gui/GameTabBar.h
        src/gui/NewGameDialog.cpp src/gui/NewGameDialog.h src/gui/PlayerDialogWidget.cpp src/gui/PlayerDialogWidget.h
        src/gui/GameWidget.cpp src/gui/GameWidget.h src/gui/GameViewer.cpp src/gui/GameViewer.h
        src/gui/render/GameModel.cpp src/gui/render/GameModel.h src/gui/render/GameObject.cpp
        src/gui/render/GameObject.h src/gui/render/transform/TransformMatrix3D.cpp
        src/gui/render/transform/TransformMatrix3D.h src/gui/render/MeshLoader.cpp
        src/gui/render/MeshLoader.h src/gui/render/Vertex.cpp src/gui/render/Vertex.h src/gui/render/TextureLoader.cpp
        src/gui/render/TextureLoader.h src/gui/render/RenderableObject.cpp src/gui/render/RenderableObject.h
        src/gui/render/CollectionObject.cpp src/gui/render/CollectionObject.h src/gui/render/RenderableCollection.cpp
        src/gui/render/RenderableCollection.h src/gui/render/Camera.cpp src/gui/render/Camera.h src/gui/render/Mesh.cpp
        src/gui/render/Mesh.h src/gui/render/Texture.cpp src/gui/render/Texture.h src/gui/render/TextObject.cpp
        src/gui/render/TextObject.h src/gui/render/StoneObject.cpp src/gui/render/StoneObject.h
        src/gui/render/RenderableStoneObject.cpp src/gui/render/RenderableStoneObject.h)
qt5_add_resources(GUI_RESOURCES src/gui/GuiResources.qrc)
add_executable(hra2016 ${GUI_SOURCE_FILES} ${GUI_RESOURCES})
qt5_use_modules(hra2016 Widgets OpenGL)
target_link_libraries(hra2016 core ${OPENGL_LIBARIES} Qt5::Widgets)
